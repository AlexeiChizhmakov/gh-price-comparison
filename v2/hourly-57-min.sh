#!/bin/bash

pkill node
sleep 3
pkill electron
sleep 3
pkill Xvfb
sleep 3

killall node
sleep 3
killall electron
sleep 3
killall Xvfb
sleep 3

node --harmony aws-s3-export.js &>> ./logs/s3-export.log&
