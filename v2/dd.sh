#!/bin/bash

  sleep 1

  node --harmony clean-used-proxies.js
  
  sleep 1
  
  for a in `seq 1 4`
  do
    xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony --max-old-space-size=2048 doordash-thread-v2-api.js $a >> ./logs/doordash-thread-$a-api.log 2>&1 &
    sleep 3
  done  
  