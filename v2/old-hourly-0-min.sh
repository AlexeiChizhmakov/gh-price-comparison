#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/home/admin/.nvm/versions/node/v6.11.5/bin
HOME=/home/admin

sleep 1

node --harmony clean-used-proxies.js
  
sleep 1

/usr/bin/Xvfb :1 -maxclients 2048 -screen 0 1x1x8 -screen 1 1x1x8 -screen 2 1x1x8 -screen 3 1x1x8 -screen 4 1x1x8 -screen 5 1x1x8 -screen 6 1x1x8 -screen 7 1x1x8 -screen 8 1x1x8 -screen 9 1x1x8 -screen 10 1x1x8 -screen 11 1x1x8 -screen 12 1x1x8 -screen 13 1x1x8 -screen 14 1x1x8 -screen 15 1x1x8 >/tmp/xvfb.log 2>&1 & 

sleep 2

/usr/bin/Xvfb :2 -maxclients 2048 -screen 0 1x1x8 -screen 1 1x1x8 -screen 2 1x1x8 -screen 3 1x1x8 -screen 4 1x1x8 -screen 5 1x1x8 -screen 6 1x1x8 -screen 7 1x1x8 -screen 8 1x1x8 -screen 9 1x1x8 -screen 10 1x1x8 -screen 11 1x1x8 -screen 12 1x1x8 -screen 13 1x1x8 -screen 14 1x1x8 -screen 15 1x1x8 >/tmp/xvfb.log 2>&1 & 

sleep 2

# This will start 315 processes
for ind in `seq 0 15 300`
do
  for screen in `seq 1 15`
  do
    b=$(expr $ind + $screen)
    export DISPLAY=:1.$screen; node --harmony --max-old-space-size=2048 doordash-thread-v2-api.js $b >> ./logs/doordash-thread-$b-api.log 2>&1 &
#    if [ "$b" -lt 256 ] 
#    then
      export DISPLAY=:2.$screen; node --harmony --max-old-space-size=2048 ubereats-thread-v2-api.js $b >> ./logs/ubereats-thread-$b-api.log 2>&1 &
#    fi
    sleep 0.5    
  done
done 

sleep 2
 
for c in `seq 1 2`
do
  node --harmony --max-old-space-size=2048 grubhub-thread-v2-api.js $c >> ./logs/grubhub-thread-$c-api.log 2>&1 &
  sleep 0.5
done  
