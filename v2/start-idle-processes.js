// node --harmony start-idle-processes.js

const fs = require('fs');
var sys = require('sys')
var exec = require('child_process').exec;

const logFolderPath = './logs/';

let deadThreads = []; 

fs.readdirSync(logFolderPath).forEach(file => {
	if (file.indexOf('.log') >= 0) {
		var fileSizeBytes = fs.statSync(logFolderPath + file).size;
		if (fileSizeBytes < 1024) {
			var thread = {};
			thread.id = file.replace(/\D/g,'');
			thread.website = file.split('-')[0].trim();
			deadThreads.push(thread);
		}
	}
});

if (deadThreads.length <= 5) {
	for (var dti = 0; dti < deadThreads.length; dti++) {
		exec('export DISPLAY=:1.1; node --harmony --max-old-space-size=2048 ' + deadThreads[dti].website + '-thread-v2-api.js ' + deadThreads[dti].id + ' >> ./logs/' + deadThreads[dti].website + '-thread-' + deadThreads[dti].id + '-api.log 2>&1 &');
	}
}
