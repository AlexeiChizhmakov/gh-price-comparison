// xvfb-run -a --server-args="-screen 0 1x1x8" node --harmony doordash-thread-v2-api.js 1 debug
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 1x1x8" node --harmony doordash-thread-v2-api.js 1 debug

// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-v2-api.js 1 debug >> ./doordash-thread-1-api.log 2>&1
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread-v2-api.js 1 debug >> ./doordash-thread-1-api.log 2>&1

const constants = require('./config/constants');

const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const scraperUtils = require('./lib/scraper-utils');
const distance = require('./lib/distance');
const proxyLib = require('./lib/proxy');
const threadStats = require('./lib/thread-stats');
var pidusage = require('pidusage');

process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = 'DoorDash';
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
    if (val == 'debug') {
      DEBUG_MODE = true;
    }
  }
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

var nightmare;

var startDateTime = new Date();

// Main RUN Function
const run = function * () {
	
	const searchPoints = yield scraperUtils.getSearchPoints(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });	
	
	loggerLib.log('THREAD ' + THREAD_ID);
	
  if (!searchPoints || searchPoints.length == 0) {
    return;
  } 
  
  threadStats.initStats(WEBSITE_NAME, THREAD_ID);
	
	const nightmareConfig = {
	    waitTimeout: 15000,
	    executionTimeout: 600000,
		  show: false,
		  switches: {
					'ignore-certificate-errors': true
			},					  
		  webPreferences: {
		    devTools: false,
		    enableRemoteModule: false,
		    textAreasAreResizable: false,
	      defaultEncoding: 'utf8',
//		  	partition: "dd-partition-" + Math.random(),
	      partition: "nopersist",
		    webaudio: false,
		    images: false,
		    plugins: false,
		    webgl: false,    
		    webSecurity: false
		  }
	};
	
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
      parent.respondTo('proxy', function(host, done) {
          win.webContents.session.setProxy({
              proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
          }, function() {
              done();
          });
      });
      done();
  },
  function(host, done) {
      this.child.call('proxy', host, done);
  });
	
	nightmare = new Nightmare(nightmareConfig);
	
  nightmare.on('console', (log, msg) => {
//  	console.log(msg);
  	if (typeof msg === 'string' || msg instanceof String) {
  		if (msg.indexOf('CONSOLE:') >= 0) {
        loggerLib.log(msg.replace('CONSOLE:', '').trim());
      }
  	}
  });
	
	const curDateStr = (new Date()).toISOString().replace('T', ' ').split('.')[0].split(' ')[0]; 
	
	let homePageReload = true;
	let homePageReloaded = false;
	
	// Loop Search Points
	for (var spi = 0; spi < searchPoints.length; spi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let searchPoint = searchPoints[spi];
		
		loggerLib.log('SEARCH POINT START: ' + (spi+1) + '/' + searchPoints.length);
		
		let address = searchPoint.Address;
		
		loggerLib.log('ADDRESS START: ' + address);
		
		let addressResult = [];
			
		if (homePageReload) {
			let proxy = '';
			let proxyFound = false;
			while (!proxyFound) {
				proxy = proxyLib.getRandomUnusedProxy();
				loggerLib.log('Checking Proxy: ' + proxy);
				proxyFound = yield proxyLib.checkProxy(proxy).then(data => {
					loggerLib.log('Proxy OK');
					proxyLib.setProxyInUse(proxy);
					return true;
			  }).catch((error) => {
			  	loggerLib.log('Proxy ERROR');
			  	proxyLib.setProxyInUse(proxy);
			  	return false;
			  });
			}
			loggerLib.log('Using Proxy: ' + proxy);
			let userAgent = scraperUtils.getRandomUserAgentString();
			
			if (nightmare && homePageReloaded) {
				loggerLib.log('Clearing cookies');
			  yield nightmare.cookies.clearAll();
			}
			
			var homePageLoad = yield nightmare
				.proxy(proxy)
				.useragent(userAgent).viewport(1, 1)
				.filter({
			    urls: ['*://*/*']
			  }, function(details, cb) {
			  	if (
			  			details.url == 'https://www.doordash.com/' 
		  				|| details.url == 'https://api-consumer-client.doordash.com/graphql'
		  				|| details.url.indexOf('https://api.doordash.com/v2/restaurant/') === 0
		  				|| details.url.indexOf('https://api.doordash.com/v1/stores/') === 0
			  		 ) {
			  		return cb({});	
			  	} else {
			  		return cb({ cancel: true });	
			  	}
			  })
				.goto('https://www.doordash.com')
        .inject('js', 'inject-js/jquery.min.js')
        .inject('js', 'inject-js/moment-with-locales.min.js')
        .inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')	
        .wait('#root')
				.evaluate(function() {
					 return document.body.innerHTML.indexOf('id="root"') >= 0 || document.body.innerHTML.indexOf('id="ConsumerApp"') >= 0;
	     	 })				
				.catch(function(error) {
		    	threadStats.incrementErrorCount();
		    	console.log(error);
			    return false;
				});
			loggerLib.log('Home Page Load: ' + homePageLoad);
			if (!homePageLoad) {
				continue;
			}
			homePageReloaded = true;
			homePageReload = false;
		}			
		
		// Get Search Queries From Job Description
		const searchQueries = scraperUtils.getSearchQueries(searchPoint);		
			
		var result = yield nightmare
							 .evaluate((addressTimeZoneSearchQueries, done) => {
							   
							   var result = {};
                 result.rests = [];
                 result.errors = [];
                 
							  function go() {
									
							  	if (typeof window == 'undefined' || window == null) {
							  		console.log('CONSOLE: ERROR: window is not defined');
							  		done(null, result);	
							  	}
							  	
							  	document.body.style.display = 'none';
							  	
							  	window.address = addressTimeZoneSearchQueries.address;
							  	
									function getCookie(name) {
									  var value = "; " + document.cookie;
									  var parts = value.split("; " + name + "=");
									  if (parts.length == 2) return parts.pop().split(";").shift();
									}
									
									// AddConsumerAddress Start ------------------------------------------
									
									console.log('CONSOLE: ' + window.address + '| AddConsumerAddress Start');
									
						 		 	var params = {};
						 		 	params.operationName = 'addConsumerAddress';
						 		 	params.query = 'mutation addConsumerAddress($lat: Float!, $lng: Float!, $city: String!, $state: String!, $zipCode: String!, $printableAddress: String!, $shortname: String!, $googlePlaceId: String!, $subpremise: String, $driverInstructions: String) {\n  addConsumerAddress(lat: $lat, lng: $lng, city: $city, state: $state, zipCode: $zipCode, printableAddress: $printableAddress, shortname: $shortname, googlePlaceId: $googlePlaceId, subpremise: $subpremise, driverInstructions: $driverInstructions) {\n    ...ConsumerFragment\n    __typename\n  }\n}\n\nfragment ConsumerFragment on Consumer {\n  id\n  timezone\n  firstName\n  lastName\n  email\n  phoneNumber\n  receiveTextNotifications\n  defaultCountry\n  isGuest\n  scheduledDeliveryTime\n  socialAccounts\n  referrerAmount {\n    unitAmount\n    __typename\n  }\n  defaultAddress {\n    ...DefaultAddressFragment\n    __typename\n  }\n  availableAddresses {\n    ...DefaultAddressFragment\n    __typename\n  }\n  defaultAddressDistrict {\n    ...DefaultAddressDistrictFragment\n    __typename\n  }\n  orderCart {\n    ...OrderCartFragment\n    __typename\n  }\n  activeSubscription {\n    ...SubscriptionFragment\n    __typename\n  }\n  allSubscriptionPlans {\n    ...ConsumerSubscriptionPlanFragment\n    __typename\n  }\n  __typename\n}\n\nfragment DefaultAddressFragment on DefaultAddress {\n  id\n  street\n  city\n  subpremise\n  state\n  zipCode\n  lat\n  lng\n  timezone\n  shortname\n  printableAddress\n  driverInstructions\n  __typename\n}\n\nfragment DefaultAddressDistrictFragment on DefaultAddressDistrict {\n  id\n  name\n  shortname\n  isOpenForAsapDelivery\n  deliveryTimes\n  __typename\n}\n\nfragment OrderCartFragment on OrderCart {\n  id\n  isConsumerPickup\n  asapTimeRange\n  asapPickupTimeRange\n  subtotal\n  total\n  taxAmount\n  tipPercentageArgument\n  deliveryFee\n  originalDeliveryFee\n  appliedServiceFee\n  minOrderFee\n  extraSosDeliveryFee\n  urlCode\n  groupCart\n  shortenedUrl\n  maxIndividualCost\n  creator {\n    id\n    firstName\n    lastName\n    __typename\n  }\n  deliveries {\n    id\n    quotedDeliveryTime\n    __typename\n  }\n  tipSuggestions {\n    type\n    defaultIndex\n    values\n    __typename\n  }\n  restaurant {\n    id\n    maxOrderSize\n    coverImgUrl\n    slug\n    address {\n      printableAddress\n      lat\n      lng\n      __typename\n    }\n    business {\n      name\n      __typename\n    }\n    merchantPromotions {\n      minimumOrderCartSubtotal\n      newStoreCustomersOnly\n      deliveryFee\n      __typename\n    }\n    __typename\n  }\n  orders {\n    ...OrdersFragment\n    __typename\n  }\n  discountDetails {\n    ...DiscountDetailsFragment\n    __typename\n  }\n  __typename\n}\n\nfragment OrdersFragment on Order {\n  id\n  consumer {\n    firstName\n    lastName\n    id\n    __typename\n  }\n  orderItems {\n    id\n    options {\n      id\n      name\n      __typename\n    }\n    specialInstructions\n    substitutionPreference\n    quantity\n    singlePrice\n    item {\n      id\n      name\n      price\n      category {\n        title\n        __typename\n      }\n      extras {\n        id\n        title\n        description\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment DiscountDetailsFragment on DiscountDetails {\n  message\n  appliedDiscount {\n    displayString\n    unitAmount\n    __typename\n  }\n  requiredAction\n  __typename\n}\n\nfragment SubscriptionFragment on Subscription {\n  subscriptionStatus\n  id\n  subscriptionPlan {\n    allowAllStores\n    id\n    numEligibleStores\n    __typename\n  }\n  __typename\n}\n\nfragment ConsumerSubscriptionPlanFragment on ConsumerSubscriptionPlan {\n  allowAllStores\n  id\n  numEligibleStores\n  __typename\n}\n';
						 		 	params.variables = {};
						 		 	params.variables.googlePlaceId = '';
						 		 	params.variables.city = '';
						 		 	params.variables.state = '';
						 		 	params.variables.street = '';
						 		 	params.variables.zipCode = '';
						 		 	params.variables.shortname = '';
						 		 	params.variables.printableAddress = '';
						 		 	params.variables.lat = parseFloat(addressTimeZoneSearchQueries.addressLatLng.lat);
						 		 	params.variables.lng = parseFloat(addressTimeZoneSearchQueries.addressLatLng.lng);
						 		 	
                  var addAddressResponse = $.ajax({
                    url: "https://api-consumer-client.doordash.com/graphql",
                    method: "POST",
                    dataType: "json",
                    data: JSON.stringify(params),
                    cache: false,
                    crossDomain: true,
                    contentType: "application/json",
                    beforeSend: function(xhr) {
                      xhr.setRequestHeader('Accept', '*/*');
                      xhr.setRequestHeader('Cache-Control', 'max-age=0');
                      xhr.setRequestHeader('X-CSRFToken', getCookie('csrf_token'));
                    },
                    xhrFields: {
                      withCredentials: true
                    },
                    async: false,
                    timeout: 30000
                  }).responseText;

                  var validResponse = true;
                  if (addAddressResponse) {
                    addAddressResponse = JSON.parse(addAddressResponse);
                    if (addAddressResponse.data && addAddressResponse.data.addConsumerAddress && addAddressResponse.data.addConsumerAddress.defaultAddress) {
                      if (addAddressResponse.data.addConsumerAddress.defaultAddress.lat != parseFloat(addressTimeZoneSearchQueries.addressLatLng.lat)) {
                        validResponse = false;
                      }
                      if (addAddressResponse.data.addConsumerAddress.defaultAddress.lng != parseFloat(addressTimeZoneSearchQueries.addressLatLng.lng)) {
                        validResponse = false;
                      }
                    }
                  }
                  addAddressResponse = undefined;
                  
                  console.log('CONSOLE: ' + window.address + '| AddConsumerAddress End. Valid: ' + (validResponse?'Yes':'No'));
                  
									// AddConsumerAddress End ------------------------------------------
                  
                  
                  // StoreSearch Start ------------------------------------------	
                  
              		var categories = addressTimeZoneSearchQueries.searchQueries;
					 				searchQueriesWithEmpty = [null];
					 				searchQueriesWithEmpty.push(...categories);                  
					 				
					 				var stores = [];
                  
                  if (validResponse) {
                  	
                  	console.log('CONSOLE: ' + window.address + '| StoreSearch Start');
                  	
                    var params2 = {};
                    params2.operationName = 'storeSearch';
                    params2.query = 'query storeSearch($offset: Int!, $limit: Int!, $order: [String!], $searchQuery: String, $filterQuery: String, $categoryQueryId: ID, $isStoreFeedExperiment: Boolean, $isConsumerSearchExperiment: Boolean, $isStoreFeedFilterExperiment: Boolean) {\n  storeSearch(offset: $offset, limit: $limit, order: $order, searchQuery: $searchQuery, filterQuery: $filterQuery, categoryQueryId: $categoryQueryId, isStoreFeedExperiment: $isStoreFeedExperiment, isConsumerSearchExperiment: $isConsumerSearchExperiment, isStoreFeedFilterExperiment: $isStoreFeedFilterExperiment) {\n    showListAsPickup\n    numStores\n    stores {\n      id\n      name\n      description\n      averageRating\n      numRatings\n      priceRange\n      featuredCategoryDescription\n      deliveryFee\n      extraSosDeliveryFee\n      displayDeliveryFee\n      headerImgUrl\n      url\n      isConsumerSubscriptionEligible\n      distanceFromConsumer\n      menus {\n        popularItems {\n          imgUrl\n          __typename\n        }\n        __typename\n      }\n      merchantPromotions {\n        id\n        categoryNewStoreCustomersOnly\n        deliveryFee\n        minimumSubtotal\n        __typename\n      }\n      status {\n        unavailableReason\n        asapAvailable\n        scheduledAvailable\n        asapMinutesRange\n        asapPickupMinutesRange\n        __typename\n      }\n      __typename\n    }\n    storeItems {\n      id\n      name\n      price\n      imageUrl\n      store {\n        name\n        url\n        id\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n';
                    params2.variables = {};
                    params2.variables.offset = 0;
                    params2.variables.limit = 48;
                    params2.variables.searchQuery = null;
                    params2.variables.filterQuery = null;
                    params2.variables.categoryQueryId = null;
                    params2.variables.isStoreFeedExperiment = true;
                    params2.variables.isConsumerSearchExperiment = true;
                    params2.variables.isStoreFeedFilterExperiment = true;
                    
                    for (var sqi = 0; sqi < searchQueriesWithEmpty.length; sqi++) {
                    
                    	params2.variables.searchQuery = searchQueriesWithEmpty[sqi];
                    	if (params2.variables.searchQuery) {
                    		params2.variables.searchQuery = params2.variables.searchQuery.toLowerCase().trim();
                    	}
                    	
                    	console.log('CONSOLE: ' + window.address + '| SearchQuery: ' + params2.variables.searchQuery);
                    	
	                    var storeSearchResponse = $.ajax({
	                      url: "https://api-consumer-client.doordash.com/graphql",
	                      method: "POST",
	                      dataType: "json",
	                      data: JSON.stringify(params2),
	                      cache: false,
	                      crossDomain: true,
	                      contentType: "application/json",
	                      beforeSend: function(xhr) {
	                        xhr.setRequestHeader('Accept', '*/*');
	                        xhr.setRequestHeader('Cache-Control', 'max-age=0');
	                        xhr.setRequestHeader('X-CSRFToken', getCookie('csrf_token'));
	                      },
	                      xhrFields: {
	                        withCredentials: true
	                      },
	                      async: false,
	                      timeout: 30000
	                    }).responseText;
	                    
	                    if (storeSearchResponse) {
	                      storeSearchResponse = JSON.parse(storeSearchResponse);
	                      if (storeSearchResponse.data && storeSearchResponse.data.storeSearch) {
	                      	
	                      	console.log('CONSOLE: ' + window.address + '| StoreSearch Done. Results: ' + storeSearchResponse.data.storeSearch.stores.length);
	                      	
	                      	for (let sti = 0; sti < storeSearchResponse.data.storeSearch.stores.length; sti++) {
									    			let store = storeSearchResponse.data.storeSearch.stores[sti];
									    			if (store.status.asapAvailable) {
									    				store.searchQuery = params2.variables.searchQuery;
									    				store.position = sti + 1;
									    				stores.push(store);
									    				store = undefined;
									    			}
									    			if (sti >= 19) {
									    				break;
									    			}
									    		}
	                      	
	                      	if (params2.variables.searchQuery == null && stores.length == 0) {
                						var resultToSave = {};
                						resultToSave.request = 'storeSearch';
                						resultToSave.msg = 'No restaurants found for empty search query';
                						result.errors.push(resultToSave);
                						console.log('CONSOLE: ' + window.address + '| StoreSearch - No Open Rests Found');
                	 					done(null, result);
                	 					return;
	                      	}	                      	
	                      	
	                      }
	                      
	                      storeSearchResponse = undefined;
	                    }
                    }
                    
                  } else {
                    result.errorMsg = 'ERROR: Invalid addConsumerAddress Lat/Lng pair!';
                    result.obj = 'Sent Lat: ' + parseFloat(addressTimeZoneSearchQueries.addressLatLng.lat) 
                    + ', Received Lat: ' + addAddressResponse.data.addConsumerAddress.defaultAddress.lat 
                    + ', Sent Lng: ' + parseFloat(addressTimeZoneSearchQueries.addressLatLng.lng)
                    + ', Received Lng: ' + addAddressResponse.data.addConsumerAddress.defaultAddress.lng;
                  }

                  // StoreSearch End ------------------------------------------	
                  
                  // Store Details Start ------------------------------------------
                  
                  if (stores.length > 0) {
                  	
                  	console.log('CONSOLE: ' + window.address + '| StoreDetails Start. Total Rests: ' + stores.length);
                  	
                  	var storeDetailsResponseCache = {};
                  	
//                  	Object.defineProperty(Array.prototype, 'chunk_inefficient', {
//                      value: function(chunkSize) {
//                          var array=this;
//                          return [].concat.apply([],
//                              array.map(function(elem,i) {
//                                  return i%chunkSize ? [] : [array.slice(i,i+chunkSize)];
//                              })
//                          );
//                      }
//                  });                  	
                  	
//                  	var uniqueIds = [];
//                  	for (var usi = 0; usi < stores.length; usi++) {
//                  		if (uniqueIds.indexOf(stores[usi].id) < 0) {
//                  			uniqueIds.push(stores[usi].id);
//                  		}
//                  	}
//                  	uniqueIds = uniqueIds.chunk_inefficient(3);
//                  	
//                  	console.log(uniqueIds);
//                  	
//                  	for (var idi = 0; idi < uniqueIds.length; idi++) {
//                  		var uniqueIdGroup = uniqueIds[idi];
//                  	}
                  	
//                  	async(function *() {
//                  	  try {
//                  	    var resultPromise1 = $.ajax("/request1");
//                  	    var resultPromise2 = $.ajax("/request2");
//                  	    var resultPromise3 = $.ajax("/request3");
//                  	    // Do something with the results
//                  	    let results = {"1": yield resultPromise1, "2": yield resultPromise2, "3": yield resultPromise3}
//                  	    console.log(results);
//                  	  } catch(xhr) {
//                  	    console.log("Error: " + xhr);
//                  	  }
//                  	});
                  	
	                  
	                  var distanceResponseCache = {};
	                  for (var si = 0; si < stores.length; si++) {
	                  	var store = stores[si];
	                  	
	        						var storeDetailsResponse = '';
	        						
	        						if (!storeDetailsResponseCache[store.id]) {
	        							
	        							storeDetailsResponse = $.ajax({
												  url: "https://api.doordash.com/v2/restaurant/" + store.id + "/",
												  method: "GET",
												  dataType: "json",
												  cache: false,
												  crossDomain: true,
												  contentType: "application/json",
												  beforeSend: function(xhr) {
												  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
												  },
											    xhrFields: {
											    	withCredentials: true
											    },
	        								async: false,
	        								timeout: 8000
												}).responseText;
	        							
	        							storeDetailsResponseCache[store.id] = storeDetailsResponse;
	        						} else {
	        							storeDetailsResponse = storeDetailsResponseCache[store.id];
	        						}
	        								                  	
	        						if (storeDetailsResponse) {
	        					  	try {
	        					  		storeDetailsResponse = JSON.parse(storeDetailsResponse);
	        					  		
	        					  		console.log('CONSOLE: ' + window.address + '| StoreDetails End. ' + (si+1) + '/' + stores.length);
	        					  		
		        					  	var resultToSave = {};
		        					  	resultToSave.id = store.id;
										    	resultToSave.name = storeDetailsResponse.business.name;
										    	resultToSave.restaurant_address = storeDetailsResponse.address.printable_address;
										    	resultToSave.restaurant_lat = storeDetailsResponse.address.lat;
										    	resultToSave.restaurant_long = storeDetailsResponse.address.lng;
										    	
										    	if (!distanceResponseCache[store.id]) {
											    	var distanceResponse = $.ajax({
														  url: "https://api.doordash.com/v1/stores/" + store.id + "/?fields=distance_from_consumer&consumer=me",
														  method: "GET",
														  dataType: "json",
														  cache: false,
														  crossDomain: true,
														  contentType: "application/json",
														  beforeSend: function(xhr) {
														  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
														  },
													    xhrFields: {
													    	withCredentials: true
													    },
			        								async: false,
			        								timeout: 8000
														}).responseText;
											    	distanceResponseCache[store.id] = distanceResponse;
			        						} else {
			        							distanceResponse = distanceResponseCache[store.id];
			        						}
										    	if (distanceResponse) {
										    		distanceResponse = JSON.parse(distanceResponse);
										    		if (distanceResponse && typeof distanceResponse.distance_from_consumer != 'undefined') {
										    			resultToSave.distance_competitor_displayed = distanceResponse.distance_from_consumer; 
										    		}
										    		distanceResponse = undefined; 
										    	}
										    	
										      resultToSave.searchQuery = store.searchQuery;
							 					  resultToSave.href = 'https://www.doordash.com/store/' + storeDetailsResponse.slug + '-' + storeDetailsResponse.id + '/';
							 				  	resultToSave.position = store.position;
							 				  	
								  	  		resultToSave.price = storeDetailsResponse.price_range;	
									  	  	resultToSave.cuisine = storeDetailsResponse.tags.join('|');
									  	  	
									  	  	var eta = storeDetailsResponse.status;
									  	  	if (eta.indexOf('mins') >= 0) {
									  	  		eta = eta.replace('mins', '').trim();;
									  	  		eta = eta.split('-');
									  	  		if (eta.length == 2) {
											  	  	resultToSave.eta = eta[0].trim() + '-' + eta[1].trim();
											  	  	resultToSave.etaMin = eta[0].trim();
											  	  	resultToSave.etaMax = eta[1].trim();																			  	  			
									  	  		}
									  	  	}
									  	  	
								  	  		resultToSave.deliverySearchPageFee = storeDetailsResponse.delivery_fee / 100;
								  	  		resultToSave.deliveryMenuPageFee = storeDetailsResponse.delivery_fee / 100;
								  	  		resultToSave.deliveryCheckoutPageFee = storeDetailsResponse.delivery_fee / 100;
								  	  		resultToSave.deliveryFeeWithoutDiscounts = storeDetailsResponse.delivery_fee_details.original_fee.unit_amount / 100;
								  	  		resultToSave.serviceFee = storeDetailsResponse.service_rate;
								  	  		
								  	  		let freeDeliveryMinOrder = null;
								  	  		let merchantPromotions = storeDetailsResponse.merchant_promotions;
								  	  		for (let mpi = 0; mpi < merchantPromotions.length; mpi++) {
								  	  			if (merchantPromotions[mpi].delivery_fee === 0 && merchantPromotions[mpi].minimum_order_cart_subtotal) {
								  	  				freeDeliveryMinOrder = merchantPromotions[mpi].minimum_order_cart_subtotal / 100;
								  	  			}
								  	  		}
								  	  		if (freeDeliveryMinOrder) {
								  	  			resultToSave.freeDeliveryMinOrder = freeDeliveryMinOrder;
								  	  		}
								  	  		
								  	  		store.position = undefined;
								  	  		store.searchQuery = undefined;;
								  	  		resultToSave.restaurant_list_raw = JSON.stringify(store);
								  	  		store = undefined; 
								  	  		resultToSave.restaurant_details_raw = JSON.stringify(storeDetailsResponse);
								  	  		
									  	  	resultToSave.dateTimeUTC0 = (new Date()).toISOString(); 						  	  	
									  	  	resultToSave.dateTimeLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD hh:mm:ss');
									  	  	resultToSave.dateLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD');
									  	  	resultToSave.hourLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('HH');
									  	  	resultToSave.dayLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('dddd');
								  	  		
										    	result.rests.push(resultToSave);
										    	resultToSave = undefined;
	        					  	}	catch (error) {
	        								console.log('CONSOLE: ERROR: StoreDetailsRequest: ' + error);
	        							}
	        					  	storeDetailsResponse = undefined;
	        						} else {
	        							console.log('CONSOLE: ERROR: StoreDetailsRequest: No or invalid response');
	        						}
	                  	
	                  }
	                  
	                  distanceResponseCache = undefined;
	                  storeDetailsResponseCache = undefined;
	                  stores = undefined;
                  }
                  // Store Details End ------------------------------------------
							    }
							    go();
							    go = undefined;
                  done(null, result);
									return;		
								}, {address: address, addressLatLng: {lat: searchPoint.Lat, lng: searchPoint.Lng}, timeZone: searchPoint.TimeZone, searchQueries: searchQueries})
							  .catch(function(error) {
							  	threadStats.incrementErrorCount();
						      loggerLib.log(error);
							    homePageReload = true;
						  		return {};
						  	});
		
		
		if (spi > 0 && spi < searchPoints.length - 1 && spi % 3 == 0) {
			loggerLib.log('Refresh Start');	
			try {
				homePageReload = yield nightmare
					.refresh()
					.wait(5)
		      .inject('js', 'inject-js/jquery.min.js')
		      .inject('js', 'inject-js/moment-with-locales.min.js')
		      .inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
		      .wait('#root')
		      .wait(5)
		      .then(() => {
				  	return true;
			  	})			
					.catch(function(error) {
			    	threadStats.incrementErrorCount();
			    	console.log(error);
				    return false;
					});
				homePageReload = !homePageReload;
				loggerLib.log('Refresh End. Reload HOME: ' + homePageReload);
			} catch (e) {
				threadStats.incrementErrorCount();
	      loggerLib.log(e);
	      homePageReload = true;
			}
		}
		
  	if (typeof result.errors != 'undefined' && result.errors.length > 0) {
  		for (let eri = 0; eri < result.errors.length; eri++) {
  			loggerLib.log(result.errors[eri]);	
  		}
  		if (result.errors.length > 1 || result.errors[0].msg != 'No restaurants found for empty search query') {
  			threadStats.incrementErrorCount();
  			homePageReload = true;	
  		}
  		delete result.errors;
  	}
  	
  	var roadDistanceCalculatedRestCount = 0;
  	if (typeof result.rests != 'undefined' && result.rests.length > 0) {

			for (var ri = 0; ri < result.rests.length; ri++) {
				result.rests[ri].site = WEBSITE_NAME;
				result.rests[ri].address = address;
				result.rests[ri].address_lat = searchPoint.Lat;
				result.rests[ri].address_long = searchPoint.Lng;				
				result.rests[ri].market = searchPoint.CBSA;
				result.rests[ri].region_name = searchPoint.Region;
				
				result.rests[ri].distance_straight_line = distance.getStraighLineDistance(
						{lat: parseFloat(result.rests[ri].address_lat), lon: parseFloat(result.rests[ri].address_long)},
						{lat: parseFloat(result.rests[ri].restaurant_lat), lon: parseFloat(result.rests[ri].restaurant_long)}
				);
				
				if (!result.rests[ri].distance_actual && !result.rests[ri].distance_duration) {
				  var roadDistanceResponse =  yield distance.getRoadDistance(
							{lat: parseFloat(result.rests[ri].address_lat), lng: parseFloat(result.rests[ri].address_long)},
							{lat: parseFloat(result.rests[ri].restaurant_lat), lng: parseFloat(result.rests[ri].restaurant_long)}
					).then(data => {
				    return data;
				  }).catch((error) => {
	//			  	loggerLib.log(error);
				    return null;
				  });
				  if (roadDistanceResponse) {
				  	result.rests[ri].distance_actual = roadDistanceResponse.distance;
				  	result.rests[ri].distance_duration = roadDistanceResponse.duration;
				  	for (var riii = 0; riii < result.rests.length; riii++) {
							if (result.rests[riii].id == result.rests[ri].id) {
								if (!result.rests[riii].distance_actual && !result.rests[riii].distance_duration) {
									result.rests[riii].distance_actual = result.rests[ri].distance_actual; 
									result.rests[riii].distance_duration = result.rests[ri].distance_duration;
								}
							}
						}				  	
				  	roadDistanceCalculatedRestCount++;
				  }	
				} else {
					roadDistanceCalculatedRestCount++;
				}
				
			}
			
			if (result.rests.length > 0) {
				var roadDistanceCalculatedPercent = Math.round((roadDistanceCalculatedRestCount / result.rests.length) * 100);
				loggerLib.log('Road Distance Calculated for ' + roadDistanceCalculatedRestCount + ' rests out of ' + result.rests.length + '|' + roadDistanceCalculatedPercent + '%');
			}
			
	  	threadStats.processSearchPointResults(result.rests);
	  	if (!DEBUG_MODE) {
	  		if (result.rests.length > 0) {
	  			if ((spi+1) == searchPoints.length) {
	  				resultsLib.saveToCSV(WEBSITE_NAME, result.rests, THREAD_ID, startDateTime, true);	  				
	  			} else {
	  				resultsLib.saveToCSV(WEBSITE_NAME, result.rests, THREAD_ID, startDateTime);	
	  			}
	  		}			
	  	}
  		
			loggerLib.log('ADDRESS END|' + address + '|Results: ' + result.rests.length);
			
			result = undefined;
	  } else {
	  	loggerLib.log('ADDRESS END|' + address + '|NO Results');
	  }
  	
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));		
		let electornMemory = yield pidusage(nightmare.proc.pid);
		electornMemory = Math.round((electornMemory.memory / 1024 / 1024) * 100) / 100
		loggerLib.log('SEARCH POINT END: ' + (spi+1) + '/' + searchPoints.length + ' Time: ' + jobTime + 'sec Memory: ' + usedMemory + 'mb Electron Memory: ' + electornMemory + 'mb');
	}
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
 	threadStats.setExecutionTime(executionTime);
 	if (!DEBUG_MODE) {
 	  threadStats.saveThreadStatsToFile();  
 	}
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }	
	
  loggerLib.log('Process exit');
	process.exit();
});