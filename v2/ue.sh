#!/bin/bash

  sleep 3

  for a in `seq 1 130`
  do
    xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony --max_old_space_size=2048 ubereats-thread-v2-api.js $a >> ./logs/ubereats-thread-$a-api.log 2>&1 &
    sleep 1
  done  