#!/bin/bash


# This will start 315 processes
for ind in `seq 0 15 300`
do
  for disp in `seq 1 15`
  do
    b=$(expr $ind + $disp)
    scr1=$(expr $disp - 1)
    scr2=$(expr $scr1 + 1)
    echo $disp $scr1;
    echo $disp $scr2;
    echo -------------;
#    export DISPLAY=:$screen.0; node --harmony --max-old-space-size=2048 doordash-thread-v2-api.js $b >> ./logs/doordash-thread-$b-api.log 2>&1 &
#    export DISPLAY=:$screen.$screen; node --harmony --max-old-space-size=2048 ubereats-thread-v2-api.js $b >> ./logs/ubereats-thread-$b-api.log 2>&1 &
  done
done 