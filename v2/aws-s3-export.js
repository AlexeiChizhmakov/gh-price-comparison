// npm install fast-csv
// npm install stream-concat
// npm install aws-sdk
// node --harmony aws-s3-export.js
// node --harmony aws-s3-export.js &>> ./logs/s3-export.log&

const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const fs = require('fs');
const csv = require('fast-csv');
var StreamConcat = require('stream-concat');
const AWS = require('aws-sdk');
const cloudWatchLib = require("./lib/cloudwatch");
const threadStats = require('./lib/thread-stats');

// TIMER Start
let start = process.hrtime();

var vo = require('vo');

loggerLib.webSite = 'S3-EXPORT';
loggerLib.threadId = 0;

loggerLib.log('--- START');

// Main RUN Function
const run = function * () {
	
	const currentDate = ((new Date()).toISOString().split('T')[0]);
	const currentHour = ((new Date()).toISOString().split('T')[1].split(':')[0]);
	
	if (fs.existsSync(threadStats.threadStatsFilePath)) {
		var websites = [constants.GRUBHUB_WEBSITE_NAME, constants.UBEREATS_WEBSITE_NAME, constants.DOORDASH_WEBSITE_NAME];
		for (var wi = 0; wi < websites.length; wi++) {
			var website = websites[wi];
			var threadStatsResult = yield threadStats.getStatsForWebsite(website);
			yield cloudWatchLib.logMetric(website, 'V2. Completed Threads', threadStatsResult.completed_thread_count);
			yield cloudWatchLib.logMetric(website, 'V2. Total Results', threadStatsResult.total_results);
			yield cloudWatchLib.logMetric(website, 'V2. Total Errors V2', threadStatsResult.total_error_count);
			yield cloudWatchLib.logMetric(website, 'V2. AVG Delivery Fee', threadStatsResult.avg_delivery_fee);
			yield cloudWatchLib.logMetric(website, 'V2. AVG Max ETA', threadStatsResult.avg_max_eta);
			yield cloudWatchLib.logMetric(website, 'V2. Road Distance Percent', threadStatsResult.road_distances_calculated_percent);
			yield cloudWatchLib.logMetric(website, 'V2. AVG Thread Execution Time', threadStatsResult.avg_execution_time);
			yield cloudWatchLib.logMetric(website, 'V2. Max Thread Execution Time', threadStatsResult.max_execution_time);
		}
		fs.unlinkSync(threadStats.threadStatsFilePath);	
	}
	
	// Step 1. Combine All Result CSV files into single Output file. ------------------------------------------------------------	

	const resultFileName = 'Results' + '-' + currentDate + '-' + currentHour + '.csv';
	
	if (!fs.existsSync(constants.EXPORT_S3_FOLDER + resultFileName)) {
	
		const processedResultFiles = [];
		const processedResultFileStats = [];
		fs.readdirSync(constants.CURRENT_RESULTS_FOLDER).map(function(file) {
			processedResultFiles.push(file);
		});
		
		loggerLib.log('Processing ' + '(' + processedResultFiles.length + ')' + ' Result Files: ' + processedResultFiles.join(', '));
		
		if (processedResultFiles.length > 0) {
			
			yield (function() {
				return new Promise((res, rej) => {
					let fileNames = processedResultFiles;
					let fileIndex = 0;
					let nextStream = function() {
					  if (fileIndex === fileNames.length) return null;
					  return csv.fromStream(fs.createReadStream(constants.CURRENT_RESULTS_FOLDER + fileNames[fileIndex++]), {headers : true});
					};
					let combinedStream = new StreamConcat(nextStream, {objectMode: true});
					combinedStream
						.pipe(csv.createWriteStream({headers: true}))
						.pipe(fs.createWriteStream(constants.EXPORT_S3_FOLDER + resultFileName))
						.on('finish', () => {
							 res();
						 })
						.on('error', () => {
							rej();
						});			
				});
			})();
			
			processedResultFiles.map(function(file) {
				fs.unlinkSync(constants.CURRENT_RESULTS_FOLDER + file);	
			});
			
			let fileSize = fs.statSync(constants.EXPORT_S3_FOLDER + resultFileName).size / 1000000.0;
			loggerLib.log('Created Merged Result Output File: ' + resultFileName + ' (' + fileSize + ' MB)' + ' and removed thread result files');
		}
	
	}
	
	
	// Step 2. Combine All Log .log files into single Output file. ------------------------------------------------------------
	
	const logsFileName = 'Logs' + '-' + currentDate + '-' + currentHour + '.log';
	
	if (!fs.existsSync(constants.EXPORT_S3_FOLDER + logsFileName)) {
	
		const processedLogFiles = [];
		
		fs.readdirSync(constants.CURRENT_LOGS_FOLDER).map(function(file) {
			processedLogFiles.push(file);
		});	
		
		loggerLib.log('Processing ' + '(' + processedLogFiles.length + ')' + ' Log Files: ' + processedLogFiles.join(', '));
		
		if (processedLogFiles.length > 0) {
			
			
			yield (function() {
				return new Promise((res, rej) => {
					let fileNames = processedLogFiles;
					let fileIndex = 0;
					let nextStream = function() {
					  if (fileIndex === fileNames.length) return null;
					  return fs.createReadStream(constants.CURRENT_LOGS_FOLDER + fileNames[fileIndex++]);
					};
					let combinedStream = new StreamConcat(nextStream);
					combinedStream
						.pipe(fs.createWriteStream(constants.EXPORT_S3_FOLDER + logsFileName))
						.on('finish', () => {
							 res();
						 })
						.on('error', () => {
							rej();
						});			
				});		
			})();
			
			
			processedLogFiles.map(function(file) {
				fs.unlinkSync(constants.CURRENT_LOGS_FOLDER + file);	
			});			
			
			let fileSize = fs.statSync(constants.EXPORT_S3_FOLDER + logsFileName).size / 1000000.0;
			loggerLib.log('Created Merged Log Output File: ' + logsFileName + ' (' + fileSize + ' MB)' + ' and removed thread log files');		
		}
		
	}
	
	// Step 3. Upload All Result files to S3 ------------------------------------------------------------
	
	const filesToExport = fs.readdirSync(constants.EXPORT_S3_FOLDER);
	
	if (filesToExport.length > 0) {
		loggerLib.log('Starting Upload to S3');
		
		AWS.config.update({
				maxRetries: 10,
				accessKeyId: constants.AWS_S3_ACCESS_KEY_ID,
				secretAccessKey: constants.AWS_S3_SECRET_ACCESS_KEY
			}
		);
		
		const s3 = new AWS.S3();
		
		for (let i = 0; i < filesToExport.length; i++) {
			let file = filesToExport[i];
			yield (function() {
				return new Promise((res, rej) => {
						let awsS3Folder = '';
						if (file.indexOf('.csv') > 0) {
							awsS3Folder = constants.AWS_S3_RESULTS_FOLDER;
						}
						if (file.indexOf('.log') > 0) {
							awsS3Folder = constants.AWS_S3_LOGS_FOLDER;
						}
						
						let params = {
							Bucket: constants.AWS_S3_BUCKET,
							Key: awsS3Folder + file,
							Body: fs.createReadStream(constants.EXPORT_S3_FOLDER + file)
						};
						
						loggerLib.log('Uploading File to S3: [' + awsS3Folder + file + ']');
						s3.upload(params, function(err, data) {
							if (err) {
								loggerLib.log(err);
								rej();
							} else {
								fs.unlinkSync(constants.EXPORT_S3_FOLDER + file);
								loggerLib.log('Successfully Uploaded File to S3: [' + awsS3Folder + file + '] and removed local file');
								res();
							}
					});
				});
			})();
		}
	}
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
		loggerLib.log(error);
	}
	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', '', [executionTime]);
});