// node --harmony create-config.js

const vo = require('vo');
const json2csv = require('json2csv');
const csv = require('csvtojson');
const fs = require('fs');

function getCSVFileContents (filePath) {
  return new Promise((res, rej) => {
    let result = [];
    csv({
      delimiter: ",",
    })
    .fromFile(filePath)
    .on('json', (json) => {
      result.push(json);
    })
    .on('error', (error)=>{
      rej(error);
    })
    .on('end', () => {
      res(result);  
    });
  });
}

var resultsGlobal = [];

//TIMER Start
let start = process.hrtime();

// Main RUN Function
const run = function * () {
  
	
	var addressCoords = yield getCSVFileContents('./address_coordinates.csv');
  var priceConfig = yield getCSVFileContents('./config1.csv');
  var newPriceConfig = yield getCSVFileContents('./config2.csv');
  var countConfig = yield getCSVFileContents('./config3.csv');
  
  for (var i = 0; i < countConfig.length; i++) {
  	var countConfigRow = countConfig[i];
  	
  	for (var l = 1; l <= 12; l++) {
  		if (countConfigRow['Location ' + l]) {
	  		var newRow = {};
	  		newRow.CBSA = countConfigRow.CBSA;
	  		newRow.TimeZone = countConfigRow.TimeZone;
	  		newRow.Address = countConfigRow['Location ' + l];
	  		
	  		for (var a = 0; a < addressCoords.length; a++) {
	  			if (addressCoords[a].Address == newRow.Address) {
	  				newRow.Lat = addressCoords[a].Lat;
  					newRow.Lng =  addressCoords[a].Lng;
	  			}
	  		}
	  		
	  		for (var b = 0; b < priceConfig.length; b++) {
	  			var priceConfigRow = priceConfig[b];
	  			for (var l1 = 1; l1 <= 12; l1++) {
	  				if (priceConfigRow['Location ' + l1] == newRow.Address) {
	  					for (var q = 1; q <= 10; q++) {
	  						newRow['Query' + q] = priceConfigRow['Query ' + q];
	  					}
	  				}
	  			}
	  		}
	  		
	  		resultsGlobal.push(newRow);
  		}
  	}
  }
  
  for (var n = 0; n < newPriceConfig.length; n++) {
  	var newPriceConfigRow = newPriceConfig[n];
  	console.log(newPriceConfigRow);
		newRow = {};
		newRow.CBSA = newPriceConfigRow.region_name;
		newRow.Address = newPriceConfigRow.address;
		newRow.Lat = newPriceConfigRow.lat_long.split(',')[0].replace('(', '').trim();
		newRow.Lng = newPriceConfigRow.lat_long.split(',')[1].replace(')', '').trim();
		resultsGlobal.push(newRow);
  }
  
  const fields = [
                  'CBSA', 'TimeZone', 'Address', 'Lat', 'Lng', 'GrubHubThreadID', 'UberEatsThreadID', 'DoorDashThreadID',
                  'Query1', 'Query2', 'Query3', 'Query3', 'Query4', 'Query5', 'Query6', 'Query7', 'Query8', 'Query9', 'Query10'
                 ];
  
  if (resultsGlobal.length > 0) {
	  const fName = './final-config.csv';
		if (fs.existsSync(fName)) {
			fs.unlinkSync(fName);
		}
		fs.writeFileSync(fName, json2csv({data: resultsGlobal, fields: fields}), 'ascii');
  }
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		console.log(error);
	}
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
 	console.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
  process.exit();
});