#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/home/admin/.nvm/versions/node/v6.11.5/bin
HOME=/home/admin

node --harmony clean-used-proxies.js

sleep 1
  
for displ in `seq 1 16`
do
  /usr/bin/Xvfb :$displ -maxclients 2048 -screen 0 1x1x8 -screen 1 1x1x8 -screen 2 1x1x8 -screen 3 1x1x8 -screen 4 1x1x8 -screen 5 1x1x8 -screen 6 1x1x8 -screen 7 1x1x8 -screen 8 1x1x8 -screen 9 1x1x8 -screen 10 1x1x8 -screen 11 1x1x8 -screen 12 1x1x8 -screen 13 1x1x8 -screen 14 1x1x8 -screen 15 1x1x8 >/tmp/xvfb-$displ.log 2>&1 &
  sleep 1  
done 

sleep 5

# This will start 330 processes
for ind in `seq 0 15 315`
do
  for disp in `seq 1 15`
  do
    b=$(expr $ind + $disp)
    scr1=$(expr $disp - 1)
    scr2=$(expr $scr1 + 1)
    disp2=$(expr $disp + 1)
    export DISPLAY=:$disp.$scr1; node --harmony --max-old-space-size=2048 doordash-thread-v2-api.js $b >> ./logs/doordash-thread-$b-api.log 2>&1 &
    export DISPLAY=:$disp2.$scr2; node --harmony --max-old-space-size=2048 ubereats-thread-v2-api.js $b >> ./logs/ubereats-thread-$b-api.log 2>&1 &
  done
done 

sleep 1
 
for c in `seq 1 2`
do
  node --harmony --max-old-space-size=2048 grubhub-thread-v2-api.js $c >> ./logs/grubhub-thread-$c-api.log 2>&1 &
done  

sleep 120

node --harmony start-idle-processes.js