// node --harmony clean-used-proxies.js


const fs = require('fs');

fs.readdirSync('./proxy/').forEach(file => {
	if (file.indexOf('.proxy') >= 0) {
		fs.unlinkSync('./proxy/' + file);
	}
})
