#!/bin/bash


pkill node
sleep 1
pkill Xvfb
sleep 1
pkill electron
sleep 1


for displ in `seq 1 15`
do
  /usr/bin/Xvfb :$displ -maxclients 2048 -screen 0 1x1x8 -screen 1 1x1x8 -screen 2 1x1x8 -screen 3 1x1x8 -screen 4 1x1x8 -screen 5 1x1x8 -screen 6 1x1x8 -screen 7 1x1x8 -screen 8 1x1x8 -screen 9 1x1x8 -screen 10 1x1x8 -screen 11 1x1x8 -screen 12 1x1x8 -screen 13 1x1x8 -screen 14 1x1x8 -screen 15 1x1x8 >/tmp/xvfb-$displ.log 2>&1 &
  sleep 1  
done 


# This will start 315 processes
for ind in `seq 0 15 300`
do
  for screen in `seq 1 15`
  do
    b=$(expr $ind + $screen)
    export DISPLAY=:$screen.0; node --harmony --max-old-space-size=2048 doordash-thread-v2-api.js $b >> ./logs/doordash-thread-$b-api.log 2>&1 &
    export DISPLAY=:$screen.$screen; node --harmony --max-old-space-size=2048 ubereats-thread-v2-api.js $b >> ./logs/ubereats-thread-$b-api.log 2>&1 &
  done
done 