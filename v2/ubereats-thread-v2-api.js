// xvfb-run -a --server-args="-screen 0 1x1x8" node --harmony ubereats-thread-v2-api.js 1 debug
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-v2-api.js 1 debug
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-v2-api.js 1 debug

const constants = require('./config/constants');

const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const scraperUtils = require('./lib/scraper-utils');
const distance = require('./lib/distance');
const proxyLib = require('./lib/proxy');
const threadStats = require('./lib/thread-stats');
var pidusage = require('pidusage');

const slugid = require('slugid');
const util = require('util');

process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

// Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.UBEREATS_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

var nightmare;

var startDateTime = new Date();

// Main RUN Function
const run = function * () {
	
	const searchPoints = yield scraperUtils.getSearchPoints(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
	loggerLib.log('THREAD ' + THREAD_ID);
	
  if (!searchPoints || searchPoints.length == 0) {
    return;
  } 
  
  threadStats.initStats(WEBSITE_NAME, THREAD_ID);
  
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
    parent.respondTo('proxy', function(host, done) {
        win.webContents.session.setProxy({
            proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
        }, function() {
            done();
        });
    });
    done();
	},
	function(host, done) {
	    this.child.call('proxy', host, done);
	});  
  
	 nightmare = new Nightmare({
	    waitTimeout: 10000,
	    executionTimeout: 400000,
	    show: false,
		  switches: {
				'ignore-certificate-errors': true
		  },		    
	    webPreferences: {
	      devTools: false,
	      enableRemoteModule: false,
	      textAreasAreResizable: false,      
	      defaultEncoding: 'utf8',
	      partition: "nopersist",
	      webaudio: false,
	      images: false,
	      plugins: false,
	      webgl: false,   
	      webSecurity: false
	    }
	  });
	  
  nightmare.on('console', (log, msg) => {
//	  	console.log(msg);
	 	if (msg.indexOf('CONSOLE:') >= 0) {
	 		loggerLib.log(msg.replace('CONSOLE:', '').trim());
	 	}
  });
  nightmare.on('close', (data) => {
  	loggerLib.log('Child process closing: ' + data);
  });
  nightmare.on('exit', (data) => {
  	loggerLib.log('Child process exiting: ' + data);
  });
  nightmare.on('error', (data) => {
  	loggerLib.log('Child process error: ' + data);
  });
  nightmare.on('disconnect', (data) => {
  	loggerLib.log('Child process disconnected: ' + data);
  });  
	  
	  
	let userAgent = scraperUtils.getRandomUserAgentString();
	
  // HELP PAGE PARSE START
	var finalHelpPageFees = {};
	finalHelpPageFees.serviceFee = '';
	finalHelpPageFees.smallOrderMin = '';
  var helpPageFees = yield nightmare.useragent(userAgent).viewport(1, 1)
										  .filter({
										    urls: ['*://*/*']
										   }, function(details, cb) {
											  	if (
											  			details.url == 'https://help.uber.com/ubereats/article/how-do-prices-work-on-uber-eats?nodeId=7411dc8d-8b92-419c-b981-b8227a91596e' 
											  		 ) {
											  		return cb({});	
											  	} else {
											  		return cb({ cancel: true });	
											  	}
										   })  
										  .goto('https://help.uber.com/ubereats/article/how-do-prices-work-on-uber-eats?nodeId=7411dc8d-8b92-419c-b981-b8227a91596e')
											.wait('footer.page-footer')
											.evaluate(function() {
												 return window.__JSON_GLOBALS_["reactProps"];
				             	 })			
											.catch(function(error) {
												threadStats.incrementErrorCount();
									      loggerLib.log(error);
										    return false;
											});
  if (helpPageFees && helpPageFees.node) {
  	helpPageFees = helpPageFees.node.components;
  	for (var hlpI = 0; hlpI < helpPageFees.length; hlpI++) {
  		var helpPageFeesContent = helpPageFees[hlpI].content.text;
  		var serviceFeeRegexp = /Service fees equal(.*?)of/ig;
  		var serviceFeePercent = serviceFeeRegexp.exec(helpPageFeesContent);
  		if (serviceFeePercent && serviceFeePercent[1]) {
  			serviceFeePercent[1] = serviceFeePercent[1].trim();
  			if (serviceFeePercent[1].indexOf('%') > 0) {
  				finalHelpPageFees.serviceFee = parseInt(serviceFeePercent[1].replace('%', '').trim());
  			}
  		}
  		var smallOrderMinFeeRegexp = /Small order fees only apply to orders less than(.*?)\./ig;
  		var smallOrderMinFee = smallOrderMinFeeRegexp.exec(helpPageFeesContent);
  		if (smallOrderMinFee && smallOrderMinFee[1]) {
  			smallOrderMinFee[1] = smallOrderMinFee[1].trim();
  			if (smallOrderMinFee[1].indexOf('$') >= 0) {
  				finalHelpPageFees.smallOrderMin = parseInt(smallOrderMinFee[1].replace('$', '').trim());
  			}
  		}
  	}
  }
  loggerLib.log('Service Fees Collected|ServiceFee: ' + finalHelpPageFees.serviceFee + '|SmallOrderMin: ' + finalHelpPageFees.smallOrderMin);
  // HELP PAGE PARSE END	
  
	let homePageReload = true;
	let useProxy = false;
	let retryCount = 3;
  
	// Loop Job Descriptions
	for (var spi = 0; spi < searchPoints.length; spi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let searchPoint = searchPoints[spi];
		
		// Get Search Queries From Job Description
		const searchQueries = scraperUtils.getSearchQueries(searchPoint);		
		
		loggerLib.log('SEARCH POINT START: ' + (spi+1) + '/' + searchPoints.length);
		
		let address = searchPoint.Address;
		
		loggerLib.log('ADDRESS START: ' + address);
		
		if (homePageReload) {

			let proxy = '';
			if (useProxy) {
				let proxyFound = false;
				while (!proxyFound) {
					proxy = proxyLib.getRandomUnusedProxy();
					loggerLib.log('Checking Proxy: ' + proxy);
					proxyFound = yield proxyLib.checkProxy(proxy).then(data => {
						loggerLib.log('Proxy OK');
						proxyLib.setProxyInUse(proxy);
						return true;
				  }).catch((error) => {
				  	loggerLib.log('Proxy ERROR');
				  	proxyLib.setProxyInUse(proxy);
				  	return false;
				  });
				}
				loggerLib.log('Using Proxy: ' + proxy);
				yield nightmare.proxy(proxy);
				
				loggerLib.log('Clearing cookies');
			  yield nightmare.cookies.clearAll();
			}
			
		  var homePageLoad = yield nightmare
			  .filter({
			    urls: ['*://*/*']
			   }, function(details, cb) {
				  	if (
				  			details.url == 'https://www.ubereats.com/en-US/' 
			  				|| details.url == 'https://www.ubereats.com/rtapi/eats/v1/bootstrap-eater'
		  					|| details.url == 'https://www.ubereats.com/rtapi/eats/v1/search'
			  				|| details.url.indexOf('https://www.ubereats.com/rtapi/eats/v2/eater-store/') === 0
				  		 ) {
				  		return cb({});	
				  	} else {
				  		return cb({ cancel: true });	
				  	}
			   })     		  
			  .goto('https://www.ubereats.com/en-US/')
        .inject('js', 'inject-js/jquery.min.js')
        .inject('js', 'inject-js/moment-with-locales.min.js')
        .inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')			  
				.wait('#address-selection-input')
			  .then(() => {
			  	return true;
		  	})
		  	.catch(function(error) {
		  		threadStats.incrementErrorCount();
			    loggerLib.log(error);
			    return false;
		  	});
			loggerLib.log('Home Page Load: ' + homePageLoad);
			if (!homePageLoad) {
				if (retryCount > 0) {
					useProxy = true;
					spi = spi - 1;
					retryCount = retryCount - 1;
				}
				continue;
			}
			homePageReload = false;
		}
		
	  var result = yield nightmare
		  .evaluate((addressTimeZoneSearchQueries, done) => {
		  	
		  	var result = {};
			  result.rests = [];
			  result.errors = [];
			  
			  function go() {
		  	
		  	if (typeof window == 'undefined' || window == null) {
		  		console.log('CONSOLE: ERROR: window is not defined');
		  		done(null, result);	
		  	}
		  	
		  	document.body.style.display = 'none';
		  	window.CONFIG = undefined;
		  	window.INITIAL_STATE = undefined;
		  	
		    window.address = addressTimeZoneSearchQueries.address;
	  	  
		    // BootstrapEaterRequest Start ------------------------------------------
		    
	  	  console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Start');
	  	  
		 		var params = {};
				params.targetLocation = {};
				params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
				params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
				params.targetLocation.reference = '';
				params.targetLocation.type = "google_places";
				params.targetLocation.address = {};
				params.targetLocation.address.title = '';
				params.targetLocation.address.address1 = '';
				params.targetLocation.address.city = '';
				params.bafEducationCount = 20;
				params.feed = "combo";
				params.feedTypes = ['STORE', 'SEE_ALL_STORES'];
				params.feedVersion = 2;
				
		  	var bootstrapEaterRequestResponse = $.ajax({
		  		url: "https://www.ubereats.com/rtapi/eats/v1/bootstrap-eater",
					method: "POST",
					dataType: "json",
					data: JSON.stringify(params),
					cache: false,
					crossDomain: true,
					contentType: "application/json",
					beforeSend: function(xhr) {
						xhr.setRequestHeader('Accept', '*/*');
						xhr.setRequestHeader('Cache-Control', 'max-age=0');
						xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
						xhr.setRequestHeader('x-csrf-token', window.csrfToken);
					},
					xhrFields: {
						withCredentials: true
					},
					async: false,
					timeout: 8000
		  	}).responseText;	
		  	
		  	console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Done');
		  	
		  	if (bootstrapEaterRequestResponse) {
		  		
			  	try {
			  		
			  		bootstrapEaterRequestResponse = JSON.parse(bootstrapEaterRequestResponse);
			  		
				  	if (!bootstrapEaterRequestResponse.marketplace.isInServiceArea) {
							var resultToSave = {};
							resultToSave.request = 'bootstrapEaterRequest';
							resultToSave.msg = 'No Service in area';
							result.errors.push(resultToSave);
							console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest - No Service In Area');
							done(null, result);
							return;
		 				}
			  		
		 				let index = 1;
		 				
		 				console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Rests: ' + bootstrapEaterRequestResponse.marketplace.feed.feedItems.length);
		 				
		 				for (let fii = 0; fii < bootstrapEaterRequestResponse.marketplace.feed.feedItems.length; fii++) {
		 					
		 				  let feedItem = bootstrapEaterRequestResponse.marketplace.feed.feedItems[fii];
		 				 
		 					if (feedItem.type == 'STORE') {
		 					  let feedItemRest = feedItem.payload.storePayload;
		 					  var feedItemRestData = JSON.parse(feedItemRest.trackingCode);
		 					  if (feedItemRestData.storePayload.isOrderable) {
		 					  	
		 					  	var resultToSave = {};
			 					  resultToSave.searchQuery = '';
			 					  resultToSave.name = feedItemRest.stateMapDisplayInfo.available.title.text;
			 					  
			 					  resultToSave.uuid = feedItem.uuid;
			 					  resultToSave.city = bootstrapEaterRequestResponse.marketplace.cityName;
			 					  resultToSave.href = '';
			 				  	resultToSave.position = index;
			 				  	
			 				  	if (typeof feedItemRest.stateMapDisplayInfo.available.heroImage != 'undefined') {
			 				  		delete feedItemRest.stateMapDisplayInfo.available.heroImage;
			 				  	}
			 				  	
			 				  	resultToSave.restaurant_list_raw = JSON.stringify({feedItem: feedItemRest, storesMapItem: bootstrapEaterRequestResponse.marketplace.feed.storesMap[feedItem.uuid]});
			 				  	
			 				  	if (typeof feedItemRest.stateMapDisplayInfo.available.tagline.text != 'undefined') {
						  	  	let priceCuisine = feedItemRest.stateMapDisplayInfo.available.tagline.text.trim().split('•');
						  	  	for (let pci = 0; pci < priceCuisine.length; pci++) {
						  	  		priceCuisine[pci] = priceCuisine[pci].trim(); 
						  	  	}
						  	  	let price = priceCuisine.shift();
						  	  	if (price.indexOf('$') >= 0) {
						  	  		resultToSave.price = price.length;	
						  	  	}
						  	  	resultToSave.cuisine = priceCuisine.join('|');
			 				  	}
					  	  	
					  	  	resultToSave.eta = feedItemRestData.storePayload.etdInfo.dropoffETARange.min + '-' + feedItemRestData.storePayload.etdInfo.dropoffETARange.max;
					  	  	resultToSave.etaMin = feedItemRestData.storePayload.etdInfo.dropoffETARange.min;
					  	  	resultToSave.etaMax = feedItemRestData.storePayload.etdInfo.dropoffETARange.max;
					  	  	
					  	  	if (typeof feedItemRestData.storePayload.fareInfo != 'undefined') {
					  	  		resultToSave.deliverySearchPageFee = feedItemRestData.storePayload.fareInfo.serviceFee;
					  	  		resultToSave.deliveryMenuPageFee = feedItemRestData.storePayload.fareInfo.serviceFee;
					  	  		resultToSave.deliveryCheckoutPageFee = feedItemRestData.storePayload.fareInfo.serviceFee;
					  	  	}
					  	  	
					  	  	resultToSave.dateTimeUTC0 = (new Date()).toISOString(); 						  	  	
					  	  	resultToSave.dateTimeLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD hh:mm:ss');
					  	  	resultToSave.dateLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD');
					  	  	resultToSave.hourLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('HH');
					  	  	resultToSave.dayLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('dddd');									  	  	
					  	  	
					  	  	result.rests.push(resultToSave);
					  	  	resultToSave = undefined; 
				 					if (index >= 20) {
				 						break;
				 					}
				 					index++;
		 					  }
		 						feedItemRest = undefined;
		 						feedItemRestData = undefined;
		 						feedItemRestData = undefined;
		 					}
		 					feedItem = undefined;
		 				}
	 				} catch(error) {
	 					console.log('CONSOLE: ERROR: BootstrapEaterRequest: ' + error);
	 				}
	 				
	 				if (result.rests.length == 0) {
						var resultToSave = {};
						resultToSave.request = 'bootstrapEaterRequest';
						resultToSave.msg = 'No restaurants found';
						result.errors.push(resultToSave);
						console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest - No Rests Found');
	 					done(null, result);
	 					return;
	 				}
	 				
	 				console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Finished');
		  	} 
		  	// BootstrapEaterRequest Finished ------------------------------------------
	 				
		  	
		  	// Search Queries Start ------------------------------------------
 				let categories = addressTimeZoneSearchQueries.searchQueries;
	 				
 				console.log('CONSOLE: ' + window.address + '| Search Queries: ' + categories.join(','));
	 				
 				var params = {};
				params.targetLocation = {};
				params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
				params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
				params.targetLocation.reference = '';
				params.targetLocation.type = "google_places";
				params.targetLocation.address = {};
				params.targetLocation.address.title = '';
				params.targetLocation.address.address1 = '';
				params.targetLocation.address.city = bootstrapEaterRequestResponse.marketplace.cityName;
				params.sortAndFilters = [];
				
				bootstrapEaterRequestResponse = undefined;
					
				var catsTotal = categories.length;
				for (var i = 0; i < catsTotal; i++) {
					
					var pars = JSON.parse(JSON.stringify(params));
					pars.userQuery = categories.shift();
					console.log('CONSOLE: ' + window.address + '| SearchRequest (' + pars.userQuery + '): Start');
					
					var searchRequestResponse = $.ajax({
						url: "https://www.ubereats.com/rtapi/eats/v1/search",
						method: "POST",
						dataType: "json",
						data: JSON.stringify(pars),
						cache: false,
						crossDomain: true,
						contentType: "application/json",
						beforeSend: function(xhr){
							xhr.setRequestHeader('Accept', '*/*');
							xhr.setRequestHeader('Cache-Control', 'max-age=0');
							xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
							xhr.setRequestHeader('x-csrf-token', window.csrfToken);
						},
						xhrFields: {
							withCredentials: true
						},
						async: false,
						timeout: 8000
					}).responseText;
									
					if (searchRequestResponse) {
						try {
							searchRequestResponse = JSON.parse(searchRequestResponse);
							
							if (searchRequestResponse.results) {
							
								let userQuery = pars.userQuery;
								
								console.log('CONSOLE: ' + window.address + '| SearchRequest (' + userQuery + '): Done. Rests: ' + searchRequestResponse.results.length);
								
								let city = params.targetLocation.address.city;
		
								let index = 1;
				 				for (let fii = 0; fii < searchRequestResponse.results.length; fii++) {
				 					
				 				  let feedItem = searchRequestResponse.results[fii];
				 				 
				 					if (feedItem.type == 'store') {
				 					  let feedItemRest = feedItem.store;
				 					  
				 					  if (feedItemRest.isOrderable) {
				 					  	var resultToSave = {};
					 					  resultToSave.searchQuery = userQuery;
					 					  resultToSave.name = feedItemRest.title;
					 					  
					 					  resultToSave.uuid = feedItemRest.uuid;
					 					  resultToSave.city = city;
					 					  resultToSave.href = '';
					 				  	resultToSave.position = index;
					 				  	resultToSave.restaurant_list_raw = JSON.stringify(feedItem);
					 				  	
					 				  	if (feedItemRest.storeBadges) {
						 				  	for (var badgeName in feedItemRest.storeBadges) {
						 				  		if (badgeName == 'restaurantDistanceBadge') {
						 				  			resultToSave.distance_competitor_displayed = feedItemRest.storeBadges[badgeName].text.toLowerCase().replace('mi', '').trim();
						 				  		}
						 				  	}
					 				  	}
					 				  	
					 				  	if (typeof feedItemRest.priceBucket != 'undefined') {
					 				  		resultToSave.price = feedItemRest.priceBucket.length;
					 				  	}
							  	  	
					 				  	if (typeof feedItemRest.categories != 'undefined') {
								  	  	let cuisine = [];
								  	  	for (let ci = 0; ci < feedItemRest.categories.length; ci++) {
								  	  		cuisine.push(feedItemRest.categories[ci].name);
								  	  	}
								  	  	resultToSave.cuisine = cuisine.join('|');
					 				  	}
							  	  	
							  	  	resultToSave.eta = feedItemRest.etaRange.min + '-' + feedItemRest.etaRange.max;
							  	  	resultToSave.etaMin = feedItemRest.etaRange.min;
							  	  	resultToSave.etaMax = feedItemRest.etaRange.max;
							  	  	
							  	  	if (typeof feedItemRest.fareInfo != 'undefined') {
							  	  		resultToSave.deliverySearchPageFee = feedItemRest.fareInfo.serviceFee;
							  	  		resultToSave.deliveryMenuPageFee = feedItemRest.fareInfo.serviceFee;
							  	  		resultToSave.deliveryCheckoutPageFee = feedItemRest.fareInfo.serviceFee;
							  	  	}
							  	  	
							  	  	resultToSave.dateTimeUTC0 = (new Date()).toISOString();
							  	  	resultToSave.dateTimeLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD hh:mm:ss');
							  	  	resultToSave.dateLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD');
							  	  	resultToSave.hourLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('HH');
							  	  	resultToSave.dayLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('dddd');									  	  	
							  	  	
							  	  	result.rests.push(resultToSave);
							  	  	resultToSave = undefined;
						 					if (index >= 20) {
						 						break;
						 					}
						 					index++;
				 					  }
				 					 feedItemRest = undefined; 
				 					}
				 					feedItem = undefined;
				 				}
			 				
							} else {
								console.log('CONSOLE: ' + window.address + '| SearchRequest (' + userQuery + '): Done. Rests: NOT RESULTS');
							}
			 				
			 				
						} catch (error) {
							console.log('CONSOLE: ERROR: SearchRequest: ' + error);
						}
						
						searchRequestResponse = undefined;
					}
				}
				// Search Queries End ------------------------------------------
				
				// Rest Details Start ------------------------------------------
				
				if (result.rests && result.rests.length > 0) {
					
					let restDetailsResponseCache = {};
					
					for (var ri = 0; ri < result.rests.length; ri++) {
						var rest = result.rests[ri];
						
						if (rest && rest.uuid) {
						
							var restDetailsRequestResponse = '';
							
							if (!restDetailsResponseCache[rest.uuid]) {
								restDetailsRequestResponse = $.ajax({
						  		url: "https://www.ubereats.com/rtapi/eats/v2/eater-store/" + rest.uuid + '?sfNuggetCount=1',
									method: "GET",
									cache: false,
									crossDomain: true,
									beforeSend: function(xhr) {
										xhr.setRequestHeader('Accept', '*/*');
										xhr.setRequestHeader('Cache-Control', 'max-age=0');
										xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
										xhr.setRequestHeader('x-csrf-token', window.csrfToken);
										xhr.setRequestHeader('x-uber-target-location-latitude', parseFloat(addressTimeZoneSearchQueries.latLng.lat));
										xhr.setRequestHeader('x-uber-target-location-longitude', parseFloat(addressTimeZoneSearchQueries.latLng.lng));
									},
									xhrFields: {
										withCredentials: true
									},
									async: false,
									timeout: 8000
						  	}).responseText;
								restDetailsResponseCache[rest.uuid] = restDetailsRequestResponse;
							} else {
								restDetailsRequestResponse = restDetailsResponseCache[rest.uuid];
							}
							
							if (restDetailsRequestResponse) {
						  	try {
						  		var restDetailsRequestResponseObj = JSON.parse(restDetailsRequestResponse).store;
						  		
						  		if (restDetailsRequestResponseObj) {
							  		if (restDetailsRequestResponseObj.nuggets && restDetailsRequestResponseObj.nuggets.length > 0) {
							  			for (var ni = 0; ni < restDetailsRequestResponseObj.nuggets.length; ni++) {
							  				var nugget = restDetailsRequestResponseObj.nuggets[ni];
							  				if (nugget.nuggetType == 'SERVICE_FEES') {
							  					var smallOrderFeeRegexp = /An additional(.*?)fee applies/ig;
							  		  		var smallOrderFee = smallOrderFeeRegexp.exec(nugget.body.text);
							  		  		if (smallOrderFee && smallOrderFee[1]) {
							  		  			smallOrderFee[1] = smallOrderFee[1].trim();
							  		  			if (smallOrderFee[1].indexOf('$') >= 0) {
							  		  				result.rests[ri].smallOrderFee = parseInt(smallOrderFee[1].replace('$', '').trim());
							  		  			}
							  		  		}
							  				}
							  			}
							  		}
							  		
							  		result.rests[ri].restaurant_address = restDetailsRequestResponseObj.location.address.formattedAddress;
							  		result.rests[ri].restaurant_lat = restDetailsRequestResponseObj.location.latitude.toString();
							  		result.rests[ri].restaurant_long = restDetailsRequestResponseObj.location.longitude.toString();
							  		
							  		restDetailsRequestResponseObj.categories = undefined;;
							  		restDetailsRequestResponseObj.sections = undefined;;
							  		restDetailsRequestResponseObj.sectionEntitiesMap = undefined;
							  		restDetailsRequestResponseObj.subsectionsMap = undefined;
			 				  		restDetailsRequestResponseObj.heroImage = undefined;
							  		
							  		result.rests[ri].restaurant_details_raw = JSON.stringify(restDetailsRequestResponseObj);
							  		restDetailsRequestResponseObj = undefined;
						  		} else {
						  			console.log('CONSOLE: ERROR: RestDetailsRequest - Response has no "store" object');
						  		}
						  		restDetailsRequestResponse = undefined;
						  	} catch (error) {
						  		console.log('CONSOLE: ERROR: RestDetailsRequest: ');
								}
							}
							console.log('CONSOLE: ' + window.address + '| RestDetailsRequest ' + (ri+1) + '/' + result.rests.length + ' Done');
						} else {
							console.log('CONSOLE: ERROR: RestDetailsRequest - Rest Has No UUID');
						}
					}
					
					restDetailsResponseCache = undefined;
				}
				// Rest Details End ------------------------------------------
				
			 }
			  
			 go(); 
			 go = undefined;
			 done(null, result);	
			 return;
		 }, {address: address, timeZone: searchPoint.TimeZone, searchQueries: searchQueries, latLng: {lat: searchPoint.Lat, lng: searchPoint.Lng}})
	   .catch(function(error) {		  	
	  	 threadStats.incrementErrorCount();
  		 loggerLib.log(error); 
	  	 homePageReload = true;
	  	 return {};
	   });
	  
	  if (spi > 0 && spi < searchPoints.length - 1 && spi % 3 == 0) {
		  loggerLib.log('Refresh Start');
			try {
				homePageReload = yield nightmare
								.refresh()
								.wait(5)
					      .inject('js', 'inject-js/jquery.min.js')
					      .inject('js', 'inject-js/moment-with-locales.min.js')
					      .inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
					      .wait('#address-selection-input')
					      .wait(5)
					      .then(() => {
							  	return true;
						  	})
						  	.catch(function(error) {
						  		threadStats.incrementErrorCount();
							    loggerLib.log(error);
							    return false;
						  	});
				homePageReload = !homePageReload;
				loggerLib.log('Refresh End. Reload HOME: ' + homePageReload);
			} catch (e) {
				threadStats.incrementErrorCount();
	      loggerLib.log(e);
	      homePageReload = true;
			}
	  }
	  
  	if (typeof result.errors != 'undefined' && result.errors.length > 0) {
  		for (let eri = 0; eri < result.errors.length; eri++) {
  			threadStats.incrementErrorCount();
  			loggerLib.log(result.errors[eri]);	
  		}
  		delete result.errors;
  	}
  	
  	
		if (typeof result.rests != 'undefined' && result.rests.length > 0) {

			var roadDistanceCalculatedRestCount = 0;
			for (var ri = 0; ri < result.rests.length; ri++) {
				result.rests[ri].site = WEBSITE_NAME;
				result.rests[ri].address = address;
				result.rests[ri].address_lat = searchPoint.Lat;
				result.rests[ri].address_long = searchPoint.Lng;
				result.rests[ri].market = searchPoint.CBSA;
				result.rests[ri].region_name = searchPoint.Region;
				result.rests[ri].serviceFee = finalHelpPageFees.serviceFee;
				result.rests[ri].smallOrderMin = finalHelpPageFees.smallOrderMin;					
	  	  let nameSlug = result.rests[ri].name.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
				let uuidSlug = slugid.encode(result.rests[ri].uuid);
				result.rests[ri].href = 'https://www.ubereats.com/en-US/' +  result.rests[ri].city + '/food-delivery/' + nameSlug + '/' + uuidSlug
				result.rests[ri].distance_straight_line = distance.getStraighLineDistance(
						{lat: parseFloat(result.rests[ri].address_lat), lon: parseFloat(result.rests[ri].address_long)},
						{lat: parseFloat(result.rests[ri].restaurant_lat), lon: parseFloat(result.rests[ri].restaurant_long)}
				);
				
				if (!result.rests[ri].distance_actual && !result.rests[ri].distance_duration) {
				  var roadDistanceResponse =  yield distance.getRoadDistance(
							{lat: parseFloat(result.rests[ri].address_lat), lng: parseFloat(result.rests[ri].address_long)},
							{lat: parseFloat(result.rests[ri].restaurant_lat), lng: parseFloat(result.rests[ri].restaurant_long)}
					).then(data => {
				    return data;
				  }).catch((error) => {
	//			  	loggerLib.log(error);
				    return null;
				  });	
				  if (roadDistanceResponse) {
				  	result.rests[ri].distance_actual = roadDistanceResponse.distance;
				  	result.rests[ri].distance_duration = roadDistanceResponse.duration;
				  	for (var riii = 0; riii < result.rests.length; riii++) {
							if (result.rests[riii].uuid == result.rests[ri].uuid) {
								if (!result.rests[riii].distance_actual && !result.rests[riii].distance_duration) {
									result.rests[riii].distance_actual = result.rests[ri].distance_actual; 
									result.rests[riii].distance_duration = result.rests[ri].distance_duration;
								}
							}
						}
				  	roadDistanceCalculatedRestCount++;
				  }
				} else {
					roadDistanceCalculatedRestCount++;
				}
				
				if (result.rests[ri].distance_competitor_displayed) {
					for (var rii = 0; rii < result.rests.length; rii++) {
						if (result.rests[rii].uuid == result.rests[ri].uuid) {
							if (!result.rests[rii].distance_competitor_displayed && result.rests[ri].distance_competitor_displayed) {
								result.rests[rii].distance_competitor_displayed = result.rests[ri].distance_competitor_displayed;
							}
						}
					}
				}
				
				delete result.rests[ri].city;				
			}
			
			
			if (result.rests.length > 0) {
				var roadDistanceCalculatedPercent = Math.round((roadDistanceCalculatedRestCount / result.rests.length) * 100);
				loggerLib.log('Road Distance Calculated for ' + roadDistanceCalculatedRestCount + ' rests out of ' + result.rests.length + '|' + roadDistanceCalculatedPercent + '%');
			}			
			
			threadStats.processSearchPointResults(result.rests);
			if (!DEBUG_MODE) {
	  		if (result.rests.length > 0) {
	  			if ((spi+1) == searchPoints.length) {
	  				resultsLib.saveToCSV(WEBSITE_NAME, result.rests, THREAD_ID, startDateTime, true);	  				
	  			} else {
	  				resultsLib.saveToCSV(WEBSITE_NAME, result.rests, THREAD_ID, startDateTime);	
	  			}	  			
	  		}			
			}			
			
			loggerLib.log('ADDRESS END: ' + address + ' Results: ' + result.rests.length);
			
			result = undefined;
	  } else {
	  	loggerLib.log('ADDRESS END: ' + address + ': NO Results');
	  }

		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));
		let electornMemory = yield pidusage(nightmare.proc.pid);
		electornMemory = Math.round((electornMemory.memory / 1024 / 1024) * 100) / 100
		loggerLib.log('SEARCH POINT END: ' + (spi+1) + '/' + searchPoints.length + ' Time: ' + jobTime + 'sec Memory: ' + usedMemory + 'mb Electron Memory: ' + electornMemory + 'mb');
		
	} // SEARCH POINT END
	
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
 	threadStats.setExecutionTime(executionTime);
 	if (!DEBUG_MODE) {
 	  threadStats.saveThreadStatsToFile();
 	}
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
  
  loggerLib.log('Process exit');
  process.exit();
});