// node --harmony grubhub-thread-v2-api.js 1 >> ./logs/grubhub-thread-api.log 2>&1 &
// node --harmony grubhub-thread-v2-api.js 1 >> grubhub-thread-1-api.log 2>&1 &
// node --harmony grubhub-thread-v2-api.js 2 >> grubhub-thread-2-api.log 2>&1 &
// node --harmony grubhub-thread-v2-api.js 3 >> grubhub-thread-3-api.log 2>&1 &
// node --harmony grubhub-thread-v2-api.js 4 >> grubhub-thread-4-api.log 2>&1 &

const vo = require('vo');
var request = require('request');

const constants = require('./config/constants');
const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const scraperUtils = require('./lib/scraper-utils');
const distance = require('./lib/distance');
const threadStats = require('./lib/thread-stats');

const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');

//TIMER Start
let start = process.hrtime();

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.GRUBHUB_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }  
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

var startDateTime = new Date();

// Main RUN Function
const run = function * () {
	
	var resultsGlobal = [];
	
	const searchPoints = yield scraperUtils.getSearchPoints(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });	
	
	loggerLib.log('THREAD ' + THREAD_ID);
	
  if (!searchPoints || searchPoints.length == 0) {
    return;
  } 	
  
  threadStats.initStats(WEBSITE_NAME, THREAD_ID);
  
  // PROD
  const clientId = 'itirra_aWhhdGVpdGlycmFwcmljZXB';
  const baseUrl = 'https://api-gtm.grubhub.com';  
  
  // Get Auth Token
  const authResponse = yield function() {
    return new Promise((res, rej) => {  
      var options = {
          method: 'POST',
          url: baseUrl + '/oauth2/direct/auth',
          json: {
            'grant_type': 'token',
            'client_id': clientId,
            'scope': 'anonymous',
          }
      };
      request(options, function (error, response, body) {
        if (error) {          
          rej('ERROR getting Auth Token: ' + error);
        }
        if (!response || response.statusCode != 200) {          
          rej('ERROR getting Auth Token. Status Code: ' + response.statusCode);         
        }       
        res(body);
      });
    }); 
  }().then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	threadStats.incrementErrorCount();
    return null;
  });
  
  if (!authResponse || typeof authResponse.access_token == 'undefined') {
  	loggerLib.log('AUTH Response = null');
  	threadStats.incrementErrorCount();
  	return;
  }
	
	// Loop Search Points
	for (var spi = 0; spi < searchPoints.length; spi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let searchPoint = searchPoints[spi];
		
		loggerLib.log('SEARCH POINT START: ' + (spi+1) + '/' + searchPoints.length);
		
		let address = searchPoint.Address;
		
		loggerLib.log('ADDRESS START: ' + address);
		
		// Get Search Queries From Job Description
		const searchQueries = scraperUtils.getSearchQueries(searchPoint);
		const searchQueriesWithEmpty = [''];
		searchQueriesWithEmpty.push(...searchQueries);
		
		var addressResult = [];
		
  	for (var sqi = 0; sqi < searchQueriesWithEmpty.length; sqi++) {
		
  		let searchQuery = searchQueriesWithEmpty[sqi].trim();
  		
      let restResponse = yield function() {
        return new Promise((res, rej) => {
          var options = {
              method: 'GET',
              url: baseUrl + '/restaurants/search/search_listing',
              qs: {
                'locationMode': 'DELIVERY',
                'pageSize': '20',
                'location': 'POINT(' + searchPoint.Lng +' ' + searchPoint.Lat + ')',
                'facet': 'open_now:true'
              },            
              'auth': {
                'bearer': authResponse.access_token
              }           
          };
          
          if (searchQuery.length > 0) {
          	options.qs.queryText = searchQuery;
          }
          
          request(options, function (error, response, body) {
            if (error) {          
            	loggerLib.log(error);
            	loggerLib.log(response);
            	loggerLib.log(body);
              rej('ERROR getting Results');
            }
            if (!response || typeof response == 'undefined' || response.statusCode != 200) {          
            	loggerLib.log(error);
            	loggerLib.log(response);
            	loggerLib.log(body);        
              rej('ERROR getting Results');          
            }       
            res(body);
          });
        }); 
      }().then(data => {
        return data;
      }).catch((error) => {
      	loggerLib.log(error);
      	threadStats.incrementErrorCount();
        return null;
      });
      
      try {
      	restResponse = JSON.parse(restResponse);
      } catch(error) {
      	loggerLib.log(error);
      	threadStats.incrementErrorCount();
      	restResponse = {};
      }
      
      if (restResponse != null && typeof restResponse.results != 'undefined') {
      	
      	var roadDistanceCalculatedRestCount = 0;
      	for (var ri = 0; ri < restResponse.results.length; ri++) {
      		let restData = restResponse.results[ri];

	        let result = {};
	        
	        result.site = WEBSITE_NAME;
	        result.address = address;
	        result.address_lat = searchPoint.Lat;
	        result.address_long = searchPoint.Lng;
	        result.market = searchPoint.CBSA;
	        result.region_name = searchPoint.Region;
	  	  	result.searchQuery = searchQuery;
	  	  	result.name = restData.name;
	  	  	let nameSlug = restData.name.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
	  	  	let addressSlug = restData.address.street_address.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
	  	  	let citySlug = restData.address.address_locality.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
	  	  	result.href = 'https://www.grubhub.com/restaurant/' + nameSlug + '-' + addressSlug + '-' + citySlug + '/' + restData.restaurant_id;
	  	  	result.position = restData.rank;
	  	  	result.price = restData.price_rating;
	  	  	result.cuisine = restData.cuisines.join('|');		  	  	
	  	  	let maxDeliveryTime = restData.delivery_time_estimate + 10;
	  	  	result.eta = restData.delivery_time_estimate + '-' + maxDeliveryTime;
	  	  	result.etaMin = restData.delivery_time_estimate;
	  	  	result.etaMax = maxDeliveryTime;
	  	  	
	  	  	result.restaurant_address = restData.address.street_address + ', ' + restData.address.address_locality + ', ' + restData.address.address_region + ' ' + restData.address.postal_code;
	  	  	result.restaurant_lat = restData.address.latitude;
	  	  	result.restaurant_long = restData.address.longitude;
	  	  	result.distance_competitor_displayed = restData.distance_from_location;
	  	  	result.restaurant_list_raw = JSON.stringify(restData);
	  	  	
	  	  	if (typeof restData.delivery_minimum != 'undefined' && restData.delivery_minimum != null) {
	  	  		result.minOrder = restData.delivery_minimum.price / 100;
	  	  	}
	  	  	
	  	  	if (typeof restData.delivery_fee != 'undefined' && restData.delivery_fee != null) {
		  	  	result.deliverySearchPageFee = restData.delivery_fee.price / 100;
		  	  	result.deliveryMenuPageFee = result.deliverySearchPageFee;
		  	  	result.deliveryCheckoutPageFee = result.deliveryMenuPageFee;
	  	  	}
	  	  	
	  	  	if (typeof restData.min_delivery_fee != 'undefined' && restData.min_delivery_fee != null) {
	  	  		result.minDeliveryFee = restData.min_delivery_fee.price / 100;
	  	  	}
	  	  	
	  	  	if (typeof restData.delivery_fee_without_discounts != 'undefined' && restData.delivery_fee_without_discounts != null) {
	  	  		result.deliveryFeeWithoutDiscounts = restData.delivery_fee_without_discounts.price / 100;
	  	  	}
	  	  	
	  	  	result.distance_straight_line = distance.getStraighLineDistance(
							{lat: parseFloat(result.address_lat), lon: parseFloat(result.address_long)},
							{lat: parseFloat(result.restaurant_lat), lon: parseFloat(result.restaurant_long)}
					);
					
				  var roadDistanceResponse =  yield distance.getRoadDistance(
							{lat: parseFloat(result.address_lat), lng: parseFloat(result.address_long)},
							{lat: parseFloat(result.restaurant_lat), lng: parseFloat(result.restaurant_long)}
					).then(data => {
				    return data;
				  }).catch((error) => {
//				  	loggerLib.log(error);
				    return null;
				  });	
				  if (roadDistanceResponse) {
				  	result.distance_actual = roadDistanceResponse.distance;
				  	result.distance_duration = roadDistanceResponse.duration;
				  	roadDistanceCalculatedRestCount++;
				  }
	  	  	
					result.dateTimeUTC0 = (new Date()).toISOString();
					result.dateTimeLocal = momentTz(new Date()).tz(searchPoint.TimeZone).format('YYYY-MM-DD hh:mm:ss');
					result.dateLocal = momentTz(new Date()).tz(searchPoint.TimeZone).format('YYYY-MM-DD');
					result.hourLocal = momentTz(new Date()).tz(searchPoint.TimeZone).format('HH');
					result.dayLocal =  momentTz(new Date()).tz(searchPoint.TimeZone).format('dddd');
					
					addressResult.push(result);
      	}
      	
  			if (restResponse.results.length > 0) {
  				var roadDistanceCalculatedPercent = Math.round((roadDistanceCalculatedRestCount / restResponse.results.length) * 100);
  				loggerLib.log('Road Distance Calculated for ' + roadDistanceCalculatedRestCount + ' rests out of ' + restResponse.results.length + '|' + roadDistanceCalculatedPercent + '%');
  			}
      	
      	loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + restResponse.results.length);
      	
      	if (restResponse.results.length == 0 && searchQuery.length == 0) {
      		break;
      	}	        	
      }
		}
		
  	resultsGlobal.push(...addressResult);
  	
  	loggerLib.log('ADDRESS END|' + address + '|Results: ' + addressResult.length + '|Results Total: ' + resultsGlobal.length);

  	threadStats.processSearchPointResults(resultsGlobal);
		if (resultsGlobal.length > 0) {
			resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID, startDateTime);
			resultsGlobal = [];
		}			
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));
		loggerLib.log('SEARCH POINT END: ' + (spi+1) + '/' + searchPoints.length + ' Time: ' + jobTime + 'sec Memory: ' + usedMemory + 'mb');
	}
	
//	if (!DEBUG_MODE) {
//		const threadTime = Math.round((process.hrtime(start)[0]/60));
//		const cloudWatchLib = require("./lib/cloudwatch");
//		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
//				
//		let totalPrice = 0;
//		let avgPrice = 0;
//		let totalMaxEta = 0;
//		let avgMaxEta = 0;
//		for (let i = 0; i < resultsGlobal.length; i++) {
//			let res = resultsGlobal[i];
//			if (typeof res.deliverySearchPageFee != 'undefined' && res.deliverySearchPageFee != null) {
//				totalPrice = totalPrice + parseFloat(res.deliverySearchPageFee);
//			}
//			if (typeof res.etaMax != 'undefined' && res.etaMax != null) {
//				totalMaxEta = totalMaxEta + res.etaMax;
//			}
//		}
//		if (resultsGlobal.length > 0) {
//			totalPrice = Math.round(totalPrice * 100) / 100;
//			avgPrice = Math.round((totalPrice / resultsGlobal.length) * 100) / 100;
//			totalMaxEta = Math.round(totalMaxEta * 100) / 100;
//			avgMaxEta = Math.round((totalMaxEta / resultsGlobal.length) * 100) / 100;
//			yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Price', avgPrice);
//			yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Max ETA', avgMaxEta);
//			loggerLib.log('CW DATA Sent|AVG Price: ' + avgPrice + '|AVG Max ETA: ' + avgMaxEta);
//		}
//	}
	
	return resultsGlobal;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
	if (results) {
		if (results.length > 0) {
			resultsLib.saveToCSV(WEBSITE_NAME, results, THREAD_ID, startDateTime, true);
		}
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
 	threadStats.setExecutionTime(executionTime);
 	threadStats.saveThreadStatsToFile();
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  process.exit();
});