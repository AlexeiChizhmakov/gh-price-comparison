const util = require('util');

module.exports = {
		
	/**
	 * Log Message
	 */	
	log: function (logMessage, prefix, additionalParams) {
		
		if (typeof logMessage == 'undefined' && typeof prefix == 'undefined') {
			console.log('---------------------------------');
			return;
		}
		
		if (logMessage == null) {
			return;
		}
		
		
		const message = [];
		
		if (typeof prefix != 'undefined' && prefix != null) {
			prefix = prefix + ': ';
		} else {
			prefix = '';
		}
		
		let status = 'OK';
		if (logMessage instanceof Error) {
			logMessage = logMessage.stack.replace(/(?:\r\n|\r|\n   )/g, "");
			status = 'ERROR';
		} else if (typeof logMessage.indexOf !== "undefined" && logMessage.indexOf('ERROR') == 0) {
			status = 'ERROR';
		}
		
		const dateStr = (new Date()).toISOString();
		
		message.push(dateStr.split('T')[0]);
		message.push(dateStr.split('T')[1]);
		message.push(this.webSite);
		message.push(this.threadId);
		message.push(status);
		message.push(prefix + util.inspect(logMessage, {showHidden: false, depth: null, breakLength: Infinity}).replace(/^'(.+(?='$))'$/, '$1'));
		
		if (Array.isArray(additionalParams)) {
			message.push(...additionalParams);
		}
		
	  console.log(message.join('|'));
	},
	
	/**
	 * Get The FileName for the Screenshot
	 */
	screenshotFileName: function(postFix) {
		return './screenshots/'  + this.webSite + '_' + this.threadId + '_' + (new Date()).toISOString() + '_' + postFix + '.png';
	}

}