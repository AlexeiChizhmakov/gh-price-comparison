module.exports = {
	
	// Thread Stats File Path	
	threadStatsFilePath: './thread-stats.csv',

	// Global stats object	
	globalStats: {
		site: "NONE",
		thread: 0,
		search_point_count: 0,
		execution_time: 0,
		results_total: 0,
		delivery_fee_total: 0,
		max_eta_total: 0,
		road_distances_calculated: 0,
		error_count: 0,
	},
		
	
	/**
	 * Init stats
	 */	
	initStats: function (site, thread) {
		this.globalStats.site = site;
		this.globalStats.thread = thread;
	},
	
	/**
	 * Increment error count
	 */	
	incrementErrorCount: function () {
		this.globalStats.error_count += 1;
	},
	
	/**
	 * Set execution time
	 */	
	setExecutionTime: function (execution_time) {
		this.globalStats.execution_time = execution_time;
	},	
	
	/**
	 * Process Search Point Results
	 */	
	processSearchPointResults: function (results) {
		this.globalStats.search_point_count += 1;
		if (results && results.length > 0) {
			this.globalStats.results_total += results.length;
			for (var i = 0; i < results.length; i++) {
				var result = results[i];
				if (result.site == 'GrubHub' || result.site == 'UberEats') {
					if (typeof result.deliverySearchPageFee != 'undefined' && result.deliverySearchPageFee != null && result.deliverySearchPageFee) {
						this.globalStats.delivery_fee_total += this.prepareDeliveryFee(result.deliverySearchPageFee);
					}					
				}
				if (result.site == 'DoorDash') {
					if (typeof result.deliveryFeeWithoutDiscounts != 'undefined' && result.deliveryFeeWithoutDiscounts != null && result.deliveryFeeWithoutDiscounts) {
						this.globalStats.delivery_fee_total += this.prepareDeliveryFee(result.deliveryFeeWithoutDiscounts);
					}
				}
				if (typeof result.etaMax != 'undefined' && result.etaMax != null && result.etaMax) {
					this.globalStats.max_eta_total += parseInt(result.etaMax);
				}
				if (result.distance_actual) {
					this.globalStats.road_distances_calculated += 1;
				}
			}
		}
	},
	
	/**
	 * Prepare delivery fee
	 */
	prepareDeliveryFee: function (deliveryFee) {
		return parseFloat(deliveryFee.toString().replace('$', '').trim());
	},
	
	/**
	 * Save Thread Stats
	 */	
	saveThreadStatsToFile: function () {
		const json2csv = require('json2csv');
		const fs = require('fs');
		
		var date = new Date();
		var currentHour = (date.toISOString().split('T')[1].split(':'))[0];
		var currentDate = (date.toISOString().split('T')[0]);
		var currentTime = (date.toISOString().split('T')[1]);		
		this.globalStats.date_local = currentDate;
		this.globalStats.time_local = currentTime;
		this.globalStats.hour_local = parseInt(currentHour);
		
		const fields = ['site', 'thread', 'date_local', 'time_local', 'hour_local', 'search_point_count', 'execution_time', 'results_total', 'delivery_fee_total', 'max_eta_total', 'road_distances_calculated', 'error_count'];

		if (!fs.existsSync(this.threadStatsFilePath)) {
			fs.writeFileSync(this.threadStatsFilePath, json2csv({data: [this.globalStats], fields: fields}), 'ascii');
		} else {
			var os = require('os');
			var eol = os.EOL || '\n';
			fs.appendFileSync(this.threadStatsFilePath, eol + json2csv({data: [this.globalStats], fields: fields, hasCSVColumnTitle: false}), 'ascii');
		}
	},
	
	/**
	 * Upload Stats to Cloud Watch
	 */	
	getStatsForWebsite: function (website) {
		const fs = require('fs');
		const csv = require('csvtojson');
		
		return new Promise((promiseRes, promiseRej) => {
			let result = [];
			csv({delimiter: ",",}).fromFile(this.threadStatsFilePath)
			.on('json', (json) => {
				if (json['site'] == website) {
					result.push(json);
				}
			})
			.on('error', (error)=>{ promiseRej(error); })
			.on('end', () => {
				var statsToReturn = {};
				
				statsToReturn.website = website; 
				
				statsToReturn.completed_thread_count = result.length;
				
				statsToReturn.total_results = 0;
				statsToReturn.total_error_count = 0;
				
				statsToReturn.delivery_fee_total = 0;
				statsToReturn.max_eta_total = 0;
				statsToReturn.road_distances_calculated = 0;
				
				statsToReturn.avg_delivery_fee = 0;
				statsToReturn.avg_max_eta = 0;
				statsToReturn.road_distances_calculated_percent = 0;
				
				statsToReturn.avg_execution_time = 0;
				statsToReturn.max_execution_time = 0;

				var executionTimes = [];
				for (var i = 0; i < result.length; i++) {
					var res = result[i];
					
					statsToReturn.total_results += parseInt(res.results_total);
					statsToReturn.total_error_count += parseInt(res.error_count);
					statsToReturn.delivery_fee_total = statsToReturn.delivery_fee_total + parseFloat(res.delivery_fee_total);
					statsToReturn.max_eta_total += parseInt(res.max_eta_total);
					statsToReturn.road_distances_calculated += parseInt(res.road_distances_calculated);
					executionTimes.push(parseInt(res.execution_time));
				}
				
				statsToReturn.avg_delivery_fee =	parseFloat(parseFloat(statsToReturn.delivery_fee_total / statsToReturn.total_results).toFixed(2));
				statsToReturn.avg_max_eta =	Math.round(statsToReturn.max_eta_total / statsToReturn.total_results);
				statsToReturn.road_distances_calculated_percent =	Math.round((statsToReturn.road_distances_calculated / statsToReturn.total_results) * 100);
				
				const arrMax = arr => Math.max(...arr);
				const arrAvg = arr2 => arr2.reduce((a,b) => a + b, 0) / arr2.length;
				
				statsToReturn.avg_execution_time = Math.round(arrAvg(executionTimes));
			  statsToReturn.max_execution_time = arrMax(executionTimes);
			  	
			  promiseRes(statsToReturn);
			});
		});
		
	}
	

}