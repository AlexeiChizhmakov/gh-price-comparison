module.exports = {
		

	// API Quota Exceeded	
	quotaExceeded: false,
	
		
	/**
	 * getStraighLineDistance
	 * distanceTo({lat: 51, lon: 4}, {lat: 51.0006, lon: 4.001}) 
	 * 96.7928594737802 meters
	 * converted to miles
	 */
	getStraighLineDistance: function(from, to) {
		const geo = require('geolocation-utils');
		return (geo.distanceTo(from, to) / 1609.344).toFixed(1);
	},
 

  getRoadDistance: function(from, to) {
  	var googleDistance = require('google-distance-matrix');
  	var redis = require("redis");
  	var client = redis.createClient();
  	
  	googleDistance.key('AIzaSyDzPLniCE4rVIjMBmgUkq5qXpkYADvWOZA');
  	googleDistance.mode('driving');
  	googleDistance.units('imperial');
  	
  	var fromLat = from.lat.toString().trim();
  	var fromLng = from.lng.toString().trim();
  	var origins = [fromLat + ',' + fromLng];
  	
  	var toLat = to.lat.toString().trim();
  	var toLng = to.lng.toString().trim();
  	var destinations = [toLat + ',' + toLng];
  	
  	var cacheKey = 'CacheKey|' + fromLat + '|' + fromLng + '|' + toLat + '|' + toLng;
  	
  	return new Promise((res, rej) => {
  		client.get(cacheKey, function(err, reply) {
  			if (err) {
  				client.quit();
  				rej(err);
  			} else {
  				if (reply) {
  					var result = {};
  					result.distance = reply.split('|')[0];
  					result.duration = reply.split('|')[1];
  					result.msg = 'Cache Hit';
  					client.quit();
  					res(result);
  				} else {
  					
  					if (this.quotaExceeded) {
  		        client.quit();
  		        rej('Quota eceeded');
  		        return;
  					}
  					
  		  		googleDistance.matrix(origins, destinations, function (err, distances) {
  		        if (err) {
  		        	client.quit();
  		        	rej(err);
  		        	return;
  		        }
  		        if(!distances) {
  		        	client.quit();
  		        	rej('no distances');
  		        	return;
  		        }
  		        if(typeof distances == 'undefined') {
  		        	client.quit();
  		        	rej('no distances');
  		        	return;
  		        }
  		        if (distances.status == 'OK') {
  		            for (var i=0; i < origins.length; i++) {
  		                for (var j = 0; j < destinations.length; j++) {
  		                    var origin = distances.origin_addresses[i];
  		                    var destination = distances.destination_addresses[j];
  		                    if (distances.rows[0].elements[j].status == 'OK') {
  		                    	var result = {};
  		                    	result.distance = distances.rows[i].elements[j].distance.text.split(' ')[0];
  		                    	result.duration = distances.rows[i].elements[j].duration.text.split(' ')[0];
  		                    	client.set(cacheKey, result.distance + '|' + result.duration);
  		                    	client.quit();
  		                    	result.msg = 'Cache Miss';
  		                    	res(result);
  		                    	return;
  		                    } else {
  		                    	client.quit();
  		                    	rej(destination + ' is not reachable by land from ' + origin);
  		                    	return;
  		                    }
  		                }
  		            }
  		        }
  		        client.quit();
  		        if (distances.error_message.indexOf('You have exceeded your daily request quota for this API') >= 0) {
  		        	this.quotaExceeded = true; 
  		        }
  		        rej('result not OK: ' + distances.error_message);
  		        return;
  		    	});
  				}
  			}
  		});
	});
  	
  }
	

}