module.exports = {
		
	/**
	 * Save Parse Results to CSV file
	 */	
	saveToCSV: function (site, rows, threadId, date, sync) {
		const Iconv  = require('iconv').Iconv;
		const iconv = new Iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE');
		const fs = require('fs');
		
		if (!sync) {
			sync = false;
		}
		
		if (!rows || rows.length == 0) {
			return;
		}
		
		rows.forEach(function (row) {
			if (row.name) {
				row.name = iconv.convert(row.name.toString()).toString().replace(/[^\x20-\x7E]+/g, '?').trim(); 
			}
			if (row.cuisine) {
				row.cuisine = iconv.convert(row.cuisine.toString()).toString().replace(/[^\x20-\x7E]+/g, '?').trim();
			}
			if (row.href) {
				row.href = iconv.convert(row.href.toString()).toString().replace(/[^\x20-\x7E]+/g, '?').trim();
			}				
			if (row.eta) {
				row.eta = iconv.convert(row.eta.toString()).toString().replace(/[^\x20-\x7E]+/g, '?').trim();
			}				
			if (row.etaMin) {
				row.etaMin = parseInt(row.etaMin);
			}
			if (row.etaMax) {
				row.etaMax = parseInt(row.etaMax);
			}
			if (row.distance_duration) {
				row.distance_duration = parseInt(row.distance_duration);
			}			
			if (row.distance_actual) {
				row.distance_actual = parseFloat(row.distance_actual);
			}
			if (row.distance_competitor_displayed) {
				row.distance_competitor_displayed = parseFloat(row.distance_competitor_displayed);
			}
			if (row.distance_straight_line) {
				row.distance_straight_line = parseFloat(row.distance_straight_line);
			}
			if (row.minOrder) {
				row.minOrder = row.minOrder.toString().replace('$', '').replace('+', '').trim();
				row.minOrder = parseFloat(row.minOrder).toFixed(2).toString().replace(',', '.');
				if (row.minOrder == 'NaN') {
					row.minOrder = '';
				}
			}
			if (row.deliverySearchPageFee) {
				row.deliverySearchPageFee = row.deliverySearchPageFee.toString().replace('$', '').replace('+', '').trim();
				row.deliverySearchPageFee = parseFloat(row.deliverySearchPageFee).toFixed(2).toString().replace(',', '.');
				if (row.deliverySearchPageFee == 'NaN') {
					row.deliverySearchPageFee = '';
				}
			}
			if (row.deliveryMenuPageFee) {
				row.deliveryMenuPageFee = row.deliveryMenuPageFee.toString().replace('$', '').trim();
				row.deliveryMenuPageFee = parseFloat(row.deliveryMenuPageFee).toFixed(2).toString().replace(',', '.');
				if (row.deliveryMenuPageFee == 'NaN') {
					row.deliveryMenuPageFee = '';
				}
			}
			if (row.deliveryCheckoutPageFee) {
				row.deliveryCheckoutPageFee = row.deliveryCheckoutPageFee.toString().replace('$', '').trim();
				row.deliveryCheckoutPageFee = parseFloat(row.deliveryCheckoutPageFee).toFixed(2).toString().replace(',', '.');
				if (row.deliveryCheckoutPageFee == 'NaN') {
					row.deliveryCheckoutPageFee = '';
				}
			}
			if (row.freeDeliveryMinOrder) {
				row.freeDeliveryMinOrder = row.freeDeliveryMinOrder.toString().replace('$', '').trim();
				row.freeDeliveryMinOrder = parseFloat(row.freeDeliveryMinOrder).toFixed(2).toString().replace(',', '.');
				if (row.freeDeliveryMinOrder == 'NaN') {
					row.freeDeliveryMinOrder = '';
				}
			}			
			if (row.smallOrderFee) {
				row.smallOrderFee = row.smallOrderFee.toString().replace('$', '').trim();
				row.smallOrderFee = parseFloat(row.smallOrderFee).toFixed(2).toString().replace(',', '.');
				if (row.smallOrderFee == 'NaN') {
					row.smallOrderFee = '';
				}
			}
			if (row.smallOrderMin) {
				row.smallOrderMin = row.smallOrderMin.toString().replace('$', '').trim();
				row.smallOrderMin = parseFloat(row.smallOrderMin).toFixed(2).toString().replace(',', '.');
				if (row.smallOrderMin == 'NaN') {
					row.smallOrderMin = '';
				}
			}
			if (row.serviceFee) {
				row.serviceFee = row.serviceFee.toString().replace('$', '').trim();
				row.serviceFee = parseFloat(row.serviceFee).toFixed(2).toString().replace(',', '.');
				if (row.serviceFee == 'NaN') {
					row.serviceFee = '';
				}
			}
			if (row.minDeliveryFee) {
				row.minDeliveryFee = row.minDeliveryFee.toString().replace('$', '').trim();
				row.minDeliveryFee = parseFloat(row.minDeliveryFee).toFixed(2).toString().replace(',', '.');
				if (row.minDeliveryFee == 'NaN') {
					row.minDeliveryFee = '';
				}
			}
			if (row.deliveryFeeWithoutDiscounts) {
				row.deliveryFeeWithoutDiscounts = row.deliveryFeeWithoutDiscounts.toString().replace('$', '').trim();
				row.deliveryFeeWithoutDiscounts = parseFloat(row.deliveryFeeWithoutDiscounts).toFixed(2).toString().replace(',', '.');
				if (row.deliveryFeeWithoutDiscounts == 'NaN') {
					row.deliveryFeeWithoutDiscounts = '';
				}
			}
			
			if (row.distance_actual) {
				row.distance_actual = parseFloat(row.distance_actual).toFixed(1).toString().replace(',', '.');
				if (row.distance_actual == 'NaN') {
					row.distance_actual = '';
				}
			}	
			if (row.distance_competitor_displayed) {
				row.distance_competitor_displayed = parseFloat(row.distance_competitor_displayed).toFixed(1).toString().replace(',', '.');
				if (row.distance_competitor_displayed == 'NaN') {
					row.distance_competitor_displayed = '';
				}
			}
			if (row.distance_straight_line) {
				row.distance_straight_line = parseFloat(row.distance_straight_line).toFixed(1).toString().replace(',', '.');
				if (row.distance_straight_line == 'NaN') {
					row.distance_straight_line = '';
				}
			}			
			
			if (row.hourLocal) {
				row.hourLocal = parseInt(row.hourLocal);
			}
			if (row.dateTimeUTC0) {
				row.dateTimeUTC0 = row.dateTimeUTC0.replace('T', ' ').split('.')[0];
			}
			if (row.restaurant_list_raw) {
				row.restaurant_list_raw = iconv.convert(row.restaurant_list_raw).toString().trim();
			}
			if (row.restaurant_details_raw) {
				row.restaurant_details_raw = iconv.convert(row.restaurant_details_raw).toString().trim();
			}
			
			row.site = site;	
		});
		 
		const json2csv = require('json2csv');
		
		const fields = [
		                'site', 'market', 'region_name', 'address', 'address_lat', 'address_long', 'searchQuery',
		                'name', 'restaurant_address', 'restaurant_lat', 'restaurant_long', 'href', 'position',
		        		    'price', 'cuisine', 'eta', 'etaMin', 'etaMax', 'minOrder', 'minDeliveryFee',
		        		    'deliverySearchPageFee', 'deliveryMenuPageFee', 'deliveryCheckoutPageFee', 'deliveryFeeWithoutDiscounts',
		        		    'freeDeliveryMinOrder', 'smallOrderFee', 'smallOrderMin', 'serviceFee',
		        		    'distance_actual', 'distance_duration', 'distance_competitor_displayed', 'distance_straight_line',
		        		    'dateTimeUTC0', 'dateTimeLocal', 'dateLocal', 'hourLocal', 'dayLocal',  
		        		    'restaurant_list_raw', 'restaurant_details_raw'
		            ];		

		if (!date) {
			date = new Date();
		}
		var currentHour = (date.toISOString().split('T')[1].split(':'))[0];
		var currentDate = (date.toISOString().split('T')[0]);
		var currentTime = (date.toISOString().split('T')[1]);
		
		//[WEBSITE]-[THREAD_ID]-[TYPE]-[DATE]-[HOUR].csv
//		const fileName = site + '-' + threadId + '-' + 'Results' + '-' + ((new Date()).toISOString().split('T')[0]) + '-' + hour + '.csv';
		const fileName = 'Results' + '-' + currentDate + '-' + currentHour + '.csv';

		const constants = require('../config/constants');
		
		if (!fs.existsSync(constants.CURRENT_RESULTS_FOLDER)){
			fs.mkdirSync(constants.CURRENT_RESULTS_FOLDER);
		}
		
		currentDate = ((new Date()).toISOString().split('T')[0]);
		currentTime = ((new Date()).toISOString().split('T')[1]);
		
		if (fs.existsSync(constants.CURRENT_RESULTS_FOLDER + fileName)){
			var os = require('os');
			var eol = os.EOL || '\n';
			if (sync) {
				fs.appendFileSync(constants.CURRENT_RESULTS_FOLDER + fileName, eol + json2csv({data: rows, fields: fields, hasCSVColumnTitle: false}), 'ascii');
				console.log(currentDate + '|' + currentTime + '|RESULTS|OK|RESULTS SAVED: ' + constants.CURRENT_RESULTS_FOLDER + fileName + ' APPEND: ' + rows.length);
			} else {
				fs.appendFile(constants.CURRENT_RESULTS_FOLDER + fileName, eol + json2csv({data: rows, fields: fields, hasCSVColumnTitle: false}), 'ascii', (err) => {
				  if (err) {
				  	console.log(currentDate + '|' + currentTime + '|RESULTS|ERROR|RESULTS NOT SAVED: ' + constants.CURRENT_RESULTS_FOLDER + fileName + ' APPEND: ' + rows.length);
				  } else {
				  	console.log(currentDate + '|' + currentTime + '|RESULTS|OK|RESULTS SAVED: ' + constants.CURRENT_RESULTS_FOLDER + fileName + ' APPEND: ' + rows.length);
				  }
				});				
			}
		} else {
			fs.writeFileSync(constants.CURRENT_RESULTS_FOLDER + fileName, json2csv({data: rows, fields: fields}), 'ascii');
			console.log(currentDate + '|' + currentTime + '|RESULTS|RESULTS SAVED: ' + constants.CURRENT_RESULTS_FOLDER + fileName + ' CREATE: ' + rows.length);
		}
		
	},
	
}