// npm install fast-csv
// npm install stream-concat
// npm install aws-sdk
// node --harmony aws-s3-export.js
// node --harmony aws-s3-export.js &>> ./logs/s3-export.log&

const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const fs = require('fs');
const csv = require('fast-csv');
var StreamConcat = require('stream-concat');
const AWS = require('aws-sdk');
const cloudWatchLib = require("./lib/cloudwatch");

// TIMER Start
let start = process.hrtime();

var vo = require('vo');

loggerLib.webSite = 'S3-EXPORT';
loggerLib.threadId = 0;

loggerLib.log('--- START');

// Main RUN Function
const run = function * () {
	
	
	// Step 1. Combine All Result CSV files into single Output file. ------------------------------------------------------------
	const processedResultFiles = [];
	const processedResultFileStats = [];
	fs.readdirSync(constants.CURRENT_RESULTS_FOLDER).map(function(file) {
		
		let fileStat = {};
		fileStat.website = file.split('-')[0];
//		fileStat.threadId = file.split('-')[1];
		fileStat.rowCount = fs.readFileSync(constants.CURRENT_RESULTS_FOLDER + file).toString().split('\n').length - 1;
		fileStat.fileSizeBytes = Math.round( fs.statSync(constants.CURRENT_RESULTS_FOLDER + file).size / 1000.0);
		processedResultFileStats.push(fileStat);
		
		processedResultFiles.push(file);
	});
	
	
	loggerLib.log('Processing ' + '(' + processedResultFiles.length + ')' + ' Result Files: ' + processedResultFiles.join(', '));
	
	if (processedResultFiles.length > 0) {
		const resultWebsiteStats = [];
		const resultWebsiteThreadStats = [];
		for (let k = 0; k < processedResultFileStats.length; k++) {
			let resultWebsiteStatsIndex = resultWebsiteStats.length;
			let resultWebsiteStat = {website: '', rowCount: 0, fileSizeBytes: 0};
			for (let k1 = 0; k1 < resultWebsiteStats.length; k1++) {
				if (resultWebsiteStats[k1].website == processedResultFileStats[k].website) {
//					delete processedResultFileStats[k].threadId;
					resultWebsiteStat = resultWebsiteStats[k1];
					resultWebsiteStatsIndex = k1;
					break;
				}
			}
			resultWebsiteStat.website = processedResultFileStats[k].website;
			resultWebsiteStat.rowCount = resultWebsiteStat.rowCount + processedResultFileStats[k].rowCount;
			resultWebsiteStat.fileSizeBytes = resultWebsiteStat.fileSizeBytes + processedResultFileStats[k].fileSizeBytes;
			resultWebsiteStats[resultWebsiteStatsIndex] = resultWebsiteStat;
		}
		
		for (let ii = 0; ii < resultWebsiteStats.length; ii++) {
			loggerLib.log('Sending data to CW: ' + resultWebsiteStats[ii].website + ' Result Count: ' + resultWebsiteStats[ii].rowCount + ' Result File Size (bytes): ' + resultWebsiteStats[ii].fileSizeBytes);
			yield cloudWatchLib.logMetric(resultWebsiteStats[ii].website, 'Result Count', resultWebsiteStats[ii].rowCount);
			yield cloudWatchLib.logMetric(resultWebsiteStats[ii].website, 'Result File Size (bytes)', resultWebsiteStats[ii].fileSizeBytes);
		}
		
		const resultFileName = 'Results' + '-' + ((new Date()).toISOString().split('T')[0]) + '-' + ((new Date()).toISOString().split('T')[1].split(':')[0]) + '.csv';
		
		yield (function() {
			return new Promise((res, rej) => {
				let fileNames = processedResultFiles;
				let fileIndex = 0;
				let nextStream = function() {
				  if (fileIndex === fileNames.length) return null;
				  return csv.fromStream(fs.createReadStream(constants.CURRENT_RESULTS_FOLDER + fileNames[fileIndex++]), {headers : true});
				};
				let combinedStream = new StreamConcat(nextStream, {objectMode: true});
				combinedStream
					.pipe(csv.createWriteStream({headers: true}))
					.pipe(fs.createWriteStream(constants.EXPORT_S3_FOLDER + resultFileName))
					.on('finish', () => {
						 res();
					 })
					.on('error', () => {
						rej();
					});			
			});
		})();
		
		processedResultFiles.map(function(file) {
			fs.unlinkSync(constants.CURRENT_RESULTS_FOLDER + file);	
		});
		
		let fileSize = fs.statSync(constants.EXPORT_S3_FOLDER + resultFileName).size / 1000000.0;
		loggerLib.log('Created Merged Result Output File: ' + resultFileName + ' (' + fileSize + ' MB)' + ' and removed thread result files');
	}
	
	
	// Step 2. Combine All Log .log files into single Output file. ------------------------------------------------------------
	const processedLogFiles = [];
	
	const errorCounts = [];
	fs.readdirSync(constants.CURRENT_LOGS_FOLDER).map(function(file) {
		let website = file.split('-')[0];
		website = (website == 'doordash')?'DoorDash':website;
		website = (website == 'ubereats')?'UberEats':website;
		website = (website == 'grubhub')?'GrubHub':website;
		if (typeof errorCounts[website] == 'undefined') {
			errorCounts[website] = 0;
		}
		errorCounts[website] += (fs.readFileSync(constants.CURRENT_LOGS_FOLDER + file).toString().match(/(\|error\||Unable|Navigation timed out)/gi) || []).length;
		processedLogFiles.push(file);
	});

	for (let kk in errorCounts) {
		yield cloudWatchLib.logMetric(kk, 'Error Count', errorCounts[kk]);
	}
	
	loggerLib.log('Processing ' + '(' + processedLogFiles.length + ')' + ' Log Files: ' + processedLogFiles.join(', '));
	
	if (processedLogFiles.length > 0) {
		const logsFileName = 'Logs' + '-' + ((new Date()).toISOString().split('T')[0]) + '-' + ((new Date()).toISOString().split('T')[1].split(':')[0]) + '.log';
		
		yield (function() {
			return new Promise((res, rej) => {
				let fileNames = processedLogFiles;
				let fileIndex = 0;
				let nextStream = function() {
				  if (fileIndex === fileNames.length) return null;
				  return fs.createReadStream(constants.CURRENT_LOGS_FOLDER + fileNames[fileIndex++]);
				};
				let combinedStream = new StreamConcat(nextStream);
				combinedStream
					.pipe(fs.createWriteStream(constants.EXPORT_S3_FOLDER + logsFileName))
					.on('finish', () => {
						 res();
					 })
					.on('error', () => {
						rej();
					});			
			});		
		})();
		
		
		processedLogFiles.map(function(file) {
			fs.unlinkSync(constants.CURRENT_LOGS_FOLDER + file);	
		});			
		
		let fileSize = fs.statSync(constants.EXPORT_S3_FOLDER + logsFileName).size / 1000000.0;
		loggerLib.log('Created Merged Log Output File: ' + logsFileName + ' (' + fileSize + ' MB)' + ' and removed thread log files');		
	}
	
	// Step 3. Upload All Result files to S3 ------------------------------------------------------------
	if (processedResultFiles.length > 0 || processedLogFiles.length > 0) {
		loggerLib.log('Starting Upload to S3');
		
		AWS.config.update({
				maxRetries: 10,
				accessKeyId: constants.AWS_S3_ACCESS_KEY_ID,
				secretAccessKey: constants.AWS_S3_SECRET_ACCESS_KEY
			}
		);
		
		const s3 = new AWS.S3();
		const filesToExport = fs.readdirSync(constants.EXPORT_S3_FOLDER);
		
		for (let i = 0; i < filesToExport.length; i++) {
			let file = filesToExport[i];
			yield (function() {
				return new Promise((res, rej) => {
						let awsS3Folder = '';
						if (file.indexOf('.csv') > 0) {
							awsS3Folder = constants.AWS_S3_RESULTS_FOLDER;
						}
						if (file.indexOf('.log') > 0) {
							awsS3Folder = constants.AWS_S3_LOGS_FOLDER;
						}
						
						let params = {
							Bucket: constants.AWS_S3_BUCKET,
							Key: awsS3Folder + file,
							Body: fs.readFileSync(constants.EXPORT_S3_FOLDER + file)
						};
						s3.putObject(params, function(err, data) {
							if (err) {
								loggerLib.log(err);
								rej();
							} else {
								fs.unlinkSync(constants.EXPORT_S3_FOLDER + file);
								loggerLib.log('Successfully Uploaded File to S3: [' + awsS3Folder + file + '] and removed local file');
								res();
							}
					});
				});
			})();
		}
	}
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
		loggerLib.log(error);
	}
	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', '', [executionTime]);
});