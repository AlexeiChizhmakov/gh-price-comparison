#!/bin/bash

  sleep 1

  node --harmony clean-used-proxies.js
  
  sleep 1
  
  for a in `seq 1 20`
  do
    DEBUG=* xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony --max-old-space-size=1024 doordash-thread-api-proxy.js $a >> ./logs/doordash-thread-$a-api.log 2>&1 &
    sleep 2
  done  
  