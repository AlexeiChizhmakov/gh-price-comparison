// Xvfb -ac -screen scrn 1280x2000x24 :9.0 &
// export DISPLAY=:9.0
// DEBUG=nightmare:*,electron:* node --harmony doordash-thread.js 1 0
// DEBUG=nightmare:actions,electron:* node --harmony doordash-thread.js 1 0 &>> ./logs/doordash-thread1.log&
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread.js 1 0 &>> ./logs/doordash-thread10.log&
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread.js 1 1 &>> ./logs/doordash-thread11.log&
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread.js 1 2 &>> ./logs/doordash-thread12.log&
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread.js 8 4 &>> ./logs/doordash-thread0000.log&


const configLib = require("./lib/config");
const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = 'DoorDash';
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let ADDRESS_INDEX = 0;
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	ADDRESS_INDEX = val; 
  }
  if (index == 4) {
    if (val == 'debug') {
      DEBUG_MODE = true;
    }
  }
});
loggerLib.threadId = THREAD_ID + '_' + ADDRESS_INDEX;

loggerLib.log('--- START');

const resultsGlobal = [];

var nightmare;

// Main RUN Function
const run = function * () {
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
	if (jobDescriptions.length == 0) {
	  return;
	}
	
	const nightmareConfig = {
		  waitTimeout: 10000,
		  show: false,
		  webPreferences: {
		    webaudio: false,
		    images: false,
		    plugins: false,
		    webgl: false    
		  }
	};

	const proxy = 'https://zproxy.lum-superproxy.io:22225'
	nightmareConfig.switches = {
			'proxy-server': proxy,
			'ignore-certificate-errors': true
	};	
	loggerLib.log('Using Proxy: ' + proxy);
	
	const nightmare = new Nightmare(nightmareConfig);
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		let addresses = configLib.getAddresses(jobDescription);
		
		if(typeof addresses[ADDRESS_INDEX] !== 'undefined') {
			addresses = [addresses[ADDRESS_INDEX]];	
		} else {
			addresses = [];
		}
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);		
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length + ' SEARCH QUERIES: ' + searchQueries.length);
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let addressResult = [];
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			loggerLib.log('SEARCH QUERY (EMPTY) START');
			
			let userAgent = configLib.getRandomUserAgentString();
			
//      let waitTimeOut = (Math.floor(Math.random() * 5000) + 100);
      
//      loggerLib.log('Waiting for ' + waitTimeOut + ' miliseconds...');
      
			let randPostfix = Math.floor(Math.random() * 10000) + 1;
      let lumSession = '-session-rand' + THREAD_ID + '_' + ADDRESS_INDEX + '_' + randPostfix;
      
      var ip = yield nightmare
      .useragent(userAgent).viewport(1600, 1200)
      .authentication('lum-customer-itirra-zone-dd-country-us' + lumSession, 'gttscewykkpo')
      .goto('http://lumtest.com/myip')
      .evaluate(() => {
      	return document.documentElement.innerText;
      })
      .catch(function(error) {
		    if (DEBUG_MODE) {
		      const screenshotFileName = loggerLib.screenshotFileName('ip_fetch_error');
		  	  nightmare.screenshot(screenshotFileName);
		  	  loggerLib.log(error, null, [screenshotFileName]);
		    } else {
		      loggerLib.log(error);
		    }
	  		return '';
	  	});
      
      loggerLib.log('Proxy IP: ' + ip + ' Session: ' + lumSession);
      
      //.wait(waitTimeOut)
			var result = yield nightmare
									.filter({
								    urls: [
								           'https://cdn.doordash.com/static/styles/*',
								           'https://cdn.segment.com/*',
								           'https://bam.nr-data.net/*',
								           'https://js-agent.newrelic.com/*',
								           'https://typography.doordash.com/*',
								           'https://cdn.doordash.com/static/img/*',
								           'https://bat.bing.com/*',
								           'https://www.google-analytics.com/*',
								           'https://www.googleadservices.com/*',
						               'https://dx.steelhousemedia.com/*',
						               'https://connect.facebook.net/*',
						               'https://ingest.doordash.com/*',
								          ],
								  }, function(details, cb) {
								    return cb({ cancel: true });
								  })
									.goto('https://www.doordash.com')
			            .inject('js', 'inject-js/jquery.slim.min.js')
									.wait('input.Inputs_input___24QD5')
									.click('input.Inputs_input___24QD5')
									.insert('input.Inputs_input___24QD5', '')
									.insert('input.Inputs_input___24QD5', address)
									.wait(300)
									.wait('li.Autocomplete_isHighlighted___1zYQX')
//                  .wait(function() {
//                    return (document.querySelector('li.Autocomplete_isHighlighted___1zYQX') !== null) || (document.querySelector('div.AddressAutocomplete_noResults___2j3hZ') !== null); 
//                  })
//									.click('li.Autocomplete_isHighlighted___1zYQX button,div.AddressAutocomplete_noResults___2j3hZ')
									.click('li.Autocomplete_isHighlighted___1zYQX button')
									.wait(300)
									.wait('div.Grid_gridCell___1odvN')
//                  .wait('span.DeliveryFee_priceText___3XCOu')
//                  .wait(function() {
//                    return (document.querySelector('div.Grid_grid___2VcYy') !== null) || (document.querySelector('div.AddressAutocomplete_noResults___2j3hZ') !== null) || (document.querySelector('img.ComingSoon_stayTuned___3NazR') !== null); 
//                  })
                  .wait(function() {
                    return document.querySelectorAll('div.StoreInfo_storeInfo___2L3BQ').length >= 1
                  })				
									.wait(500)
									.evaluate(function() {
									 	var resultsToSave = [];
									 	$('div.SearchResults_root___UhxJr div.StoreInfo_storeInfo___2L3BQ').each(function(num) {
								   		if (num >= 20) {
								   			return false;
								   		}
								   		var name = $(this).find('div.StoreInfo_copy___1-w1- span:first').text();
								   		if (typeof name == 'undefined' || name == null || name.length == 0) {
								   			return true;
								   		}
								   		
								   		var eta = '';
								   		if ($(this).find('div.StoreInfo_copy___1-w1- div.StoreInfo_promotion___2yTZj').length > 0) {
								   			eta = $(this).find('div.StoreInfo_copy___1-w1- div.StoreInfo_promotion___2yTZj').text();
								   		} else {
								   			
								   		}
								   		
								   		var eta = $(this).find('div.StoreInfo_copy___1-w1- div:first span:last').text();
								   		if (typeof eta == 'undefined' || eta == null || eta.length == 0) {
								   			return true;
								   		}				
								   		eta = eta.replace('mins', '').trim();
								   		if(eta == 'Preorder' || eta == 'Closed' || eta == 'Unavailable') {
								        return true;
								      }
							   			var resultToSave = {};
									 		resultToSave.position = (num + 1);
									 		resultToSave.name = name.trim();
									 		resultToSave.eta = eta;
								   		resultToSave.href = 'https://www.doordash.com' + $(this).parents('a:first').attr('href').trim();
								   		resultsToSave.push(resultToSave);
								 		});
									 	return resultsToSave;
									})
								  .catch(function(error) {
								    if (DEBUG_MODE) {
								      const screenshotFileName = loggerLib.screenshotFileName('address_search_error');
					  		  	  nightmare.screenshot(screenshotFileName);
					  		  	  loggerLib.log(error, null, [screenshotFileName]);
								    } else {
								      loggerLib.log(error);
								    }
							  		return [];
							  	});
			
//			console.log(result);
//			if (result.length == 0) {
//			  yield nightmare.screenshot('./screenshots/dd_debug_address_' + ai + '_' + result.length + '.png');
//			}
//			break;
			
		  for (let k = 0; k < result.length; k++) {
		  	result[k].address = address;
		  	result[k].searchQuery = '';
		  	result[k].market = jobDescription.CBSA;
		  }
		  
		  if (result.length > 0) {
  	  	addressResult.push(...result);
  	  }
		  
		  loggerLib.log('SEARCH QUERY (EMPTY) END. Results: ' + result.length);
		  
			if (result.length > 0) {
			  
			  let searchQueryErrorCount = 0;
				
				for (let sqi = 0; sqi < searchQueries.length; sqi++) {
			  	let searchQuery = searchQueries[sqi];
			  	
			  	loggerLib.log('SEARCH QUERY (' + searchQuery + ') START: ' + (sqi+1) + '/' + searchQueries.length);
			  	
//			  	waitTimeOut = (Math.floor(Math.random() * 1000) + 300);
			  	
//			  	loggerLib.log('Waiting for ' + waitTimeOut + ' miliseconds...');
			  	
			  	var result2 = yield nightmare
//			  	              .wait(waitTimeOut)
			  	              .click('div.SearchInput_root___3qQVa input')
												.insert('div.SearchInput_root___3qQVa input', '')
												.insert('div.SearchInput_root___3qQVa input', searchQuery)
												.wait('div.Option_verbatimOptionContainer___2Fs8U')
												.wait(250)
												.click('div.Option_verbatimOptionContainer___2Fs8U')
												.wait(1000)
												.wait('div.Grid_grid___2VcYy')
												.wait('p.HeaderTitle_subtitle___2iNBa')
			                  .wait(function() {
			                    return document.querySelectorAll('div.StoreInfo_storeInfo___2L3BQ').length >= 1 
			                  })	
												.wait(500)
												.evaluate(function() {
												 	var resultsToSave = [];
												 	$('div.SearchResults_root___UhxJr div.StoreInfo_storeInfo___2L3BQ').each(function(num) {
											   		if (num >= 20) {
											   			return false;
											   		}
											   		
											   		var eta = '';
											   		if ($(this).find('div.StoreInfo_copy___1-w1- div.StoreInfo_promotion___2yTZj').length > 0) {
											   			eta = $(this).find('div.StoreInfo_copy___1-w1- div.StoreInfo_promotion___2yTZj').text();
											   		} else {
											   			eta = $(this).find('div.StoreInfo_copy___1-w1- div:first span:last').text();
											   		}
											   		if (typeof eta == 'undefined' || eta == null || eta.length == 0) {
											   			return true;
											   		}
											   		eta = eta.replace('mins', '').trim();
											      if(eta == 'Preorder' || eta == 'Closed' || eta == 'Unavailable') {
											        return true;
											      }
											      var name = $(this).find('div.StoreInfo_copy___1-w1- span:first').text();
											   		if (typeof name == 'undefined') {
											   			return true;
											   		}											      
												 		var resultToSave = {};
												 		resultToSave.position = (num + 1);
												 		resultToSave.name = name.trim();
												 		resultToSave.eta = eta;
											   		resultToSave.href = 'https://www.doordash.com' + $(this).parents('a:first').attr('href').trim();
											   		resultsToSave.push(resultToSave);
											 		});
												 	return resultsToSave;
												})
											  .catch(function(error) {
											    if (DEBUG_MODE) {
  								  		  	const screenshotFileName = loggerLib.screenshotFileName('search_query_error');
  								  		  	nightmare.screenshot(screenshotFileName);
  								  		  	loggerLib.log(error, null, [screenshotFileName]);
											    } else {
											      loggerLib.log(error);
											    }
											    
											    if (searchQueryErrorCount >= 2) {
									          loggerLib.log('ERROR: TO MANY ERRORS FROM DD WHILE GETTING Search Query Results');
									          searchQueryErrorCount = 0;
									        } else {
									          sqi = sqi - 1;
	                          searchQueryErrorCount++;  
									        }
										  		return [];
										  	});
			  	
		      if (DEBUG_MODE) {
		        yield nightmare.screenshot('./screenshots/dd_debug_searchquery_' + ai + '_' + sqi + '_' + result2.length + '.png');
		      }
		      
				  for (let k = 0; k < result2.length; k++) {
				  	result2[k].address = address;
				  	result2[k].searchQuery = searchQuery;
				  	result2[k].market = jobDescription.CBSA;
				  }
				  
				  if (result2.length > 0) {
		  	  	addressResult.push(...result2);
		  	  }				  
				
			  	loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + result2.length);
				}
			}
			
			// Get Unique URL from Empty + All Query Results
		  var uniqueUrls = [];
		  for (let z = 0; z < addressResult.length; z++) {
		  	if (uniqueUrls.indexOf(addressResult[z].href) === -1) {
		  		uniqueUrls.push(addressResult[z].href);
		  	}
		  }
		  
		  loggerLib.log('UNIQUE URLS: ' + uniqueUrls.length);
		  
		  let uniqueUrlErrorCount = 0;
		  
		  let prevRequestIsError = false;
		  
		  for (let uui = 0; uui < uniqueUrls.length; uui++) {
		  	
		  	let startRest = process.hrtime();
		  	
		  	let uniqueUrl = uniqueUrls[uui];
		  	
		  	if (uniqueUrlErrorCount >= 5) {
		  		loggerLib.log('ERROR: TO MANY ERRORS FROM DD WHILE GETTING Unique Urls');	
		  		break;
		  	}
		  	
		  	loggerLib.log('UNIQUE URL: ' + (uui+1) + '/' + uniqueUrls.length + ' ' + uniqueUrl);
		  	
		  	let userAgent = configLib.getRandomUserAgentString();
		  	
//		  	waitTimeOut = (Math.floor(Math.random() * 300) + 0);
		  	
//		  	loggerLib.log('Waiting for ' + waitTimeOut + ' miliseconds...');		  	
		  	
		  	let storeData = yield nightmare
//		  									.wait(waitTimeOut)
		  									.useragent(userAgent)
 											  .filter({
											    urls: [
											           'https://cdn.doordash.com/static/styles/*',
											           'https://cdn.segment.com/*',
											           'https://static.criteo.net/*',
											           'https://ingest.doordash.com/*',
                                 'https://dx.steelhousemedia.com/*',
											           'https://js.stripe.com/*',
											           'https://sp.analytics.yahoo.com/*',
											           'https://bam.nr-data.net/*',
											           'https://bat.bing.com/*',
											           'https://js-agent.newrelic.com/*',
											           'https://www.google-analytics.com/*',
											           'https://www.googleadservices.com/*',
											           'https://connect.facebook.net/*',
											           'https://cdn.nanigans.com/*',
											           'https://cdn.doordash.com/static/css/fonts/*',
											           'https://typography.doordash.com/*',
											           'https://apis.google.com/*'
											          ],
											  }, function(details, cb) {
											    return cb({ cancel: true });
											  })		  									
		  									.goto(uniqueUrl)
												.wait('h1.StorePictureHeader_storeName___1aCTo')
												.wait('div.MenuPage_storeAddress___1MRAI')
												.inject('js', 'inject-js/jquery.min.js')
//												.evaluate(function() {
//													if ($('button.OrderItem_action___ALyHj').length > 0) {
//														$('button.OrderItem_action___ALyHj').each(function(num) {
//														  $(this).attr('id', 'remove-btn-' + num);
//													    var elem = document.querySelector('#remove-btn-' + num);
//													    var event = document.createEvent('MouseEvent');
//													    event.initEvent('click', true, true);
//													    elem.dispatchEvent(event);	  
//														});
//													 }
//												  }
//												)
//												.wait(500)
												.evaluate(function() {
													var minPriceItem = '';
													minPriceItem = $('button.Item_root___Ez8cp:first');
													$('button.Item_root___Ez8cp').each(function(num) {
														if ($(this).find('span.Item_price___1Aqfj').length > 0) {
															if (parseFloat($(this).find('span.Item_price___1Aqfj').text().replace('$', '')) < parseFloat($(minPriceItem).find('span.Item_price___1Aqfj').text().replace('$', '')) && parseFloat($(this).find('span.Item_price___1Aqfj').text().replace('$', '')) > 0) {
																minPriceItem = $(this);
															}
														}
													});
													if (minPriceItem.length > 0) {
													  $(minPriceItem).attr('id', 'min-price-menu-item');
												    var elem = document.querySelector('#min-price-menu-item');
												    var event = document.createEvent('MouseEvent');
												    event.initEvent('click', true, true);
												    elem.dispatchEvent(event);	  			
													} else {
													  return {};
													}
												})
												.wait(150)
												.wait('button[aria-label="Add to Cart"]')
												.evaluate(function() {
													if ($('label[color="#808080"]').length > 0) {
														$('label[color="#808080"]').each(function(numm) {
															if ($(this).text().indexOf('Required') >= 0) {
																var that = this;
																setTimeout(function () {
												  				  $(that).parent().parent().find('input:first').attr('id', 'option_' + numm);
													 		      var elem = document.querySelector('#option_' + numm);
															      var event = document.createEvent('MouseEvent');
												 			      event.initEvent('click', true, true);
															      elem.dispatchEvent(event);
																}, 10);
															}
														});
													}
												})
												.wait(100)
                        .evaluate(function() {
                          if ($('button[aria-label="Add to Cart"]').length == 0) {
                          	
  													if ($('label[color="#808080"]').length > 0) {
  														$('label[color="#808080"]').each(function(numm) {
  															if ($(this).text().indexOf('Required') >= 0) {
  																var that = this;
  																setTimeout(function () {
  												  				  $(that).parent().parent().find('input:first').attr('id', 'option2_' + numm);
  													 		      var elem = document.querySelector('#option2_' + numm);
  															      var event = document.createEvent('MouseEvent');
  												 			      event.initEvent('click', true, true);
  															      elem.dispatchEvent(event);
  																}, 10);
  															}
  														});
  													}
  													
  													setTimeout(function () {
  														$('div[role="dialog"] button[kind="BUTTON/PRIMARY"]').attr('id', 'savebutton');
  									 		      var elem = document.querySelector('#savebutton');
												      var event = document.createEvent('MouseEvent');
									 			      event.initEvent('click', true, true);
												      elem.dispatchEvent(event);
  													}, 50);
  													
                          }
                        })
												.wait(150)
												.wait('button[aria-label="Add to Cart"]')
												.click('button[aria-label="Add to Cart"]')
												.wait(100)
												.wait(function() {
													return (document.querySelector('a.CartHeader_checkoutButton___1v7S4') !== null) || (document.querySelector('div.OrderErrors_root___XOjFe') !== null) 
												})
												.inject('js', 'inject-js/moment-with-locales.min.js')
												.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
												.wait(1000)
												.evaluate((timeZone, done) => {
													var request = $.ajax({
													  url: "https://api.doordash.com/v2/consumer/me/order_cart/current_cart/",
													  method: "GET",
													  dataType: "json",
												    xhrFields: {
												    	withCredentials: true
												    }
													});
													request.done(function(msg) {
														var resultToSave = {};
														if (window.location.href.indexOf(msg.restaurant.slug) > 0) {
															resultToSave.cart = msg;
															resultToSave.store = msg.restaurant;
												   		resultToSave.dateTimeUTC0 = (new Date()).toISOString();
												   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
												   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
												   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');			   		
												   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
														} else{
															resultToSave.errorMsg = 'Slug not found in HREF: ' + window.location.href + ' SLUG: ' + msg.restaurant.slug;
															resultToSave.obj = msg;
														}
														done(null, resultToSave);	
													});
													
												}, jobDescription.TimeZone)
												.catch(function(error) {
												  if (uniqueUrlErrorCount == 0 || prevRequestIsError) {
												    uniqueUrlErrorCount++;  
												  }
													prevRequestIsError = true;
													if (DEBUG_MODE) {
													  const screenshotFileName = loggerLib.screenshotFileName('restaurant_page_error');
	                          nightmare.screenshot(screenshotFileName);
	                          loggerLib.log(error, null, [screenshotFileName]);  
													} else {
													  loggerLib.log(error);  
													}
										  		return {};
												});
		  	
		  	if (typeof storeData.store != 'undefined' && typeof storeData.cart != 'undefined') {
		  		loggerLib.log('Price Data Received. Run Time: ' + Math.round((process.hrtime(startRest)[0])));
		  	  prevRequestIsError = false;
		  	  uniqueUrlErrorCount = 0;
  		  	for (let x = 0; x < addressResult.length; x++) {
  		  		if (addressResult[x].href == uniqueUrl) {
  		  			if (typeof storeData.store.price_range != 'undefined') {
  		  				addressResult[x].price = storeData.store.price_range;
  		  			}
  		  			if (typeof storeData.store.tags != 'undefined') {
  		  				addressResult[x].cuisine = storeData.store.tags.join('|');
  		  			}
  		  			if (typeof storeData.cart.asap_time_range != 'undefined') {
  			  			let eta = storeData.cart.asap_time_range.replace(' mins', '');
  			  			if (eta.indexOf('-') >= 0) {
  			  				addressResult[x].eta = eta; 
  			  				addressResult[x].etaMin = eta.split('-')[0].trim();
  			  				addressResult[x].etaMax = eta.split('-')[1].trim();				   			
  				   		}
  		  			}
  		  			if (typeof storeData.cart.original_delivery_fee != 'undefined') {
  		  				addressResult[x].deliveryCheckoutPageFee = storeData.cart.original_delivery_fee / 100;
  		  			}
  		  			if (typeof storeData.cart.min_order_subtotal != 'undefined') {
  		  				addressResult[x].smallOrderMin = storeData.cart.min_order_subtotal / 100;
  		  			}		  			
  		  			if (typeof storeData.cart.min_order_fee != 'undefined') {
  		  				addressResult[x].smallOrderFee = storeData.cart.min_order_fee / 100;
  		  			}
  		  			if (typeof storeData.cart.applied_service_rate != 'undefined') {
  		  				addressResult[x].serviceFee = storeData.cart.applied_service_rate;
  		  			}
  						if (typeof storeData.store.merchant_promotions != 'undefined') {
  							for (let n = 0; n < storeData.store.merchant_promotions.length; n++) {
  								if (storeData.store.merchant_promotions[n].delivery_fee === 0) {
  									addressResult[x].freeDeliveryMinOrder = storeData.store.merchant_promotions[n].minimum_order_cart_subtotal / 100;
  								}
  							}
  						}
  						addressResult[x].dateTimeUTC0 = storeData.dateTimeUTC0;
  						addressResult[x].dateTimeLocal = storeData.dateTimeLocal;
  						addressResult[x].dateLocal = storeData.dateLocal;
  						addressResult[x].hourLocal = storeData.hourLocal;
  						addressResult[x].dayLocal = storeData.dayLocal;
  				  }
  		  	}
		  	} else {
		  		loggerLib.log('ERROR: Price Data NOT Received for: ' + uniqueUrl);
		  		if (typeof storeData.errorMsg != 'undefined') {
		  			loggerLib.log('Error MSG: ' + storeData.errorMsg);	
		  		}
		  		if (typeof storeData.obj != 'undefined' && typeof storeData.obj.restaurant != 'undefined') {
		  			loggerLib.log(storeData.obj.restaurant);
		  		}
		  	}
		  	
	  	}
		
			resultsGlobal.push(...addressResult);
			loggerLib.log('ADDRESS END. Results: ' + addressResult.length);
			yield nightmare.cookies.clearAll();
		}
	}
	
	if (!DEBUG_MODE) {
	  const threadTime = Math.round((process.hrtime(start)[0]/60));
	  const cloudWatchLib = require("./lib/cloudwatch");
	  yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads2', THREAD_ID);
	  let priceResultsCount = 0;
	  for (let resInd = 0; resInd < resultsGlobal.length; resInd++) {
	  	if (typeof resultsGlobal[resInd].price != 'undefined') {
	  		priceResultsCount++;
	  	}
	  }
	  if (resultsGlobal.length > 0) {
	  	let fullRowsPercent = 100 - Math.round((resultsGlobal.length-priceResultsCount)/resultsGlobal.length * 100);
		  yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Full Row Count Percent', fullRowsPercent, WEBSITE_NAME + '-Threads2', THREAD_ID);
		  loggerLib.log('Full Row Count Percent: ' + fullRowsPercent + '%');
	  }
	}
		
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
	if (resultsGlobal.length > 0) {
	  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID + '-' + ADDRESS_INDEX);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
	if (nightmare) {
	  nightmare.end();
	  nightmare.halt();
	}
	
	process.exit();
});