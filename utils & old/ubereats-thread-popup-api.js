// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-popup-api.js 1 2

const configLib = require("./lib/config");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const proxyLib = require('./lib/proxy');
const addressCoordsLib = require('./lib/address_coords');
const slugid = require('slugid');
const util = require('util');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');
const json2csv = require('json2csv');
const fs = require('fs');
const striptags = require('striptags');
const csv = require('csvtojson');

process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

// Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.UBEREATS_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
let TOTAL_THREADS = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
		TOTAL_THREADS = val;
  }
});

DEBUG_MODE = true;
loggerLib.threadId = THREAD_ID;

function chunkArray(myArray, chunk_size){
  var index = 0;
  var arrayLength = myArray.length;
  var tempArray = [];
  
  for (index = 0; index < arrayLength; index += chunk_size) {
      myChunk = myArray.slice(index, index+chunk_size);
      // Do something if you want with the group
      tempArray.push(myChunk);
  }

  return tempArray;
}

//'market',

const fields = [
                 'address', 'id', 'name', 'restAddress', 'href', 'hostRest' 
               ];

loggerLib.log('--- START');

var nightmare;

// Main RUN Function
const run = function * () {
	
	const resultsGlobal = [];
	
	let CONFIG = yield function(){
		return new Promise((res, rej) => {
			let result = [];
			csv({
				delimiter: ",",
			})
			.fromFile('./cache/address_coordinates.csv')
			.on('json', (json) => {
				result.push(json);
			})
			.on('error', (error)=>{
				rej(error);
			})
			.on('end', () => {
				res(result);	
			});
		});
	}();
	
	loggerLib.log('THREAD ' + THREAD_ID + ' OUT OF ' + TOTAL_THREADS);
	
	CONFIG = chunkArray(CONFIG, Math.ceil(CONFIG.length / TOTAL_THREADS))[THREAD_ID-1];
	
  nightmare = new Nightmare({
    waitTimeout: 30000,
    executionTimeout: 100000,
    show: false,
	  switches: {
			'ignore-certificate-errors': true
	  },		    
    webPreferences: {
    	partition: "ue-partition-" + Math.random(),
      webaudio: false,
      images: false,
      plugins: false,
      webgl: false,   
      webSecurity: false
    }
  });
  
  nightmare.on('console', (log, msg) => {
  	console.log(msg);
//	 	if (msg.indexOf('CONSOLE:') >= 0) {
//	 		loggerLib.log(msg);
//	 	}
  });
  nightmare.on('close', (data) => {
  	loggerLib.log('Child process closing: ' + data);
  });
  nightmare.on('exit', (data) => {
  	loggerLib.log('Child process exiting: ' + data);
  });
  nightmare.on('error', (data) => {
  	loggerLib.log('Child process error: ' + data);
  });
  nightmare.on('disconnect', (data) => {
  	loggerLib.log('Child process disconnected: ' + data);
  });  
  
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
    parent.respondTo('proxy', function(host, done) {
        win.webContents.session.setProxy({
            proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
        }, function() {
            done();
        });
    });
    done();
	},
	function(host, done) {
	    this.child.call('proxy', host, done);
	
	});
  
  const curDateStr = (new Date()).toISOString().replace('T', ' ').split('.')[0].split(' ')[0];
  
	let homePageReload = true;
  
	// Loop Job Descriptions
	for (var jdi = 0; jdi < CONFIG.length; jdi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let jobDescription = CONFIG[jdi];
		
		let address = jobDescription.Address;
		
//		address = '1435 Collins Ave, Miami Beach, FL 33139';
//		jobDescription.Lat = 25.786691;
//		jobDescription.Lng = -80.1306331;
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + CONFIG.length + ' ADDRESS: ' + address);
		
		if (homePageReload) {
		  let userAgent = configLib.getRandomUserAgentString();
		  var homePageLoad = yield nightmare.useragent(userAgent).viewport(720, 1280)
							  .filter({
							    urls: [
							         	  'https://d3i4yxtzktqr9n.cloudfront.net/*',
							         	  'https://www.ubereats.com/static/*'
							          ],
							   }, function(details, cb) {
							     return cb({ cancel: true });
							   })     		  
							  .goto('https://www.ubereats.com/en-US/')
								.inject('js', 'inject-js/jquery.min.js')
								.wait('#address-selection-input')
							  .then(() => {
							  	return true;
						  	})
						  	.catch(function(error) {
							    if (DEBUG_MODE) {
							      const screenshotFileName = loggerLib.screenshotFileName('home_page_load_error');
				  		  	  nightmare.screenshot(screenshotFileName);
				  		  	  loggerLib.log(error, null, [screenshotFileName]);
							    } else {
							      loggerLib.log(error);
							    }
							    return false;
						  	});
		  
			loggerLib.log('Home Page Load: ' + homePageLoad);
			if (!homePageLoad) {
				continue;
			}
			homePageReload = false;
		}
		
	  var result = yield nightmare
	  	.evaluate((addressTimeZoneSearchQueries, done) => {
	  	
	  	var result = {};
		  result.rests = [];
		  result.errors = [];
	  	
	  	if (typeof window == 'undefined' || window == null) {
	  		console.log('CONSOLE: ERROR: window is not defined');
	  		done(null, result);	
	  	}
	  	
	    window.address = addressTimeZoneSearchQueries.address;
	    
	    
	 		var params = {};
			params.targetLocation = {};
			params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
			params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
			params.targetLocation.reference = '';
			params.targetLocation.type = "google_places";
			params.targetLocation.address = {};
			params.targetLocation.address.title = '';
			params.targetLocation.address.address1 = '';
			params.targetLocation.address.city = '';
			params.bafEducationCount = 20;
			params.feed = "combo";
			params.feedTypes = ['STORE', 'SEE_ALL_STORES'];
			params.feedVersion = 2;
	    
			var bootstrapEaterRequestResult = $.ajax({
				url: "https://www.ubereats.com/rtapi/eats/v1/bootstrap-eater",
				method: "POST",
				dataType: "json",
				data: JSON.stringify(params),
				cache: false,
				crossDomain: true,
				contentType: "application/json",
				beforeSend: function(xhr) {
					xhr.setRequestHeader('Accept', '*/*');
					xhr.setRequestHeader('Cache-Control', 'max-age=0');
					xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
					xhr.setRequestHeader('x-csrf-token', window.csrfToken);
				},
				xhrFields: {
					withCredentials: true
				},
				async: false,
        timeout: 10000
	  	}).responseText;
	    
			var categoryList = [];
	    if (bootstrapEaterRequestResult) {
	    	bootstrapEaterRequestResult = JSON.parse(bootstrapEaterRequestResult);
	  		if (bootstrapEaterRequestResult && bootstrapEaterRequestResult.marketplace && bootstrapEaterRequestResult.marketplace.search && bootstrapEaterRequestResult.marketplace.search.suggestedSearches.length > 0) {
	  			console.log('CONSOLE: ' + window.address + '| Found ' + bootstrapEaterRequestResult.marketplace.search.suggestedSearches.length + ' Categories');
	  			categoryList = bootstrapEaterRequestResult.marketplace.search.suggestedSearches;
	  		}
	    	
	    }
	    
//	    // Get All Categories
//    	var params = {};
//			params.targetLocation = {};
//			params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
//			params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
//	  	var categoriesRequestResult = $.ajax({
//				url: "https://www.ubereats.com/rtapi/eats/v1/city/categories",
//				method: "POST",
//				dataType: "json",
//				data: JSON.stringify(params),
//				cache: false,
//				crossDomain: true,
//				contentType: "application/json",
//				beforeSend: function(xhr) {
//					xhr.setRequestHeader('Accept', '*/*');
//					xhr.setRequestHeader('Cache-Control', 'max-age=0');
//					xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
//					xhr.setRequestHeader('x-csrf-token', window.csrfToken);
//				},
//				xhrFields: {
//					withCredentials: true
//				},
//				async: false,
//        timeout: 30000
//	  	}).responseText;
//	  	var categoryList = [];
//	  	if (categoriesRequestResult) {
//	  		categoriesRequestResult = JSON.parse(categoriesRequestResult);
//	  		if (categoriesRequestResult && categoriesRequestResult.categories && categoriesRequestResult.categories.length > 0) {
//	  			categoryList = categoriesRequestResult.categories[0];
//	  			console.log('CONSOLE: ' + window.address + '| Found ' + categoryList.items.length + ' ' + categoryList.title + ' Categories');
//	  			categoryList = categoryList.items;
//	  		}
//	  	}
	    

	  	// Category by category
	  	for (var cId = 0; cId < categoryList.length; cId++) {
	  		var category = categoryList[cId].uuid.replace(/\s/g, '').replace(/[^a-z0-9-]+/gi, '');
	  		console.log('CONSOLE: ' + window.address + '| Starting Category ' + category);
	  		var params = {};
				params.targetLocation = {};
				params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
				params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);	  		
	  		params.keyName = category;
		  	var searchRequestResult = $.ajax({
					url: "https://www.ubereats.com/rtapi/eats/v1/search/category",
					method: "POST",
					dataType: "json",
					data: JSON.stringify(params),
					cache: false,
					crossDomain: true,
					contentType: "application/json",
					beforeSend: function(xhr) {
						xhr.setRequestHeader('Accept', '*/*');
						xhr.setRequestHeader('Cache-Control', 'max-age=0');
						xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
						xhr.setRequestHeader('x-csrf-token', window.csrfToken);
					},
					xhrFields: {
						withCredentials: true
					},
					async: false,
	        timeout: 10000
		  	}).responseText;
	  		
		  	if (searchRequestResult) {
		  		searchRequestResult = JSON.parse(searchRequestResult)
		  		if (searchRequestResult.results) {
		  			console.log('CONSOLE: ' + window.address + '| Category ' + category + ' Rests: ' + searchRequestResult.results.length);
			  		for (var fi = 0; fi < searchRequestResult.results.length; fi++) {
			  			var feedItem = searchRequestResult.results[fi];
			  			if (feedItem.type == 'store') {
			  				feedItem = feedItem.store;
			  				if (feedItem.storeBadges && feedItem.storeBadges.endorsement && feedItem.storeBadges.endorsement.text && feedItem.storeBadges.endorsement.text.indexOf('pop-up') >= 0) {
			  					var restToSave = {};
			  					restToSave.id = feedItem.uuid;
			  					restToSave.name = feedItem.title;
			  					restToSave.badgeText = feedItem.storeBadges.endorsement.text;
			  					restToSave.storeInfo = feedItem;
			  					result.rests.push(restToSave);
			  					console.log('FOUND POPUP!!!');
			  				}
			  			}
			  		}
		  		}
		  	}
		  	
//		  	if (cId >= 5) {
//		  		break;
//		  	}
	  	}
	  	
//	  	console.log('CONSOLE: POPUP Rests: ' + result.rests.length);
	  	
			done(null, result);	
  	}, {address: address, latLng: {lat: jobDescription.Lat, lng: jobDescription.Lng}})
  	.catch(function(error) {
  	  loggerLib.log(error);
  	  return null;
	  });
	  
	  
	  if (result && result.rests.length > 0) {
	  	
	  	for (var ri = 0; ri < result.rests.length; ri++) {
	  		
	  		var alreadyExists = false;
	  		resultsGlobal.forEach(function(existingRest){
	  			if (existingRest.id == result.rests[ri].id) {
	  				alreadyExists = true;
	  				return;
	  			}
	  		});

	  		if (!alreadyExists) {
//					result.rests[ri].market = jobDescription.CBSA;
					result.rests[ri].address = address;
		  		var craftedBy = striptags(result.rests[ri].badgeText).trim();
					var craftedByIndex = craftedBy.indexOf('crafted');
		  		result.rests[ri].hostRest = striptags(result.rests[ri].badgeText).trim().substring(craftedByIndex + 11);
		  		delete result.rests[ri].badgeText;
		  		
					let uuidSlug = slugid.encode(result.rests[ri].id);
					let nameSlug = result.rests[ri].name.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
					result.rests[ri].href = 'https://www.ubereats.com/en-US/city/food-delivery/' + nameSlug + '/' + uuidSlug
					
					result.rests[ri].restAddress = result.rests[ri].storeInfo.location.address.formattedAddress;
					delete result.rests[ri].storeInfo;
					
					console.log(result.rests[ri]);	
		  		resultsGlobal.push(result.rests[ri]);
	  		}
	  		
	  	}
	  	
			const threadTime = Math.round((process.hrtime(start)[0]/60));
	  	loggerLib.log('--- Results Saved: ' + resultsGlobal.length + ' --- Execution Time (minutes): ' + threadTime);
	  	let fName = 'ue-popup-results-' + THREAD_ID + '.csv';
			if (fs.existsSync(fName)) {
				fs.unlinkSync(fName);
			}
			fs.writeFileSync(fName, json2csv({data: resultsGlobal, fields: fields}), 'ascii');		  	
	  	
	  	loggerLib.log('ADDRESS END|' + address + '|Results: ' + result.rests.length);
	  } else {
	  	loggerLib.log('ADDRESS END|' + address + '|NO Results');
	  }
			
	} // JOB END
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
	
	return resultsGlobal;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
  
  loggerLib.log('Process exit');
  process.exit();
});