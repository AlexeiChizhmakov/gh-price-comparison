// Xvfb -ac -screen scrn 1280x2000x24 :9.0 &  
// export DISPLAY=:9.0  
// DEBUG=nightmare:*,electron:* node --harmony ubereats-thread.js 1
// DEBUG=nightmare:actions,electron:* node --harmony ubereats-thread.js 1 &>> ./logs/ubereats-thread1.log&
// node --harmony ubereats-thread.js 1
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony ubereats-thread-test.js 1 debug
// node --harmony ubereats-thread.js 1 &>> ./logs/ubereats-thread1.log&
const configLib = require("./lib/config");
const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
var vo = require('vo');


// Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.UBEREATS_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

const resultsGlobal = [];

var nightmare;

// Main RUN Function
const run = function * () {
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
  if (jobDescriptions.length == 0) {
    return;
  } 
  
  nightmare = new Nightmare({
    waitTimeout: 30000,
    show: false,
    webPreferences: {
      webaudio: false,
      images: false,
      plugins: false,
      webgl: false,   
    }
  });  
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length + ' SEARCH QUERIES: ' + searchQueries.length);
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let userAgent = configLib.getRandomUserAgentString();
			
			let addressResult = [];
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			loggerLib.log('User Agent: ' + userAgent);
			
			loggerLib.log('SEARCH QUERY (EMPTY) START');
			
		  var result = yield nightmare.useragent(userAgent).viewport(1600, 5000).goto('https://www.ubereats.com/columbus/')
				.inject('js', 'inject-js/jquery.slim.min.js')
				.inject('js', 'inject-js/moment-with-locales.min.js')
				.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')		  
				.wait('#address-selection-input')
				.type('#address-selection-input', '')
			  .insert('#address-selection-input', address)
			  .wait(500)
				.wait(function() {
					return (document.querySelector('div.iconLoadingContainer_') === null) 
				})
			  .evaluate(function () {
				  var elem = document.querySelector('#address-selection-input');
				  var event = document.createEvent('MouseEvent');
				  event.initEvent('mousedown', true, true);
				  elem.dispatchEvent(event);		  	
			  	$('#address-selection-input').focus();
			  })
        .wait(function() {
          return (document.querySelector('button.lastPrediction_') !== null) || (document.querySelector('div.centered_') !== null) 
        })
        .evaluate(function() {
          if ($("button.prediction_:first").length > 0) {
            $("button.prediction_:first").attr('id', 'my-first-a');
          } else if ($("button.lastPrediction_:first").length > 0) {
            $("button.lastPrediction_:first").attr('id', 'my-first-a');
          }
          var elem = document.querySelector('#my-first-a');
          if (elem) {
            var event = document.createEvent('MouseEvent');
            event.initEvent('click', true, true);
            elem.dispatchEvent(event);
          }
        })
			 .wait(function() {
					return (document.querySelector('div.loadMore_') !== null) || (document.querySelector('div.centered_') !== null)  || (document.querySelector('div.headerTitle_') !== null)
			 })			 
			 .wait(500)
			 .evaluate((timeZone) => {
			 	var resultsToSave = [];
			 	$('a.base_').each(function(num) {
			 		var resultToSave = {};
			 		if ($(this).find('div.title_').length > 0) {
			 			resultToSave.name = $(this).find('div.title_').text().trim();
			   		resultToSave.href = 'https://www.ubereats.com' + $(this).attr('href').trim();
			   		var list = $(this).find('div.list_').text().trim().split('•');
			   		resultToSave.cuisine = '';
			   		if (list.length > 1) {
			   		  resultToSave.cuisine = list.map(Function.prototype.call, String.prototype.trim).join('|').trim();
			   		}
			   		resultToSave.price = $(this).find('div.pillContainer_ div:first div:eq(0) div:first').text().trim().length;
			   		var eta = $(this).find('div.pillContainer_ div:first div:eq(2) div:first').text().trim();
			   		if (eta.indexOf('Opens') === 0 || eta.indexOf('Currently unavailable') === 0) {
			   			return false;
			   		}					   		
			   		resultToSave.eta = eta; 
			   		resultToSave.etaMin = '';
			   		resultToSave.etaMax = '';
			   		if (eta.indexOf('–') >= 0) {
				   		resultToSave.etaMin = eta.split('–')[0].trim();
				   		resultToSave.etaMax = eta.split('–')[1].trim().split(' ')[0].trim();				   			
			   		}
			   		resultToSave.dateTimeUTC0 = (new Date()).toISOString();
			   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
			   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
			   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');			   		
			   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
			   		resultsToSave.push(resultToSave);
			 	  }
		   		if (num >= 20) {
		   			return false;
		   		}		 		
			 	});
			 	
			 	return resultsToSave;
			 }, jobDescription.TimeZone)
		   .catch(function(error) {
		  	 if (DEBUG_MODE) {
		  		 const screenshotFileName = loggerLib.screenshotFileName('address_search_error'); 
		  		 nightmare.screenshot(screenshotFileName);
		  		 loggerLib.log(error, null, [screenshotFileName]);
		  	 } else {
		  		 loggerLib.log(error); 
		  	 }
		  	 return [];
		   });    
		  
		  for (let k = 0; k < result.length; k++ ) {
		  	result[k].position = k + 1;
		  	result[k].address = address;
		  	result[k].searchQuery = '';
		  	result[k].market = jobDescription.CBSA;
		  }
		  
		  addressResult.push(...result);
		  loggerLib.log('SEARCH QUERY (EMPTY) END. Results: ' + result.length);
		  
		  if (false) {
		  	
			  for (let sqi = 0; sqi < searchQueries.length; sqi++) {
			  	let searchQuery = searchQueries[sqi];
			  	
			  	loggerLib.log('SEARCH QUERY (' + searchQuery + ') START: ' + (sqi+1) + '/' + searchQueries.length);
			  	
			  	let waitTimeOut = (Math.floor(Math.random() * 500) + 100);
			  	
			  	loggerLib.log('Waiting for ' + waitTimeOut + ' miliseconds...');
			  	
			  	var result2 = yield nightmare
		  	  .wait('form.inputWrapper_')
		  	  .wait(waitTimeOut)
		  		.evaluate(function () {
		  			$("form.inputWrapper_ input:first").attr('id', 'search-input-id');
		  		})
		  		.insert('#search-input-id', '\u0008')
		  		.insert('#search-input-id', searchQuery)
		  		.type('#search-input-id', '\u000d')
		  		.wait(5000)
			 	  .evaluate((timeZone) => {
					 	var resultsToSave = [];
					 	$('a.base_').each(function(num) {
					 		var resultToSave = {};
					 		if ($(this).find('div.title_').length > 0) {
					 			resultToSave.name = $(this).find('div.title_').text().trim();
					   		resultToSave.href = 'https://www.ubereats.com' + $(this).attr('href').trim();
					   		var list = $(this).find('div.list_').text().trim().split('•');
					   		resultToSave.cuisine = '';
					   		if (list.length > 1) {
	  			   		  resultToSave.cuisine = list.map(Function.prototype.call, String.prototype.trim).join('|').trim();
					   		}
					   		resultToSave.price = $(this).find('div.pillContainer_ div div:eq(0) div').text().trim().length;
					   		var eta = $(this).find('div.pillContainer_ div div:eq(1) div').text().trim();
					   		if (eta.indexOf('Opens') === 0 || eta.indexOf('Currently unavailable') === 0) {
					   			return false;
					   		}					   		
					   		resultToSave.eta = eta; 
					   		resultToSave.etaMin = '';
					   		resultToSave.etaMax = '';
					   		if (eta.indexOf('–') >= 0) {
						   		resultToSave.etaMin = eta.split('–')[0].trim();
						   		resultToSave.etaMax = eta.split('–')[1].trim().split(' ')[0].trim();				   			
					   		}
					   		resultToSave.dateTimeUTC0 = (new Date()).toISOString();
					   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
					   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
					   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');
					   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
					   		resultsToSave.push(resultToSave);
					 	  }
				   		if (num >= 20) {
				   			return false;
				   		}
					 	});
			 	
					 	return resultsToSave;
				  }, jobDescription.TimeZone)
				  .catch(function(error) {
				  	if (DEBUG_MODE) {
//				  		if (error.stack.indexOf('.wait()') < 0) {
				  		const screenshotFileName = loggerLib.screenshotFileName('search_query_error');
					  	nightmare.screenshot(screenshotFileName);
					  	loggerLib.log(error, null, [screenshotFileName]);
//				  		}
				  	} else {
				  		loggerLib.log(error);
				  	}
				  	return 'ERROR';				  	
				  });
			  	
			  	if (result2 === 'ERROR') {
			  		break;
			  	}
			  	
		  	  for (var k = 0; k < result2.length; k++) {
		  	  	result2[k].address = address;
		  	  	result2[k].position = k + 1;
		  	  	result2[k].searchQuery = searchQuery;
		  	  	result2[k].market = jobDescription.CBSA;
	  	  	}
		  	  
			  	// Check if "We are not there yet"
			  	let weArnetThereYet = yield nightmare
			  	.evaluate(function () {
			  		let weArnetThereYetResult = false;
			  		if ($('div.headerTitle_').length > 0 && $('div.headerTitle_').text().indexOf('not') >= 0) {
			  			weArnetThereYetResult = true;
			  		}
  	        return weArnetThereYetResult;
          })
          .catch(function(error) {
          	if (DEBUG_MODE) {
          		const screenshotFileName = loggerLib.screenshotFileName('search_query_we_arnet_there_yet_error'); 
				  		nightmare.screenshot(screenshotFileName);
				  		loggerLib.log(error, null, [screenshotFileName]);
          	} else {
          		loggerLib.log(error);	
          	}
				  	return [];				  	
				  });
			  	if (weArnetThereYet) {
			  		break;
			  	}		  	  
		  	  
		  		addressResult.push(...result2);
		  		loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + result2.length);
		  	}
		  }
		  
		  resultsGlobal.push(...addressResult);
			loggerLib.log('ADDRESS END. Results: ' + addressResult.length);
			
			yield nightmare.evaluate(() => {
		    window.localStorage.clear();
		  });			
		  yield nightmare.cookies.clearAll();
		  break;
		}
	}
	
	if (!DEBUG_MODE) {
		const threadTime = Math.round((process.hrtime(start)[0]/60));
		const cloudWatchLib = require("./lib/cloudwatch");
		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
	}

	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
	if (resultsGlobal.length > 0) {
	  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.end();
    nightmare.halt();
  }
  
});