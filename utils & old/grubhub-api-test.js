// node --harmony grubhub-api-test.js 1

const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
var request = require('request');
var vo = require('vo');


//Website Name
const WEBSITE_NAME = constants.GRUBHUB_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

//Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }  
});
loggerLib.threadId = THREAD_ID;

//loggerLib.log('--- START');


//Main RUN Function
const run = function * () {

	// Get Auth Token
	let authResponse = yield function() {
		return new Promise((res, rej) => {	
			
			
			// PRE PROD
			const clientId = 'itirra_aXRpcnJhX2h';
			const baseUrl = 'https://api-pp.grubhub.com';
				
  		// PROD
			const clientId = 'itirra_aWhhdGVpdGlycmFwcmljZXB';
			const baseUrl = 'https://api-gtm.grubhub.com';	
				
			var options = {
					method: 'POST',
				  url: baseUrl + '/oauth2/direct/auth',
				  json: {
				    'grant_type': 'token',
				    'client_id': clientId,
				    'scope': 'anonymous',
				  }
			};
			request(options, function (error, response, body) {
				if (error) {					
					rej('ERROR getting Auth Token: ' + error);
				}
				if (!response || response.statusCode != 200) {					
					rej('ERROR getting Auth Token. Status Code: ' + response.statusCode);					
				}				
				res(body);
			});
		}); 
	}().then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return null;
  });
	
	console.log(authResponse);
	
	if (authResponse) {
		const authToken = authResponse.access_token;
		
		let restResponse = yield function() {
			return new Promise((res, rej) => {	
				var options = {
						method: 'GET',
						url: baseUrl + '/restaurants/search/search_listing',
						qs: {
							'locationMode': 'DELIVERY',
							'pageSize': '20',
							'location': 'POINT(-81.5892334 41.02938842)',
						},					  
					  'auth': {
					    'bearer': authToken
					  }						
				};
				request(options, function (error, response, body) {
					if (error) {					
						console.log(error);
						console.log(response);
						console.log(body);
						rej('ERROR getting Results: ' + error);
					}
					if (!response || response.statusCode != 200) {					
						console.log(error);
						console.log(response);
						console.log(body);						
						rej('ERROR getting Results. Status Code: ' + response.statusCode);					
					}				
					res(body);
				});
			}); 
		}().then(data => {
	    return data;
	  }).catch((error) => {
	  	loggerLib.log(error);
	  	return null;
	  });

		
		loggerLib.log(restResponse);
			
		
	}
	
	
	
	return;
}

vo(run)(function(error, results) {
	if (error) { 		
		console.log(error);
 		loggerLib.log(error);
	}
	if (results) {
		loggerLib.log(results);
	}
	
//	if (resultsGlobal.length > 0) {
//	  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
//	}
	
// 	const executionTime = Math.round((process.hrtime(start)[0]/60));
//	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);	
 
  process.exit();
});


//https://api-pp.grubhub.com/restaurants/search/search_listing?locationMode=DELIVERY&pageSize=5&location=POINT(-73.99124146%2040.73610687)
	
	

