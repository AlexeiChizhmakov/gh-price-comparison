// Xvfb -ac -screen scrn 1280x2000x24 :9.0 &  
// export DISPLAY=:9.0  
// DEBUG=nightmare:*,electron:* node --harmony grubhub-thread.js 1
// DEBUG=nightmare:actions,electron:* node --harmony grubhub-thread.js 1 &>> ./logs/grubhub-thread1.log&
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony grubhub-thread.js 1 debug
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony grubhub-thread.js 1 &>> ./logs/grubhub-thread000000000.log&
// node --harmony grubhub-thread.js 1 &>> ./logs/grubhub-thread1.log&

const configLib = require("./lib/config");
const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.GRUBHUB_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }  
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

const resultsGlobal = [];

var nightmare;

// Main RUN Function
const run = function * () {
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
  if (jobDescriptions.length == 0) {
    return;
  }	
  
  nightmare = new Nightmare({
    waitTimeout: 3000,
    show: false,
    webPreferences: {
      webaudio: false,
      images: false,
      plugins: false,
      webgl: false    
    }
  });  
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);		
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length + ' SEARCH QUERIES: ' + searchQueries.length);
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let addressResult = [];
			
			let address = addresses[ai].trim();
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			loggerLib.log('SEARCH QUERY (EMPTY) START');
			
//			let userAgent = 'Itirra Price Parser x' +  (Math.floor(Math.random() * 10000) + 100);
			let userAgent = configLib.getRandomUserAgentString();
			
		  var result = yield nightmare.useragent(userAgent)
		  .viewport(1600, 5000)
		  .filter({
		    urls: [
		           'https://*.optimizely.com/*',
		           'https://*.criteo.com/*',
		           'https://tags.tiqcdn.com/*',
		           'https://www.cdn-net.com/*',
		           'https://cdn.branch.io/*',
		           'https://cdn.contentful.com/*',
		           'https://connect.facebook.net/*',
		           'https://dev.appboy.com/*',
		           'https://bat.bing.com/*',
		           'https://client.perimeterx.net/*',
		           'https://assets.grubhub.com/libs/appboy/*',
		           'https://assets.grubhub.com/libs/clickstreamjs/*',
		           'https://assets.grubhub.com/assets/img/*',
		           'https://bam.nr-data.net/*'
		          ],
		   }, function(details, cb) {
		     return cb({ cancel: true });
		   })		  
		  .goto('https://www.grubhub.com?utm_medium=bot&utm_source=itirrapriceparser')
		  .inject('js', 'inject-js/jquery.slim.min.js')
			.wait('form.startOrder-form-inline input.addressInput-textInput')
			.click('form.startOrder-form-inline .ghs-clearInput-icon')
			.insert('form.startOrder-form-inline input.addressInput-textInput', address)
		  .click('form.startOrder-form-inline input.addressInput-textInput')
			.click('#ghs-startOrder-searchBtn')
			.wait(function() {
				return (document.querySelector('div.restaurantCard-search') !== null) || (document.querySelector('h3.no-searchResults-restaurantSection') !== null) || (document.querySelector('div.s-input-error') !== null); 
			})
			.inject('js', 'inject-js/moment-with-locales.min.js')
			.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')			
			.wait(500)
	 	  .evaluate((timeZone) => {
			 	var resultsToSave = [];
			 	if ($('h3.no-searchResults-restaurantSection:visible').length == 0) {
				 	$('div.restaurantCard-search').each(function() {
				 		var resultToSave = {};
			 			resultToSave.name = $(this).find('a.restaurant-name').text().trim();
			   		resultToSave.href = 'https://www.grubhub.com' + $(this).find('a.restaurant-name').attr('href');
			   		resultToSave.price = $(this).find('div.priceRating-value').text().trim().length;
			   		resultToSave.cuisine = $(this).find('span.restaurantCard-cuisines').text().replace('...', '').trim().split(', ').join('|');
			   		var eta = $(this).find('span.timeEstimate:visible:first').text().replace('–', '-');
			   		resultToSave.eta = eta;
			   		resultToSave.etaMin = '';
			   		resultToSave.etaMax = '';
			   		if (eta.indexOf('-') >= 0) {
				   		resultToSave.etaMin = eta.split('-')[0].trim();
				   		resultToSave.etaMax = eta.split('-')[1].replace('min', '').trim();				   			
			   		}
			   		resultToSave.minOrder = $(this).find('div.restaurantCard-deliveryStats-min span.value').text().trim();
			   		resultToSave.deliverySearchPageFee = '$0';
			   		var deliveryFeeElement = $(this).find('span.deliveryFee-variable');
			   		if (deliveryFeeElement.length > 0) {
			   			resultToSave.deliverySearchPageFee = deliveryFeeElement.text().trim();	
			   		}
			   		resultToSave.deliveryMenuPageFee = resultToSave.deliverySearchPageFee;
			   		resultToSave.dateTimeUTC0 = (new Date()).toISOString(); 
			   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
			   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
			   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');
			   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
			   		resultsToSave.push(resultToSave);
				 	});
			 	}
	 	
			 	return resultsToSave;
		  }, jobDescription.TimeZone)
		  .catch(function(error) {
		  	if (DEBUG_MODE) {
			  	const screenshotFileName = loggerLib.screenshotFileName('address_search_error'); 
			  	nightmare.screenshot(screenshotFileName);
			  	loggerLib.log(error, null, [screenshotFileName]);		  		
		  	} else {
			  	loggerLib.log(error);		  		
		  	}
		  	return [];
			});
		  
		  for (let k = 0; k < result.length; k++) {
		  	result[k].position = k + 1;
		  	result[k].address = address;
		  	result[k].searchQuery = '';
		  	result[k].market = jobDescription.CBSA;
		  }
		  
			for (let j = 0; j < result.length; j++) {
				if (result[j].deliverySearchPageFee.indexOf('+') >= 0) {
					var result3 = yield nightmare
												.insert('input.ghs-searchInput', '')
												.insert('input.ghs-searchInput', result[j].name)
												.type('input.ghs-searchInput', '\u000d')
												.wait('div.restaurantCard-search')
												.click('a.restaurant-name')
												.wait(400)
												.wait('div.simplifiedAddressForm-cartHeader')
												.inject('js', 'inject-js/jquery.slim.min.js')
												.evaluate(function() {
												 	var result = '';
												 	if (jQuery('div.simplifiedAddressForm-cartHeader-instructions--fees').length > 0) {
												 		result = jQuery('div.simplifiedAddressForm-cartHeader-instructions--fees span').text().trim().split(',')[1].trim().replace('delivery fee', '').trim();
												 	}
											 		if (result == 'Free delivery') {
											 			result = '$0';
											 		}													 	
												 	return result;
											  })
								  		  .catch(function(error) {
								  		  	if (DEBUG_MODE) {
									  		  	const screenshotFileName = loggerLib.screenshotFileName('address_restaurant_page_error');
									  		  	nightmare.screenshot(screenshotFileName);
									  		  	loggerLib.log(error, null, [screenshotFileName]);								  		  	
								  		  	} else {
									  		  	loggerLib.log(error);								  		  		
								  		  	}
								  		  	return '';
							  		  	});
					
					result[j].deliveryMenuPageFee = result3;
				}
			}
			
			if (DEBUG_MODE) {
			  yield nightmare.screenshot('./screenshots/gh_debug_address_' + ai + '_' + result.length + '.png');
			}			
			
		  addressResult.push(...result);
		  loggerLib.log('SEARCH QUERY (EMPTY) END. Results: ' + result.length);
		  
			for (let sqi = 0; sqi < searchQueries.length; sqi++) {
		  	let searchQuery = searchQueries[sqi];
		  	
		  	loggerLib.log('SEARCH QUERY (' + searchQuery + ') START: ' + (sqi+1) + '/' + searchQueries.length);
		  	
		  	var result2 = yield nightmare
	  	  .wait('input.ghs-searchInput')
  	  	.insert('input.ghs-searchInput', '')
	  		.insert('input.ghs-searchInput', searchQuery)
	  		.type('input.ghs-searchInput', '\u000d')
	  		.wait(200)
				.wait(function() {
					return (document.querySelector('div.restaurantCard-search') !== null) || (document.querySelector('h3.no-results-heading') !== null) 
				})		  		
	  		.wait(500)
		 	  .evaluate((timeZone) => {
				 	var resultsToSave = [];
				 	if ($('h3.no-results-heading:visible').length == 0) {
					 	$('div.restaurantCard-search').each(function() {
					 		var resultToSave = {};
				 			resultToSave.name = $(this).find('a.restaurant-name').text().trim();
				   		resultToSave.href = 'https://www.grubhub.com' + $(this).find('a.restaurant-name').attr('href');
				   		resultToSave.price = $(this).find('div.priceRating-value').text().trim().length;
				   		resultToSave.cuisine = $(this).find('span.restaurantCard-cuisines').text().replace('...', '').trim().split(', ').join('|');
				   		var eta = $(this).find('span.timeEstimate:visible:first').text().replace('–', '-');
				   		resultToSave.eta = eta;
				   		resultToSave.etaMin = '';
				   		resultToSave.etaMax = '';
				   		if (eta.indexOf('-') >= 0) {
					   		resultToSave.etaMin = eta.split('-')[0].trim();
					   		resultToSave.etaMax = eta.split('-')[1].replace('min', '').trim();				   			
				   		}
				   		resultToSave.minOrder = $(this).find('div.restaurantCard-deliveryStats-min span.value').text().trim();
				   		resultToSave.deliverySearchPageFee = '$0';
				   		var deliveryFeeElement = $(this).find('span.deliveryFee-variable');
				   		if (deliveryFeeElement.length > 0) {
				   			resultToSave.deliverySearchPageFee = deliveryFeeElement.text().trim();	
				   		}
				   		resultToSave.deliveryMenuPageFee = resultToSave.deliverySearchPageFee;
				   		resultToSave.dateTimeUTC0 = (new Date()).toISOString();
				   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
				   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
				   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');
				   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
				   		resultsToSave.push(resultToSave);
					 	});
				 	}
				 	return resultsToSave;
			  }, jobDescription.TimeZone)
  		  .catch(function(error) {
  		  	if (DEBUG_MODE) {
	  		  	const screenshotFileName = loggerLib.screenshotFileName('search_query_error');
	  		  	nightmare.screenshot(screenshotFileName);
	  		  	loggerLib.log(error, null, [screenshotFileName]);	  		  		
  		  	} else {
	  		  	loggerLib.log(error);	  		  		
  		  	}
  		  	return [];
		  	});					  
			  
			  
				for ( var o = 0; o < result2.length; o++ ) {
					if (result2[o].deliverySearchPageFee.indexOf('+') >= 0) {
						
						var result3 = yield nightmare
													.insert('input.ghs-searchInput', '')
													.insert('input.ghs-searchInput', result2[o].name)
													.type('input.ghs-searchInput', '\u000d')
													.wait('div.restaurantCard-search')
													.click('a.restaurant-name')
													.wait(400)
													.wait('div.simplifiedAddressForm-cartHeader')
													.inject('js', 'inject-js/jquery.slim.min.js')
													.evaluate(function() {
													 	var result = '';
													 	if (jQuery('div.simplifiedAddressForm-cartHeader-instructions--fees').length > 0) {
													 		result = jQuery('div.simplifiedAddressForm-cartHeader-instructions--fees span').text().trim().split(',')[1].trim().replace('delivery fee', '').trim();
													 	}
												 		if (result == 'Free delivery') {
												 			result = '$0';
												 		}														 	
													 	return result;
												  })
						  		  		  .catch(function(error) {
						  		  		  	if (DEBUG_MODE) {
										  		  	const screenshotFileName = loggerLib.screenshotFileName('search_query_restaurant_page_error');
										  		  	nightmare.screenshot(screenshotFileName);
										  		  	loggerLib.log(error, null, [screenshotFileName]);							  		  		  		
						  		  		  	} else {
							  		  		  	loggerLib.log(error);							  		  		  		
						  		  		  	}
						  		  		  	return '';
					  		  		  	});
						
						result2[o].deliveryMenuPageFee = result3;
						
					}
				}
	  		
	  		
	  	  for ( var k = 0; k < result2.length; k++ ) {
	  	  	result2[k].address = address;
	  	  	result2[k].position = k + 1;
	  	  	result2[k].searchQuery = searchQuery;
	  	  	result2[k].market = jobDescription.CBSA;
  	  	}
	  	  
	      if (DEBUG_MODE) {
	        yield nightmare.screenshot('./screenshots/gh_debug_searchquery_' + ai + '_' + sqi + '_' + result2.length + '.png');
	      }	  	  
	  	
	  	  if (result2.length > 0) {
	  	  	addressResult.push(...result2);
	  	  }
	  	  
	  	  loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + result2.length);
		  }
			
			resultsGlobal.push(...addressResult);
			loggerLib.log('ADDRESS END. Results: ' + addressResult.length);
			
			yield nightmare.evaluate(() => {
		    window.localStorage.clear();
		  });
			yield nightmare.cookies.clearAll();
		}
	}
	
	if (!DEBUG_MODE) {
		const threadTime = Math.round((process.hrtime(start)[0]/60));
		const cloudWatchLib = require("./lib/cloudwatch");
		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
	}
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
	if (resultsGlobal.length > 0) {
	  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.end();
    nightmare.halt();
  }
  
  process.exit();
});