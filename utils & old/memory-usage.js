//node --harmony memory-usage.js

const shell = require('shelljs');

let vmstat = shell.exec('vmstat -s', {silent:true}).stdout;

let vmstatArr = vmstat.split("\n");

let totalRam = 0;
let totalUsedRam = 0;

for (let i = 0; i < vmstatArr.length; i++) {
	let vmstatRow = vmstatArr[i].trim();
	if (vmstatRow.indexOf('used memory') > 0) {
		totalUsedRam = parseInt(vmstatRow.split('K')[0].trim());
	}
	if (vmstatRow.indexOf('total memory') > 0) {
		totalRam = parseInt(vmstatRow.split('K')[0].trim());
	}		
}

totalUsedRam = Math.round(totalUsedRam/1000 * 100) / 100;
totalRam = Math.round(totalRam/1000 * 100) / 100;
let totalUsedRamPercent = Math.round(totalUsedRam / totalRam * 100) / 100;

const cloudWatchLib = require("./lib/cloudwatch");

cloudWatchLib.logMetric('System', 'RAM Used (%)', totalUsedRamPercent).then(function() {
//	console.log('Ram Used: ' + totalUsedRamPercent);
	process.exit();
});
