// Xvfb -ac -screen scrn 1280x2000x24 :9.0 &  
// export DISPLAY=:9.0  
// DEBUG=nightmare:*,electron:* node --harmony ubereats-thread.js 1
// DEBUG=nightmare:actions,electron:* node --harmony ubereats-thread.js 1 &>> ./logs/ubereats-thread1.log&
// node --harmony ubereats-thread.js 1
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony ubereats-thread.js 1 debug
// DEBUG=nightmare:actions,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony ubereats-thread.js 1 debug
// node --harmony ubereats-thread.js 1 &>> ./logs/ubereats-thread1.log&
const configLib = require("./lib/config");
const resultsLib = require("./lib/results");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

// Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.UBEREATS_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

const resultsGlobal = [];

var nightmare;

// Main RUN Function
const run = function * () {
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
  if (jobDescriptions.length == 0) {
    return;
  } 
  
  nightmare = new Nightmare({
    waitTimeout: 10000,
    show: false,
    webPreferences: {
      webaudio: false,
      images: false,
      plugins: false,
      webgl: false,   
    }
  });  
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length + ' SEARCH QUERIES: ' + searchQueries.length);
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let userAgent = configLib.getRandomUserAgentString();
			
			let addressResult = [];
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			loggerLib.log('User Agent: ' + userAgent);
			
			loggerLib.log('SEARCH QUERY (EMPTY) START');
			
		  var result = yield nightmare.useragent(userAgent).viewport(1600, 5000)
        .filter({
          urls: [
                 'https://d3i4yxtzktqr9n.cloudfront.net/web-eats/stylesheets/*',
                 'https://www.ubereats.com/static/*',
                 'https://tags.tiqcdn.com/*',
                 'https://www.cdn-net.com/*',
                 'https://www.google-analytics.com/*',
                 'https://analytics.twitter.com/*',
                 'https://pixel.mathtag.com/*',
                 'https://uber.demdex.net/*',
                 'https://six.cdn-net.com/*',
                 'https://sp.analytics.yahoo.com/*'
                ],
         }, function(details, cb) {
           return cb({ cancel: true });
         })     		  
		    .goto('https://www.ubereats.com/en-US/')
				.inject('js', 'inject-js/jquery.slim.min.js')
				.inject('js', 'inject-js/moment-with-locales.min.js')
				.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
				.wait('#address-selection-input')
				.type('#address-selection-input', '')
			  .insert('#address-selection-input', address)
			  .wait(500)
				.wait(function() {
					return (document.querySelector('div.iconLoadingContainer_') === null) 
				})
			  .evaluate(function () {
				  var elem = document.querySelector('#address-selection-input');
				  var event = document.createEvent('MouseEvent');
				  event.initEvent('mousedown', true, true);
				  elem.dispatchEvent(event);		  	
			  	$('#address-selection-input').focus();
			  })
        .wait(function() {
          return document.querySelector('.locationInputExpanded_ div.base_').children.length > 0 
        })
        .evaluate(function() {
          if ($(".locationInputExpanded_ div.base_ div button").length > 0) {
            $(".locationInputExpanded_ div.base_ div button:first").attr('id', 'my-first-a');
          }
          var elem = document.querySelector('#my-first-a');
          if (elem) {
            var event = document.createEvent('MouseEvent');
            event.initEvent('click', true, true);
            elem.dispatchEvent(event);
          }
        })
			 .wait(function() {
					return (document.querySelector('div.loadMore_') !== null) || (document.querySelector('div.centered_') !== null)  || (document.querySelector('div.headerTitle_') !== null)
			 })			 
			 .wait(750)
			 .evaluate((timeZone) => {
			 	var resultsToSave = [];
			 	$('a.base_').each(function(num) {
			 		var resultToSave = {};
			 		if ($(this).attr('href').indexOf('/food-delivery/') >= 0) {
			 			var mainDiv = $(this).children('div')[0];
			 			resultToSave.name = $($(mainDiv).children()[1]).text().trim();
			   		resultToSave.href = 'https://www.ubereats.com' + $(this).attr('href').trim();
			   		
			   		var detailsDiv = $(mainDiv).children('div')[2];
			   		
			   		var list = $($(detailsDiv).children('div')[0]).text().trim().split('•');
			   		resultToSave.cuisine = '';
			   		resultToSave.price = '';
			   		if (list.length >= 1) {
			   			if (list[0].indexOf('$') >= 0) {
			   				resultToSave.price = list.shift().trim().length;
			   			}
			   		  resultToSave.cuisine = list.map(Function.prototype.call, String.prototype.trim).join('|').trim();
			   		}
			   		
			   		var fee = '';
			   		var eta = '';
			   		var rating = '';
			   		var otherInfoDivs = $($($(detailsDiv).children('div')[1]).children('div')[0]).children('div');
			   		
			   		for (var i = 0; i < otherInfoDivs.length; i++) {
			   			var infoDiv = otherInfoDivs[i];
			   			var infoDivText = $(infoDiv).text().trim()
			   			
				   		if (infoDivText.toLowerCase().indexOf('opens') >= 0 || infoDivText.toLowerCase().indexOf('currently unavailable') >= 0 || infoDivText.toLowerCase().indexOf('closed') >= 0 || infoDivText.toLowerCase().indexOf('min') >= 0) {
				   			eta = infoDivText; 
				   		} else if (infoDivText.toLowerCase().indexOf('fee') >= 0 && infoDivText.indexOf('$') >= 0) {
				   			fee = infoDivText;
				   		} else if (infoDivText.indexOf('(') >= 0 && infoDivText.indexOf(')') >= 0) {
				   			rating = infoDivText;
				   		}
			   		}
			   		
			   		if (eta.length == 0 || eta.indexOf('Opens') === 0 || eta.indexOf('Currently unavailable') === 0 || eta.indexOf('Closed') === 0) {
			   			return false;
			   		}
			   		resultToSave.eta = eta; 
			   		resultToSave.etaMin = '';
			   		resultToSave.etaMax = '';
			   		if (eta.indexOf('–') >= 0) {
				   		resultToSave.etaMin = eta.split('–')[0].trim();
				   		resultToSave.etaMax = eta.split('–')[1].trim().split(' ')[0].trim();				   			
			   		}
			   		
			   		fee = fee.toLowerCase().replace('fee', '').trim();
			   		resultToSave.deliverySearchPageFee = fee;
			   		resultToSave.deliveryCheckoutPageFee = fee;
			   		
			   		resultToSave.dateTimeUTC0 = (new Date()).toISOString();
			   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
			   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
			   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');			   		
			   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
			   		resultsToSave.push(resultToSave);
			 	  }
		   		if (num >= 20) {
		   			return false;
		   		}
			 	});
			 	
			 	return resultsToSave;
			 }, jobDescription.TimeZone)
		   .catch(function(error) {
		  	 if (DEBUG_MODE) {
		  		 const screenshotFileName = loggerLib.screenshotFileName('address_search_error'); 
		  		 nightmare.screenshot(screenshotFileName);
		  		 loggerLib.log(error, null, [screenshotFileName]);
		  	 } else {
		  		 loggerLib.log(error); 
		  	 }
		  	 return [];
		   });
		  
		  
		  for (let k = 0; k < result.length; k++ ) {
		  	result[k].position = k + 1;
		  	result[k].address = address;
		  	result[k].searchQuery = '';
		  	result[k].market = jobDescription.CBSA;
		  }
		  
		  addressResult.push(...result);
		  loggerLib.log('SEARCH QUERY (EMPTY) END. Results: ' + result.length);
		  
		  if (result.length > 0) {
		  	
			  for (let sqi = 0; sqi < searchQueries.length; sqi++) {
			  	let searchQuery = searchQueries[sqi];
			  	
			  	loggerLib.log('SEARCH QUERY (' + searchQuery + ') START: ' + (sqi+1) + '/' + searchQueries.length);
			  	
			  	var result2 = yield nightmare
			  	.goto('https://www.ubereats.com/en-US/search/?q=' + searchQuery)
			  	.inject('js', 'inject-js/jquery.slim.min.js')
			  	.inject('js', 'inject-js/moment-with-locales.min.js')
					.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')			  	
		  		.wait(1000)
		  		.wait(function() {
		  			return (document.querySelector('a.base_') !== null) || (document.querySelector('div.noResultsImageWrapper_') !== null)
		  		})
			 	  .evaluate((timeZone) => {
					 	var resultsToSave = [];
					 	$('a.base_').each(function(num) {
					 		var resultToSave = {};
					 		if ($(this).attr('href').indexOf('/food-delivery/') >= 0) {
					 			var mainDiv = $(this).children('div')[0];
					 			resultToSave.name = $($(mainDiv).children()[1]).text().trim();
					   		resultToSave.href = 'https://www.ubereats.com' + $(this).attr('href').trim();
					   		
					   		var detailsDiv = $(mainDiv).children('div')[2];
					   		
					   		var list = $($(detailsDiv).children('div')[0]).text().trim().split('•');
					   		resultToSave.cuisine = '';
					   		resultToSave.price = '';
					   		if (list.length >= 1) {
					   			if (list[0].indexOf('$') >= 0) {
					   				resultToSave.price = list.shift().trim().length;
					   			}
					   		  resultToSave.cuisine = list.map(Function.prototype.call, String.prototype.trim).join('|').trim();
					   		}
					   		
					   		var fee = '';
					   		var eta = '';
					   		var rating = '';
					   		var otherInfoDivs = $($($(detailsDiv).children('div')[1]).children('div')[0]).children('div');
					   		
					   		for (var i = 0; i < otherInfoDivs.length; i++) {
					   			var infoDiv = otherInfoDivs[i];
					   			var infoDivText = $(infoDiv).text().trim()
					   			
						   		if (infoDivText.toLowerCase().indexOf('opens') >= 0 || infoDivText.toLowerCase().indexOf('currently unavailable') >= 0 || infoDivText.toLowerCase().indexOf('closed') >= 0 || infoDivText.toLowerCase().indexOf('min') >= 0) {
						   			eta = infoDivText; 
						   		} else if (infoDivText.toLowerCase().indexOf('fee') >= 0 && infoDivText.indexOf('$') >= 0) {
						   			fee = infoDivText;
						   		} else if (infoDivText.indexOf('(') >= 0 && infoDivText.indexOf(')') >= 0) {
						   			rating = infoDivText;
						   		}
					   		}
					   		
					   		if (eta.length == 0 || eta.indexOf('Opens') === 0 || eta.indexOf('Currently unavailable') === 0 || eta.indexOf('Closed') === 0) {
					   			return false;
					   		}
					   		resultToSave.eta = eta; 
					   		resultToSave.etaMin = '';
					   		resultToSave.etaMax = '';
					   		if (eta.indexOf('–') >= 0) {
						   		resultToSave.etaMin = eta.split('–')[0].trim();
						   		resultToSave.etaMax = eta.split('–')[1].trim().split(' ')[0].trim();				   			
					   		}
					   		
					   		fee = fee.toLowerCase().replace('fee', '').trim();
					   		resultToSave.deliverySearchPageFee = fee;
					   		resultToSave.deliveryCheckoutPageFee = fee;
					   		
					   		resultToSave.dateTimeUTC0 = (new Date()).toISOString();
					   		resultToSave.dateTimeLocal = moment(new Date()).tz(timeZone).format();
					   		resultToSave.dateLocal = moment(new Date()).tz(timeZone).format('YYYY-MM-DD');
					   		resultToSave.hourLocal = moment(new Date()).tz(timeZone).format('HH');			   		
					   		resultToSave.dayLocal = moment(new Date()).tz(timeZone).format('dddd');
					   		resultsToSave.push(resultToSave);
					 	  }
				   		if (num >= 20) {
				   			return false;
				   		}
					 	});
			 	
					 	return resultsToSave;
				  }, jobDescription.TimeZone)
				  .catch(function(error) {
				  	if (DEBUG_MODE) {
				  		const screenshotFileName = loggerLib.screenshotFileName('search_query_error');
					  	nightmare.screenshot(screenshotFileName);
					  	loggerLib.log(error, null, [screenshotFileName]);
				  	} else {
				  		loggerLib.log(error);
				  	}
				  	return [];				  	
				  });
			  	
		  	  for (var k = 0; k < result2.length; k++) {
		  	  	result2[k].address = address;
		  	  	result2[k].position = k + 1;
		  	  	result2[k].searchQuery = searchQuery;
		  	  	result2[k].market = jobDescription.CBSA;
	  	  	}

		  	  
		  		addressResult.push(...result2);
		  		loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + result2.length);
		  	}
		  }
		  
		  resultsGlobal.push(...addressResult);
			loggerLib.log('ADDRESS END. Results: ' + addressResult.length);
			if (nightmare) {
				yield nightmare.evaluate(() => {
			    window.localStorage.clear();
			  }).catch(function(error) {
			  	loggerLib.log(error);
			  });
			  yield nightmare.cookies.clearAll();
			}
		}
		
	}
	
	if (!DEBUG_MODE) {
		const threadTime = Math.round((process.hrtime(start)[0]/60));
		const cloudWatchLib = require("./lib/cloudwatch");
		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
		
	  let etaResultsCount = 0;
	  for (let resInd = 0; resInd < resultsGlobal.length; resInd++) {
	  	if (typeof resultsGlobal[resInd].etaMin != 'undefined' && resultsGlobal[resInd].etaMin != null && resultsGlobal[resInd].etaMin.length != 0) {
	  		etaResultsCount++;
	  	}
	  }
	  if (resultsGlobal.length > 0) {
	  	let fullRowsPercent = 100 - Math.round((resultsGlobal.length-etaResultsCount)/resultsGlobal.length * 100);
		  yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Full Row Count Percent', fullRowsPercent, WEBSITE_NAME + '-Threads', THREAD_ID);
		  loggerLib.log('Full Row Count Percent: ' + fullRowsPercent + '%');
	  }		
	}

	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
	if (resultsGlobal.length > 0) {
	  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.end();
    nightmare.halt();
  }
  
  process.exit();
});