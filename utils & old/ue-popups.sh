#!/bin/bash

  sleep 1

  for b in `seq 1 8`
  do
    xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-popup-api.js $b 8 >> ./ubereats-thread-popup-$b-api.log 2>&1 &
    sleep 3
  done