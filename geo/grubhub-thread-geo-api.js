// node --harmony grubhub-thread-geo-api.js 1 >> ./logs/grubhub-thread-geo-api.log 2>&1 &

//node --harmony grubhub-thread-geo-api.js 1 >> ./logs/grubhub-thread-geo-1-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 2 >> ./logs/grubhub-thread-geo-2-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 3 >> ./logs/grubhub-thread-geo-3-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 4 >> ./logs/grubhub-thread-geo-4-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 5 >> ./logs/grubhub-thread-geo-5-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 6 >> ./logs/grubhub-thread-geo-6-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 7 >> ./logs/grubhub-thread-geo-7-api.log 2>&1 &
//node --harmony grubhub-thread-geo-api.js 8 >> ./logs/grubhub-thread-geo-8-api.log 2>&1 &


const vo = require('vo');
const configLib = require("./lib/config");
const loggerLib = require("./lib/logger");
const resultsLib = require("./lib/results2");
const addressCoordsLib = require('./lib/address_coords');
const constants = require('./config/constants');
var request = require('request');
const geo = require('geolocation-utils');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');
const json2csv = require('json2csv');
const csv = require('csvtojson');
const fs = require('fs');


//TIMER Start
let start = process.hrtime();

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.GRUBHUB_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }  
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

let resultsGlobal = [];

function getRests (baseUrl, authToken, latLng, searchQuery) {
	return new Promise((res, rej) => {
    var options = {
        method: 'GET',
        url: baseUrl + '/restaurants/search/search_listing',
        qs: {
          'locationMode': 'DELIVERY',
          'pageSize': '100',
          'location': 'POINT(' + latLng.lng +' ' + latLng.lat + ')',
          'facet': 'group:GHD'
        },            
        'auth': {
          'bearer': authToken
        },
        headers: {
          'User-Agent': 'Itirra Data Collection Worker'
        }
    };
    if (searchQuery) {
    	options.qs.queryText = searchQuery;
    	options.qs.pageSize = 10;
    }    
    request(options, function (error, response, body) {
    	var restResponse;
      if (error) {          
      	loggerLib.log(error);
      	loggerLib.log(response);
      	loggerLib.log(body);
        rej('ERROR getting Results');
      }
      if (!response || typeof response == 'undefined' || response.statusCode != 200) {          
      	loggerLib.log(error);
      	loggerLib.log(response);
      	loggerLib.log(body);        
        rej('ERROR getting Results');          
      }       
      try {
      	restResponse = JSON.parse(body);
      } catch(error) {
      	loggerLib.log(error);
      	restResponse = [];
      }      
      res(restResponse);
    });
  }); 
}

function isRestInRestResponse(restResponse, restId) {
	var result = false;
  if (restResponse != null && typeof restResponse.results != 'undefined') {
  	var savedResults = 0;
  	for (var ri = 0; ri < restResponse.results.length; ri++) {
  		let restData = restResponse.results[ri];
  		if (restData.delivery) {
	  		if (restData.restaurant_id == restId) {
	  			result = true;
	  			break;
	  		}
  		}
  	}
  }
	return result;
}

function addressAlreadyProcessed(address, results) {
	var result = false;
	var addressResultCount = 0;
	for (var i = 0; i < results.length; i++) {
		if (results[i].address == address) {
			addressResultCount++;
		}
	}
	if (addressResultCount >= 10) {
		result = true;
	}
	return result;
}

function restAlreadyProcessed(address, restId, results) {
	var result = false;
	for (var i = 0; i < results.length; i++) {
		if (results[i].address == address && results[i].restaurant_id == restId) {
			result = true;
			break;
		}
	}
	return result;
}

function getDirectionText(directon) {
	switch (directon) {
	  case 0: return 'north'
	  case 45: return 'north_east'
	  case 90: return 'east'
	  case 135: return 'south_east'
	  case 180: return 'south'
	  case 225: return 'south_west'
	  case 270: return 'west'
	  case 315: return 'north_west'
	}
}

function sleep(ms){
  return new Promise(resolve=>{
      setTimeout(resolve,ms)
  })
}

const fName = './results/gh-geo-results-' + THREAD_ID + '.csv';

const fields = [
                'site', 'market', 'address', 'restaurant_id', 'name', 'fullAddress', 'lat', 'lng', 'api_radius',
                'north_distance', 'north_max_lat', 'north_max_lng',
                'north_east_distance', 'north_east_max_lat', 'north_east_max_lng',
                'east_distance', 'east_max_lat', 'east_max_lng',
                'south_east_distance', 'south_east_max_lat', 'south_east_max_lng',
                'south_distance', 'south_max_lat', 'south_max_lng',
                'south_west_distance', 'south_west_max_lat', 'south_west_max_lng',
                'west_distance', 'west_max_lat', 'west_max_lng',
                'north_west_distance', 'north_west_max_lat', 'north_west_max_lng',
                'dateTimeUTC0', 'dateTimeLocal', 'hourLocal', 'dayLocal'
            ];

// Main RUN Function
const run = function * () {
	
	if (fs.existsSync(fName)) {
		resultsGlobal = yield function(){
			return new Promise((res, rej) => {
				let result = [];
				csv({
					delimiter: ",",
				})
				.fromFile(fName)
				.on('json', (json) => {
					result.push(json);
				})
				.on('error', (error)=>{
					rej(error);
				})
				.on('end', () => {
					res(result);	
				});
			});
		}();
	}
	
	
  // PROD
  const clientId = 'itirra_aWhhdGVpdGlycmFwcmljZXB';
  const baseUrl = 'https://api-gtm.grubhub.com';  
  
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = [];
		const searchQueriesWithEmpty = [''];
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length);
		
		
//		if (jdi == 0 || jdi % 5) {
		loggerLib.log('-------- AUTH --------');
		
		 // Get Auth Token
	  const authResponse = yield function() {
	    return new Promise((res, rej) => {  
	      var options = {
	          method: 'POST',
	          url: baseUrl + '/oauth2/direct/auth',
	          json: {
	            'grant_type': 'token',
	            'client_id': clientId,
	            'scope': 'anonymous',
	          },
	          headers: {
	            'User-Agent': 'Itirra Data Collection Worker'
	          }
	      };
	      request(options, function (error, response, body) {
	        if (error) {          
	          rej('ERROR getting Auth Token: ' + error);
	        }
	        if (!response || response.statusCode != 200) {          
	          rej('ERROR getting Auth Token. Status Code: ' + response.statusCode);         
	        }       
	        res(body);
	      });
	    }); 
	  }().then(data => {
	    return data;
	  }).catch((error) => {
	  	loggerLib.log(error);
	    return null;
	  });		
	  
//		}
		
		
		let timezone = '';
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let addressResult = [];
			
			let address = addresses[ai].trim();
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			var addressProcessed = addressAlreadyProcessed(address, resultsGlobal);
			if (addressProcessed) {
				loggerLib.log('ADDRESS ALREADY PROCESSED');	
				continue;
			}
			
		  var latLng =  yield addressCoordsLib.getCoordinatesByAddress(address).then(data => {
		    return data;
		  }).catch((error) => {
		  	loggerLib.log(error);
		    return [];
		  });
		  
		  if (typeof latLng != 'undefined' && latLng) {
					
		  	for (var sqi = 0; sqi < searchQueriesWithEmpty.length; sqi++) {
				
		  		let searchQuery = searchQueriesWithEmpty[sqi].trim();
			  
		      if (authResponse) {
		        
		        let restResponse = yield getRests(baseUrl, authResponse.access_token, latLng);
		        
		        if (restResponse != null && typeof restResponse.results != 'undefined') {
		        	
		        	var savedResults = 0;
		        	for (var ri = 0; ri < restResponse.results.length; ri++) {
		        		let restData = restResponse.results[ri];
		        		
		        		if (restData.delivery) {
					        let result = {};
					        result.site = WEBSITE_NAME;
					        result.market = jobDescription.CBSA;				        
					  	  	result.address = address;
					  	  	result.restaurant_id = restData.restaurant_id;
					  	  	result.name = restData.name;
					  	  	result.restAddress = restData.address;
									addressResult.push(result);
		        		}
		        		
								if (ri >= 9) {
		        			break;
		        		}
		        	}
		        	
		        	loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + addressResult.length);
		        	
		        	if (restResponse.results.length == 0 && searchQuery.length == 0) {
		        		break;
		        	}		        	
		        }
		        
		      }
				}
		  } else {
		  	loggerLib.log('LAT LNG IS UNDEFINED !!!|' + address);
		  }
		  
		  
		  // --------------------------------- V2 ---------------------------------------------------
		  
		  var directions = [0, 45, 90, 135, 180, 225, 270, 315];
		  for (var restIndex = 0; restIndex < addressResult.length; restIndex++) {
		  	var rest = addressResult[restIndex];
		  	loggerLib.log('Checking ' + rest.name + ' ' + (restIndex+1) + '/' + addressResult.length);
		  	
		  	if (restAlreadyProcessed(address, rest.restaurant_id, resultsGlobal)) {
		  		loggerLib.log('REST Already Processed');	
		  		continue;
		  	}
		  	var errorOccured = false;
		  	for (var dirI = 0; dirI < directions.length; dirI++) {
		  		var direction = directions[dirI];
		  		
		  		if (errorOccured) {
		  			break;
		  		}
		  		
		  		var distanceMeters = 6000;
		  		
		  		var lastFound = null;
		  		var lastFoundLatLng = {};
		  		var lastNotFound = null;
		  		
		  		var maxSteps = 20;
		  		var step = 1;
		  		var distanceFound = false;
		  		while (!distanceFound) {
		  			var oldDistanceMeters = distanceMeters;
		  			var directionText = getDirectionText(direction);
		  			loggerLib.log('Step[' + step + '] Distance: ' + distanceMeters + ' Direction: ' + directionText);
		  			var coordsToCheck = geo.moveTo({lat: parseFloat(rest.restAddress.latitude), lon: parseFloat(rest.restAddress.longitude)}, {heading: direction, distance: distanceMeters});
			  		let restCheckResponse = yield getRests(baseUrl, authResponse.access_token, {lat: coordsToCheck.lat, lng: coordsToCheck.lon}, rest.name);
			  		var restFound = isRestInRestResponse(restCheckResponse, rest.restaurant_id);
		  			if (restFound) {
		  				loggerLib.log('Step[' + step + '] Rest found');
		  				lastFoundLatLng = coordsToCheck;
		  				lastFound = distanceMeters;
		  				if (lastNotFound) {
		  					distanceMeters = Math.round((distanceMeters + lastNotFound) / 2);
		  				} else {
		  					distanceMeters = distanceMeters + Math.round(distanceMeters / 2);
		  				}
		  				if (distanceMeters == oldDistanceMeters) {
		  					distanceMeters = distanceMeters + 1;
		  				}
		  			} else {
		  				loggerLib.log('Step[' + step + '] Rest NOT found');
		  				lastNotFound = distanceMeters;
		  				if (lastFound) {
		  					distanceMeters = Math.round((distanceMeters + lastFound) / 2);
		  				} else {
			  				distanceMeters = distanceMeters - Math.round(distanceMeters / 2);		  					
		  				}
		  				if (distanceMeters == oldDistanceMeters) {
		  					distanceMeters = distanceMeters - 1;
		  				}
		  			}
		  			
		  			if (lastFound && lastNotFound) {
		  				var accuracy = Math.abs(lastFound - lastNotFound);
		  				loggerLib.log('Step[' + step + '] Accuracy: ' + accuracy);
		  				if (accuracy <= 100) {
		  					var dirText = getDirectionText(direction); 
				  			rest[dirText + '_distance'] = lastFound;
				  			rest[dirText + '_max_lat'] = lastFoundLatLng.lat;
				  			rest[dirText + '_max_lng'] = lastFoundLatLng.lon;
		  					distanceFound = true;
		  					loggerLib.log('Step[' + step + '] FINISHED !!! Direction: ' + directionText + ' Distance: ' + lastFound);
		  				}
		  			}
		  			step++;
		  			
		  			if (step >= maxSteps) {
		  				errorOccured = true;
		  			  distanceFound = true;
		  				break;
		  			}
		  			
		  			yield sleep(250);
		  		}
		  	}
		  	
		  	if (!errorOccured) {
		  		rest.dateTimeUTC0 = (new Date()).toISOString();
	  			rest.dateTimeLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format();
	  			rest.dateLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('YYYY-MM-DD');
	  			rest.hourLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('HH');
	  			rest.dayLocal =  momentTz(new Date()).tz(jobDescription.TimeZone).format('dddd');
	  			rest.fullAddress = rest.restAddress.street_address + ', ' + rest.restAddress.address_locality + ' ' + rest.restAddress.address_region + ' ' + rest.restAddress.postal_code;
	  			rest.lat = rest.restAddress.latitude;
	  			rest.lng = rest.restAddress.longitude;
	  			resultsGlobal.push(rest);		  	
			  	
	  			const threadTime = Math.round((process.hrtime(start)[0]/60));
			  	loggerLib.log('--- Results Saved: ' + resultsGlobal.length + ' --- Execution Time (minutes): ' + threadTime);
					if (fs.existsSync(fName)) {
						fs.unlinkSync(fName);
					}
					fs.writeFileSync(fName, json2csv({data: resultsGlobal, fields: fields}), 'ascii');
		  	}
		  	
		  }
		  
		  // --------------------------------- V2 ---------------------------------------------------		  
		  
		  
			loggerLib.log('ADDRESS END.');
		}
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));
		loggerLib.log('JOB END|' + (jdi+1) + '|' + jobTime + '|' + usedMemory);
	}
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  process.exit();
});