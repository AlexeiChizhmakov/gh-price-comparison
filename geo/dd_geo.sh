#!/bin/bash

sleep 1

for b in `seq 1 5`
do
  xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js $b debug >> ./logs/doordash-thread-geo-$b-api-proxy.log 2>&1 &
  sleep 1
done