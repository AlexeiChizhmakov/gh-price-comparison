// node --harmony geo-combine.js

const vo = require('vo');
const configLib = require("./lib/config");
const loggerLib = require("./lib/logger");
const resultsLib = require("./lib/results2");
const addressCoordsLib = require('./lib/address_coords');
const constants = require('./config/constants');
var request = require('request');
const geo = require('geolocation-utils');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');
const json2csv = require('json2csv');
const csv = require('csvtojson');
const fs = require('fs');
const turf = require('@turf/turf');

//TIMER Start
let start = process.hrtime();

loggerLib.threadId = 1;

loggerLib.log('--- START');

let resultsGlobal = [];

function getCSVFileContents (filePath) {
  return new Promise((res, rej) => {
    let result = [];
    csv({
      delimiter: ",",
    })
    .fromFile(filePath)
    .on('json', (json) => {
      result.push(json);
    })
    .on('error', (error)=>{
      rej(error);
    })
    .on('end', () => {
      res(result);  
    });
  });
}

function getWebsiteDataForAddr (allWebsiteData, addr) {
  var result = [];
  for (var i = 0; i < allWebsiteData.length; i++) {
    if (allWebsiteData[i].address == addr) {
      result.push(allWebsiteData[i]);
    }
  }
  return result;
}


let directions = ['north',
                  	'north_east',
                  	'east',
                  	'south_east',
                  	'south',
                  	'south_west',
                  	'west',
                  	'north_west'].reverse();

function restValid (singleRestData) {
	var valid = true;
	directions.forEach(function(direction) {
		direction = direction + '_distance';
		if (typeof !singleRestData[direction] == 'undefined' || !singleRestData[direction] || singleRestData[direction].length == 0) {
			valid = false;
			return;
		}
	});
	return valid;
}

function getPolygonCoords (singleRestData) {
	var coords = [];
	directions.forEach(function(direction) {
		coords.push([parseFloat(singleRestData[direction + '_max_lng']), parseFloat(singleRestData[direction + '_max_lat'])]);
	});
	coords.push([parseFloat(singleRestData['north_west_max_lng']), parseFloat(singleRestData['north_west_max_lat'])]);
	return coords;
}

function getAVGAreaKm2(data) {
	var areaArray = [];
  for (var addrDataI = 0; addrDataI < data.length; addrDataI++) {
	  loggerLib.log('Starting Rest: ' + (addrDataI+1) + '/' + data.length);
  	var singleRestData = data[addrDataI];
  	if (restValid(singleRestData)) {
  		var polygonCoords = getPolygonCoords(singleRestData);
  		var polygon = turf.polygon([polygonCoords]);
  		var area = turf.area(polygon);
  		loggerLib.log('AREA: ' + area);
  		areaArray.push(area);
  	} else {
  		 loggerLib.log('Invalid Rest');
  	}
  }
  var sum = 0;
  for(var areaI = 0; areaI < areaArray.length; areaI++){
      sum += areaArray[areaI];
  }
  var avg = sum / areaArray.length;
  loggerLib.log('AVG AREA (m2): ' + avg);
  avg = avg / 1000000;
  avg = +avg.toFixed(2);
  loggerLib.log('AVG AREA (km2): ' + avg);
  return avg; 
}

function getAVGDeliveryRange(data) {
	var array = [];
  for (var addrDataI = 0; addrDataI < data.length; addrDataI++) {
  	var singleRestData = data[addrDataI];
  	if (restValid(singleRestData)) {
	  	directions.forEach(function(direction) {
	  		array.push(parseFloat(singleRestData[direction + '_distance']));
	  	});
  	}
  }
  var sum = 0;
  for(var areaI = 0; areaI < array.length; areaI++){
      sum += array[areaI];
  }
  var avg = sum / array.length;
  avg = +avg.toFixed(2);
  loggerLib.log('AVG Distance (m): ' + avg);
  return avg; 
}

function getStdDivDeliveryRange(data) {
	var array = [];
  for (var addrDataI = 0; addrDataI < data.length; addrDataI++) {
  	var singleRestData = data[addrDataI];
  	if (restValid(singleRestData)) {
	  	directions.forEach(function(direction) {
	  		array.push(parseFloat(singleRestData[direction + '_distance']));
	  	});
  	}
  }
  return +standardDeviation(array).toFixed(2); 
}

function standardDeviation(values){
  var avg = average(values);
  
  var squareDiffs = values.map(function(value){
    var diff = value - avg;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });
  
  var avgSquareDiff = average(squareDiffs);

  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}


// Main RUN Function
const run = function * () {
  
  // Get GH Data
  var ghData = [];
  for (var ghThreadId = 1; ghThreadId <= 15; ghThreadId++) {
    var ghDataTmp = yield getCSVFileContents('./results/gh-geo-results-' + ghThreadId + '.csv');
    ghData.push(...ghDataTmp);
  }
  
  // Get DD Data
  var ddData = [];
  for (var ddThreadId = 1; ddThreadId <= 5; ddThreadId++) {
    var ddDataTmp = yield getCSVFileContents('./results/dd-geo-results-' + ddThreadId + '.csv');
    ddData.push(...ddDataTmp);
  }
  
  // Get UE Data
  var ueData = [];
  for (var ueThreadId = 1; ueThreadId <= 20; ueThreadId++) {
    var ueDataTmp = yield getCSVFileContents('./results/ue-geo-results-' + ueThreadId + '.csv');
    ueData.push(...ueDataTmp);
  }
  
    
  var config = yield getCSVFileContents('./config/config.csv');

  for (jdi = 0; jdi < config.length; jdi++) {
    
    var jobDescription = config[jdi];
    
    loggerLib.log('Starting Market: ' + jobDescription.CBSA);
    
    let addresses = configLib.getAddresses(jobDescription);
    
    for (ai = 0; ai < addresses.length; ai++) {

      let address = addresses[ai];
      
      loggerLib.log('Starting Address: ' + address);
      
      var resultData = {};
      resultData.CBSA = jobDescription.CBSA;
      resultData.address = address;
      
      var ghAddressData = getWebsiteDataForAddr(ghData, address);
      if (ghAddressData.length > 0) {
      	resultData.gh_avg_area_km2 = getAVGAreaKm2(ghAddressData);
      	resultData.gh_avg_delivery_range_m = getAVGDeliveryRange(ghAddressData);
      	resultData.gh_std_div_delivery_range_m = getStdDivDeliveryRange(ghAddressData);
      	resultData.gh_circular_delivery_range_percent = +(100 - (resultData.gh_std_div_delivery_range_m / resultData.gh_avg_delivery_range_m) * 100).toFixed(2);
      	if (resultData.gh_avg_area_km2 > 5000) {
      		resultData.gh_avg_area_km2 = '';
      		resultData.gh_avg_delivery_range_m = '';
      		resultData.gh_std_div_delivery_range_m = '';
      		resultData.gh_circular_delivery_range_percent = '';
      	}      	
      }
      
      var ddAddressData = getWebsiteDataForAddr(ddData, address);
      if (ddAddressData.length > 0) {
      	resultData.dd_avg_area_km2 = getAVGAreaKm2(ddAddressData);
      	resultData.dd_avg_delivery_range_m = getAVGDeliveryRange(ddAddressData);
      	resultData.dd_std_div_delivery_range_m = getStdDivDeliveryRange(ddAddressData);
      	resultData.dd_circular_delivery_range_percent = +(100 - (resultData.dd_std_div_delivery_range_m / resultData.dd_avg_delivery_range_m) * 100).toFixed(2);
      	if (resultData.dd_avg_area_km2 > 5000) {
      		resultData.dd_avg_area_km2 = '';
      		resultData.dd_avg_delivery_range_m = '';
      		resultData.dd_std_div_delivery_range_m = '';
      		resultData.dd_circular_delivery_range_percent = '';
      	}
      }  
      
      var ueAddressData = getWebsiteDataForAddr(ueData, address);
      if (ueAddressData.length > 0) {
      	resultData.ue_avg_area_km2 = getAVGAreaKm2(ueAddressData);
      	resultData.ue_avg_delivery_range_m = getAVGDeliveryRange(ueAddressData);
      	resultData.ue_std_div_delivery_range_m = getStdDivDeliveryRange(ueAddressData);
      	resultData.ue_circular_delivery_range_percent = +(100 - (resultData.ue_std_div_delivery_range_m / resultData.ue_avg_delivery_range_m) * 100).toFixed(2);
      	if (resultData.ue_avg_area_km2 > 5000) {
      		resultData.ue_avg_area_km2 = '';
      		resultData.ue_avg_delivery_range_m = '';
      		resultData.ue_std_div_delivery_range_m = '';
      		resultData.ue_circular_delivery_range_percent = '';
      	}
      }        
      
      var winner = '';
      if (resultData.gh_avg_area_km2 > resultData.dd_avg_area_km2 && resultData.gh_avg_area_km2 > resultData.ue_avg_area_km2) {
      	winner = 'GH';
      }
      if (resultData.dd_avg_area_km2 > resultData.gh_avg_area_km2 && resultData.dd_avg_area_km2 > resultData.ue_avg_area_km2) {
      	winner = 'DD';
      }   
      if (resultData.ue_avg_area_km2 > resultData.gh_avg_area_km2 && resultData.ue_avg_area_km2 > resultData.dd_avg_area_km2) {
      	winner = 'UE';
      }
      
      if (!winner) {
      	if (!resultData.dd_avg_area_km2) {
          if (resultData.gh_avg_area_km2 > resultData.ue_avg_area_km2) {
          	winner = 'GH';
          } else {
          	winner = 'UE';
          }
      	}
      	if (!resultData.ue_avg_area_km2) {
          if (resultData.gh_avg_area_km2 > resultData.dd_avg_area_km2) {
          	winner = 'GH';
          } else {
          	winner = 'DD';
          }
      	}
      	if (!resultData.gh_avg_area_km2) {
          if (resultData.dd_avg_area_km2 > resultData.ue_avg_area_km2) {
          	winner = 'DD';
          } else {
          	winner = 'UE';
          }
      	}      	
      }
      
      resultData.winner = winner;
      
      if (resultData.dd_avg_area_km2 && resultData.gh_avg_area_km2) {
      	resultData.dd_gh_avg_area_diff_km2 = +(resultData.dd_avg_area_km2 - resultData.gh_avg_area_km2).toFixed(2);
      }
      if (resultData.ue_avg_area_km2 && resultData.gh_avg_area_km2) {
      	resultData.ue_gh_avg_area_diff_km2 = +(resultData.ue_avg_area_km2 - resultData.gh_avg_area_km2).toFixed(2);
      }
      
      resultsGlobal.push(resultData);
    }
  }
  
  
  const fields = [
                  'CBSA', 'address', 'gh_avg_area_km2', 'dd_avg_area_km2', 'ue_avg_area_km2', 'winner', 'dd_gh_avg_area_diff_km2', 'ue_gh_avg_area_diff_km2',
                  'gh_avg_delivery_range_m', 'gh_std_div_delivery_range_m', 'gh_circular_delivery_range_percent',
                  'dd_avg_delivery_range_m', 'dd_std_div_delivery_range_m', 'dd_circular_delivery_range_percent',
                  'ue_avg_delivery_range_m', 'ue_std_div_delivery_range_m', 'ue_circular_delivery_range_percent',
              ];
  
  const fName = './distance-results.csv';
	loggerLib.log('--- Results Saved: ' + resultsGlobal.length + ' ---');
	if (fs.existsSync(fName)) {
		fs.unlinkSync(fName);
	}
	fs.writeFileSync(fName, json2csv({data: resultsGlobal, fields: fields}), 'ascii');
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  process.exit();
});