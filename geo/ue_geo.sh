#!/bin/bash

sleep 1

for b in `seq 1 20`
do
  xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-geo-api.js $b debug >> ./logs/ubeareast-thread-geo-$b-api-proxy.log 2>&1 &
  sleep 1
done