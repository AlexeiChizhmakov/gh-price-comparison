// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-geo-api.js 1 debug

const configLib = require("./lib/config");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const proxyLib = require('./lib/proxy');
const addressCoordsLib = require('./lib/address_coords');
const slugid = require('slugid');
const util = require('util');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');
const geo = require('geolocation-utils');
const json2csv = require('json2csv');
const csv = require('csvtojson');
const fs = require('fs');

process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

// Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.UBEREATS_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }
});
loggerLib.threadId = THREAD_ID;

function getDirectionText(directon) {
	switch (directon) {
	  case 0: return 'north'
	  case 45: return 'north_east'
	  case 90: return 'east'
	  case 135: return 'south_east'
	  case 180: return 'south'
	  case 225: return 'south_west'
	  case 270: return 'west'
	  case 315: return 'north_west'
	}
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function isRestInRestResponse(searchRequestResult, restId) {
	var result = false;
  if (searchRequestResult != null && typeof searchRequestResult.results != 'undefined') {
  	for (var ri = 0; ri < searchRequestResult.results.length; ri++) {
  		let restData = searchRequestResult.results[ri];
  		if (restData.type == 'store') {
	  		if (restData.store.uuid == restId) {
	  			result = true;
	  			break;
	  		}
  		}
  	}
  }
	return result;
}

const fields = [
                'site', 'market', 'address', 'restaurant_id', 'name', 'fullAddress', 'lat', 'lng', 'api_radius',
                'north_distance', 'north_max_lat', 'north_max_lng',
                'north_east_distance', 'north_east_max_lat', 'north_east_max_lng',
                'east_distance', 'east_max_lat', 'east_max_lng',
                'south_east_distance', 'south_east_max_lat', 'south_east_max_lng',
                'south_distance', 'south_max_lat', 'south_max_lng',
                'south_west_distance', 'south_west_max_lat', 'south_west_max_lng',
                'west_distance', 'west_max_lat', 'west_max_lng',
                'north_west_distance', 'north_west_max_lat', 'north_west_max_lng',
                'dateTimeUTC0', 'dateTimeLocal', 'hourLocal', 'dayLocal'
            ];

loggerLib.log('--- START');

function addressAlreadyProcessed(address, results) {
	var result = false;
	var addressResultCount = 0;
	for (var i = 0; i < results.length; i++) {
		if (results[i].address == address) {
			addressResultCount++;
		}
	}
	if (addressResultCount >= 10) {
		result = true;
	}
	return result;
}

function restAlreadyProcessed(address, restId, results) {
	var result = false;
	for (var i = 0; i < results.length; i++) {
		if (results[i].address == address && results[i].restaurant_id == restId) {
			result = true;
			break;
		}
	}
	return result;
}

var nightmare;

// Main RUN Function
const run = function * () {
	
	let resultsGlobal = [];
	
	let fName = './results/ue-geo-results-' + THREAD_ID + '.csv';
	
	if (fs.existsSync(fName)) {
		resultsGlobal = yield function(){
			return new Promise((res, rej) => {
				let result = [];
				csv({
					delimiter: ",",
				})
				.fromFile(fName)
				.on('json', (json) => {
					result.push(json);
				})
				.on('error', (error)=>{
					rej(error);
				})
				.on('end', () => {
					res(result);	
				});
			});
		}();
	}
	
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	let jobDescriptionsNew = [];
	
	loggerLib.log('THREAD ' + THREAD_ID);
	
  if (jobDescriptions.length == 0) {
    return;
  } 
  
  nightmare = new Nightmare({
    waitTimeout: 10000,
    executionTimeout: 200000,
    show: false,
	  switches: {
			'ignore-certificate-errors': true
	  },		    
    webPreferences: {
    	partition: "ue-partition-" + Math.random(),
      webaudio: false,
      images: false,
      plugins: false,
      webgl: false,   
      webSecurity: false
    }
  });
  
  nightmare.on('console', (log, msg) => {
  	console.log(msg);
//	 	if (msg.indexOf('CONSOLE:') >= 0) {
//	 		loggerLib.log(msg);
//	 	}
  });
  nightmare.on('close', (data) => {
  	loggerLib.log('Child process closing: ' + data);
  });
  nightmare.on('exit', (data) => {
  	loggerLib.log('Child process exiting: ' + data);
  });
  nightmare.on('error', (data) => {
  	loggerLib.log('Child process error: ' + data);
  });
  nightmare.on('disconnect', (data) => {
  	loggerLib.log('Child process disconnected: ' + data);
  });  
  
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
    parent.respondTo('proxy', function(host, done) {
        win.webContents.session.setProxy({
            proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
        }, function() {
            done();
        });
    });
    done();
	},
	function(host, done) {
	    this.child.call('proxy', host, done);
	
	});
  
  const curDateStr = (new Date()).toISOString().replace('T', ' ').split('.')[0].split(' ')[0];
  
	let homePageReload = true;
  
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);		
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length);
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			var addressProcessed = addressAlreadyProcessed(address, resultsGlobal);
			if (addressProcessed) {
				loggerLib.log('ADDRESS ALREADY PROCESSED');	
				continue;
			}
			
			if (homePageReload) {
			  let userAgent = configLib.getRandomUserAgentString();
			  yield nightmare.cookies.clearAll();
			  var homePageLoad = yield nightmare.useragent(userAgent).viewport(720, 1280)
								  .filter({
								    urls: [
								         	  'https://d3i4yxtzktqr9n.cloudfront.net/*',
								         	  'https://www.ubereats.com/static/*'
								          ],
								   }, function(details, cb) {
								     return cb({ cancel: true });
								   })     		  
								  .goto('https://www.ubereats.com/en-US/')
									.inject('js', 'inject-js/jquery.min.js')
									.inject('js', 'inject-js/moment-with-locales.min.js')
									.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
									.wait('#address-selection-input')
								  .then(() => {
								  	return true;
							  	})
							  	.catch(function(error) {
								    if (DEBUG_MODE) {
								      const screenshotFileName = loggerLib.screenshotFileName('home_page_load_error');
					  		  	  nightmare.screenshot(screenshotFileName);
					  		  	  loggerLib.log(error, null, [screenshotFileName]);
								    } else {
								      loggerLib.log(error);
								    }
								    return false;
							  	});
			  
				loggerLib.log('Home Page Load: ' + homePageLoad);
				if (!homePageLoad) {
					continue;
				}
				homePageReload = false;
			}
			
		  var latLng =  yield addressCoordsLib.getCoordinatesByAddress(address).then(data => {
		    return data;
		  }).catch((error) => {
		  	loggerLib.log(error);
		    return [];
		  });			
			
		  var result = yield nightmare
			  .evaluate((addressTimeZoneSearchQueries, done) => {
			  	
			  	var result = {};
				  result.rests = [];
				  result.errors = [];
			  	
			  	if (typeof window == 'undefined' || window == null) {
			  		console.log('CONSOLE: ERROR: window is not defined');
			  		done(null, result);	
			  	}
			  	
			    window.address = addressTimeZoneSearchQueries.address;
		  	  console.log('CONSOLE: ' + window.address + '| Evaluate Start');
				 
			 		var params = {};
					params.targetLocation = {};
					params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
					params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
					params.targetLocation.reference = '';
					params.targetLocation.type = "google_places";
					params.targetLocation.address = {};
					params.targetLocation.address.title = '';
					params.targetLocation.address.address1 = '';
					params.targetLocation.address.city = '';
					params.bafEducationCount = 20;
					params.feed = "combo";
					params.feedTypes = ['STORE', 'SEE_ALL_STORES'];
					params.feedVersion = 2;
			 		
					console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Start');
		 			var bootstrapEaterRequest = $.ajax({
						url: "https://www.ubereats.com/rtapi/eats/v1/bootstrap-eater",
						method: "POST",
						dataType: "json",
						data: JSON.stringify(params),
						cache: false,
						crossDomain: true,
						contentType: "application/json",
						beforeSend: function(xhr) {
							xhr.setRequestHeader('Accept', '*/*');
							xhr.setRequestHeader('Cache-Control', 'max-age=0');
							xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
							xhr.setRequestHeader('x-csrf-token', window.csrfToken);
						},
						xhrFields: {
							withCredentials: true
						},
						timeout: 8000
					});
		 			
		 			bootstrapEaterRequest.done(function(bootstrapEaterRequestResponse) {
		 				
		 				console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Done');
		 				
		 				if (!bootstrapEaterRequestResponse.marketplace.isInServiceArea) {
							var resultToSave = {};
							resultToSave.request = 'bootstrapEaterRequest';
							resultToSave.msg = 'No Service in area';
							result.errors.push(resultToSave);
							done(null, result);
							return;
		 				}
		 				
		 			
		 				try {
			 				let index = 1;
			 				for (let fii = 0; fii < bootstrapEaterRequestResponse.marketplace.feed.feedItems.length; fii++) {
			 				  let feedItem = bootstrapEaterRequestResponse.marketplace.feed.feedItems[fii];
			 					if (feedItem.type == 'STORE') {
			 					  let feedItemRest = feedItem.payload.storePayload;
			 					  var feedItemRestData = JSON.parse(feedItemRest.trackingCode);
		 					  	var resultToSave = {};
			 					  resultToSave.name = feedItemRest.stateMapDisplayInfo.available.title.text;
			 					  resultToSave.uuid = feedItem.uuid;
			 					  resultToSave.href = '';
			 				  	if (typeof feedItemRest.stateMapDisplayInfo.available.tagline.text != 'undefined') {
						  	  	let priceCuisine = feedItemRest.stateMapDisplayInfo.available.tagline.text.trim().split('•');
						  	  	for (let pci = 0; pci < priceCuisine.length; pci++) {
						  	  		priceCuisine[pci] = priceCuisine[pci].trim(); 
						  	  	}
						  	  	let price = priceCuisine.shift();
						  	  	if (price.indexOf('$') >= 0) {
						  	  		resultToSave.price = price.length;	
						  	  	}
						  	  	resultToSave.cuisine = priceCuisine.join('|');
			 				  	}
					  	  	result.rests.push(resultToSave);
				 					if (index >= 10) {
				 						break;
				 					}
				 					index++;
			 					}
			 				}
		 				} catch(error) {
		 					console.log('CONSOLE: ERROR: ' + error);
		 				}
		 				
		 				if (result.rests.length == 0) {
							var resultToSave = {};
							resultToSave.request = 'bootstrapEaterRequest';
							resultToSave.msg = 'No restaurants found';
							result.errors.push(resultToSave);				 					
		 					done(null, result);
		 					return;
		 				}
		 				
		 				done(null, result);
			 		}); // bootstrapEaterRequest
		 			
		 			
		 			bootstrapEaterRequest.fail(function(msg, textStatus, errorThrown) {
						var resultToSave = {};
						resultToSave.request = 'bootstrapEaterRequest';
						resultToSave.msg = msg;
						resultToSave.textStatus = textStatus;
						resultToSave.errorThrown = errorThrown;
						result.errors.push(resultToSave);
						done(null, result);	
						return;
					});	
				 			
			 }, {address: address, timeZone: jobDescription.TimeZone, latLng: latLng})
		   .catch(function(error) {		  	 
		  	 if (DEBUG_MODE) {
		  		 const screenshotFileName = loggerLib.screenshotFileName('address_search_error'); 
		  		 nightmare.screenshot(screenshotFileName);
		  		 loggerLib.log(error, null, [screenshotFileName]);
		  	 } else {
		  		 loggerLib.log(error); 
		  	 }
		  	 homePageReload = true;
		  	 return {};
		   });
		  
		  
	  	if (typeof result.errors != 'undefined' && result.errors.length > 0) {
	  		for (let eri = 0; eri < result.errors.length; eri++) {
	  			loggerLib.log(result.errors[eri]);	
	  		}
	  		delete result.errors;
	  	}
	  	
			if (typeof result.rests != 'undefined' && result.rests.length > 0) {

				var restsToCheck = [];
				for (var ri = 0; ri < result.rests.length; ri++) {
					result.rests[ri].site = WEBSITE_NAME;
					result.rests[ri].address = address;
					result.rests[ri].market = jobDescription.CBSA;
					
			  	if (restAlreadyProcessed(address, result.rests[ri].uuid, resultsGlobal)) {
			  		loggerLib.log('REST Already Processed');	
			  		continue;
			  	}
					
					
		  	  let nameSlug = result.rests[ri].name.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
					let uuidSlug = slugid.encode(result.rests[ri].uuid);
					result.rests[ri].href = 'https://www.ubereats.com/en-US/' +  result.rests[ri].city + '/food-delivery/' + nameSlug + '/' + uuidSlug
					delete result.rests[ri].city;
					let restId = result.rests[ri].uuid;
					
					loggerLib.log('Getting lat/lang for: ' + result.rests[ri].name);
					result.rests[ri] = yield nightmare
				  .evaluate((params, done) => {
				  	var result = params.rest;
				  	
				  	let storeDetails = $.ajax({
    	        type: "GET",
    	        url: 'https://www.ubereats.com/rtapi/eats/v2/eater-store/' + params.rest.uuid,
						  cache: false,
						  crossDomain: true,
						  beforeSend: function(xhr){
								xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
						  },
    	        async: false
				  	}).responseText;
				  	
				  	storeDetails = JSON.parse(storeDetails);
				  	if (storeDetails.store) {
				  		result.restAddress = storeDetails.store.location.address.address1;
				  		result.lat = storeDetails.store.location.latitude;
				  		result.lng = storeDetails.store.location.longitude;
				  	}
				  	done(null, result);	
						return;
					}, {rest: result.rests[ri], latLng: latLng})
				  .catch(function(error) {		  	 
			  	  loggerLib.log(error); 
				  	return {};
				  });
					
					if (result.rests[ri].restAddress) {
						result.rests[ri].restaurant_id = restId;
						restsToCheck.push(result.rests[ri]);
					}
				}
				
				
				var directions = [0, 45, 90, 135, 180, 225, 270, 315];
				for (var ri = 0; ri < restsToCheck.length; ri++) {
					var rest = restsToCheck[ri];
					loggerLib.log('Checking ' + rest.name + ' ' + (ri+1) + '/' + restsToCheck.length);
					var errorOccured = false;
			  	for (var dirI = 0; dirI < directions.length; dirI++) {
			  		var direction = directions[dirI];
			  		
			  		if (errorOccured) {
			  			break;
			  		}
			  		
			  		var distanceMeters = 6000;
			  		
			  		var lastFound = null;
			  		var lastFoundLatLng = {};
			  		var lastNotFound = null;
			  		
			  		var step = 1;
			  		var maxSteps = 50;
			  		var distanceFound = false;
			  		while (!distanceFound) {
			  			var oldDistanceMeters = distanceMeters;
			  			var directionText = getDirectionText(direction);
			  			loggerLib.log('Step[' + step + '] Distance: ' + distanceMeters + ' Direction: ' + directionText);
			  			
			  			var coordsToCheck = geo.moveTo({lat: rest.lat, lon: rest.lng}, {heading: direction, distance: distanceMeters});
			  		
				  		var searchRequestResult = yield nightmare
							 										.evaluate((params, done) => {
							 									  	if (typeof window == 'undefined' || window == null) {
							 									  		console.log('CONSOLE: ERROR: window is not defined');
							 									  		done(null, []);	
							 									  	}
							 									  	
							 									  	var postParams = {};
							 									  	postParams.targetLocation = {};
							 									  	postParams.targetLocation.latitude = parseFloat(params.latLng.lat);
							 									  	postParams.targetLocation.longitude = parseFloat(params.latLng.lng);
							 									  	postParams.targetLocation.reference = '';
							 									  	postParams.targetLocation.type = "google_places";
							 									  	postParams.targetLocation.address = {};
							 									  	postParams.targetLocation.address.title = '';
							 									  	postParams.targetLocation.address.address1 = '';
							 									  	postParams.targetLocation.address.city = '';
							 									  	postParams.userQuery = params.rest.name;
							 									  	postParams.sortAndFilters = [];
							 									  	
							 									  	var searchRequestResult = $.ajax({
																				url: "https://www.ubereats.com/rtapi/eats/v1/search",
																				method: "POST",
																				dataType: "json",
																				data: JSON.stringify(postParams),
																				cache: false,
																				crossDomain: true,
																				contentType: "application/json",
																				beforeSend: function(xhr) {
																					xhr.setRequestHeader('Accept', '*/*');
																					xhr.setRequestHeader('Cache-Control', 'max-age=0');
																					xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
																					xhr.setRequestHeader('x-csrf-token', window.csrfToken);
																				},
																				xhrFields: {
																					withCredentials: true
																				},
																				async: false,
								 					    	        timeout: 30000
							 									  	}).responseText;							 									  	
							 									  	
													    			done(null, JSON.parse(searchRequestResult));	
							 										}, {rest: rest, latLng: {lat: coordsToCheck.lat, lng: coordsToCheck.lon}})
							 									  .catch(function(error) {
						 									      loggerLib.log(error);
							 								  		return null;
							 								  	});
				  		
				  		if (searchRequestResult === null) {
				  			distanceFound = true;
				  			break;
				  		}

				  		var restFound = isRestInRestResponse(searchRequestResult, rest.uuid);
				  		
			  			if (restFound) {
			  				loggerLib.log('Step[' + step + '] Rest found');
			  				lastFoundLatLng = coordsToCheck;
			  				lastFound = distanceMeters;
			  				if (lastNotFound) {
			  					distanceMeters = Math.round((distanceMeters + lastNotFound) / 2);
			  				} else {
			  					distanceMeters = distanceMeters + Math.round(distanceMeters / 2);
			  				}
			  				if (distanceMeters == oldDistanceMeters) {
			  					distanceMeters = distanceMeters + 1;
			  				}
			  			} else {
			  				loggerLib.log('Step[' + step + '] Rest NOT found');
			  				lastNotFound = distanceMeters;
			  				if (lastFound) {
			  					distanceMeters = Math.round((distanceMeters + lastFound) / 2);
			  				} else {
				  				distanceMeters = distanceMeters - Math.round(distanceMeters / 2);		  					
			  				}
			  				if (distanceMeters == oldDistanceMeters) {
			  					distanceMeters = distanceMeters - 1;
			  				}
			  			}
			  			
			  			if (lastFound && lastNotFound) {
			  				var accuracy = Math.abs(lastFound - lastNotFound);
			  				loggerLib.log('Step[' + step + '] Accuracy: ' + accuracy);
			  				if (accuracy <= 100) {
			  					var dirText = getDirectionText(direction); 
					  			rest[dirText + '_distance'] = lastFound;
					  			rest[dirText + '_max_lat'] = lastFoundLatLng.lat;
					  			rest[dirText + '_max_lng'] = lastFoundLatLng.lon;
			  					distanceFound = true;
			  					loggerLib.log('Step[' + step + '] FINISHED !!! Direction: ' + directionText + ' Distance: ' + lastFound);
			  				}
			  			}
			  			step++;
			  			
			  			if (step >= maxSteps) {
			  				errorOccured = true;
			  			  distanceFound = true;
			  				break;
			  			}
			  			
			  		}
			  	}
			  	
			  	if (!errorOccured) {
			  		rest.dateTimeUTC0 = (new Date()).toISOString();
		  			rest.dateTimeLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format();
		  			rest.dateLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('YYYY-MM-DD');
		  			rest.hourLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('HH');
		  			rest.dayLocal =  momentTz(new Date()).tz(jobDescription.TimeZone).format('dddd');
		  			rest.fullAddress = rest.restAddress;
		  			rest.lat = rest.lat;
		  			rest.lng = rest.lng;
		  			resultsGlobal.push(rest);		  	
				  	
		  			const threadTime = Math.round((process.hrtime(start)[0]/60));
				  	loggerLib.log('--- Results Saved: ' + resultsGlobal.length + ' --- Execution Time (minutes): ' + threadTime);
						if (fs.existsSync(fName)) {
							fs.unlinkSync(fName);
						}
						fs.writeFileSync(fName, json2csv({data: resultsGlobal, fields: fields}), 'ascii');		
			  	}
				}
				
				
				loggerLib.log('ADDRESS END|' + address + '|Results: ' + result.rests.length + '|Results Total: ' + resultsGlobal.length);				
		  } else {
		  	loggerLib.log('ADDRESS END|' + address + '|NO Results');
		  }
			
		} // Address END
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));
		
		loggerLib.log('JOB END|' + (jdi+1) + '|' + jobTime + '|' + usedMemory + '|Results saved: ' + resultsGlobal.length);
	} // JOB END
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
	
	return resultsGlobal;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
  
  loggerLib.log('Process exit');
  process.exit();
});