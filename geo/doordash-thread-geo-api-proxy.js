// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js 1 debug

// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js 1 debug >> ./logs/doordash-thread-geo-1-api-proxy.log 2>&1 &
// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js 2 debug >> ./logs/doordash-thread-geo-2-api-proxy.log 2>&1 &
// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js 3 debug >> ./logs/doordash-thread-geo-3-api-proxy.log 2>&1 &
// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js 4 debug >> ./logs/doordash-thread-geo-4-api-proxy.log 2>&1 &
// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-geo-api-proxy.js 5 debug >> ./logs/doordash-thread-geo-5-api-proxy.log 2>&1 &


const configLib = require("./lib/config");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const geo = require('geolocation-utils');
const proxyLib = require('./lib/proxy');
const addressCoordsLib = require('./lib/address_coords');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');
const json2csv = require('json2csv');
const csv = require('csvtojson');
const fs = require('fs');


process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = 'DoorDash';
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
    if (val == 'debug') {
      DEBUG_MODE = true;
    }
  }
});
loggerLib.threadId = THREAD_ID;

let resultsGlobal = [];

function getDirectionText(directon) {
	switch (directon) {
	  case 0: return 'north'
	  case 45: return 'north_east'
	  case 90: return 'east'
	  case 135: return 'south_east'
	  case 180: return 'south'
	  case 225: return 'south_west'
	  case 270: return 'west'
	  case 315: return 'north_west'
	}
}

let fName = './results/dd-geo-results-' + THREAD_ID + '.csv';

const fields = [
                'site', 'market', 'address', 'restaurant_id', 'name', 'fullAddress', 'lat', 'lng', 'api_radius',
                'north_distance', 'north_max_lat', 'north_max_lng',
                'north_east_distance', 'north_east_max_lat', 'north_east_max_lng',
                'east_distance', 'east_max_lat', 'east_max_lng',
                'south_east_distance', 'south_east_max_lat', 'south_east_max_lng',
                'south_distance', 'south_max_lat', 'south_max_lng',
                'south_west_distance', 'south_west_max_lat', 'south_west_max_lng',
                'west_distance', 'west_max_lat', 'west_max_lng',
                'north_west_distance', 'north_west_max_lat', 'north_west_max_lng',
                'dateTimeUTC0', 'dateTimeLocal', 'hourLocal', 'dayLocal'
            ];

function addressAlreadyProcessed(address, results) {
	var result = false;
	var addressResultCount = 0;
	for (var i = 0; i < results.length; i++) {
		if (results[i].address == address) {
			addressResultCount++;
		}
	}
	if (addressResultCount >= 10) {
		result = true;
	}
	return result;
}

function restAlreadyProcessed(address, restId, results) {
	var result = false;
	for (var i = 0; i < results.length; i++) {
		if (results[i].address == address && results[i].restaurant_id == restId) {
			result = true;
			break;
		}
	}
	return result;
}

loggerLib.log('--- START');

var nightmare;

// Main RUN Function
const run = function * () {
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
	if (jobDescriptions.length == 0) {
	  return;
	}
	
	if (fs.existsSync(fName)) {
		resultsGlobal = yield function(){
			return new Promise((res, rej) => {
				let result = [];
				csv({
					delimiter: ",",
				})
				.fromFile(fName)
				.on('json', (json) => {
					result.push(json);
				})
				.on('error', (error)=>{
					rej(error);
				})
				.on('end', () => {
					res(result);	
				});
			});
		}();	
	}
	
	const nightmareConfig = {
	    waitTimeout: 15000,
	    executionTimeout: 300000,
		  show: false,
		  switches: {
					'ignore-certificate-errors': true
			},					  
		  webPreferences: {
		  	partition: "dd-partition-" + Math.random(),
		    webaudio: false,
		    images: false,
		    plugins: false,
		    webgl: false,    
		    webSecurity: false
		  }
	};
	
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
      parent.respondTo('proxy', function(host, done) {
          win.webContents.session.setProxy({
              proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
          }, function() {
              done();
          });
      });
      done();
  },
  function(host, done) {
      this.child.call('proxy', host, done);
  });
	
	nightmare = new Nightmare(nightmareConfig);
	
  nightmare.on('console', (log, msg) => {
  	if (typeof msg === 'string' || msg instanceof String) {
  		if (msg.indexOf('CONSOLE:') >= 0) {
        loggerLib.log(msg);
      }
  	}
  });
	
	const curDateStr = (new Date()).toISOString().replace('T', ' ').split('.')[0].split(' ')[0]; 
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		let addresses = configLib.getAddresses(jobDescription);
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length);
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let homePageReload = true;
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let addressResult = [];
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length + ' JOB: ' + (jdi+1) + '/' + jobDescriptions.length + ' ' + address);
			
			var addressProcessed = addressAlreadyProcessed(address, resultsGlobal);
			if (addressProcessed) {
				loggerLib.log('ADDRESS ALREADY PROCESSED');	
				continue;
			}			
			
			if (homePageReload) {
				let proxy = '';
				let proxyFound = false;
				while (!proxyFound) {
					proxy = proxyLib.getRandomUnusedProxy();
					loggerLib.log('Checking Proxy: ' + proxy);
					proxyFound = yield proxyLib.checkProxy(proxy).then(data => {
						loggerLib.log('Proxy OK');
						proxyLib.setProxyInUse(proxy);
						return true;
				  }).catch((error) => {
				  	loggerLib.log('Proxy ERROR');
				  	proxyLib.setProxyInUse(proxy);
				  	return false;
				  });
				}
				loggerLib.log('Using Proxy: ' + proxy);
				let userAgent = configLib.getRandomUserAgentString();
				var homePageLoad = yield nightmare
							.proxy(proxy)
							.useragent(userAgent).viewport(720, 1280)
							.filter({
						    urls: [
					           	 'https://typography.doordash.com/*',
					           	 'https://connect.facebook.net/*',
					           	 'https://cdn.doordash.com/*',
					           	 'https://apis.google.com/*',
					           	 'https://bam.nr-data.net/*',
					           	 'https://js-agent.newrelic.com/*',
					           	 'https://web.btncdn.com/*',
					           	 'https://cdn.segment.com/*'
						          ],
						  }, function(details, cb) {
						    return cb({ cancel: true });
						  })
							.goto('https://www.doordash.com')
							.inject('js', 'inject-js/jquery.min.js')
							.inject('js', 'inject-js/moment-with-locales.min.js')
							.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
							.evaluate(function() {
								 return document.body.innerHTML.indexOf('id="root"') >= 0 || document.body.innerHTML.indexOf('id="ConsumerApp"') >= 0;
             	 })				
							.catch(function(error) {
						    if (DEBUG_MODE) {
						      const screenshotFileName = loggerLib.screenshotFileName('home_page_load_error');
			  		  	  nightmare.screenshot(screenshotFileName);
			  		  	  loggerLib.log(error, null, [screenshotFileName]);
						    } else {
						      loggerLib.log(error);
						    }
						    return false;
					  	});
				loggerLib.log('Home Page Load: ' + homePageLoad);
				if (!homePageLoad) {
					continue;
				}
				homePageReload = false;
			}			
			
			// Get Search Queries From Job Description
			const searchQueries = configLib.getSearchQueries(jobDescription);
			
		  var latLng =  yield addressCoordsLib.getCoordinatesByAddress(address).then(data => {
		    return data;
		  }).catch((error) => {
		  	loggerLib.log(error);
		    return [];
		  });			
			
			var result = yield nightmare
								 .evaluate((addressTimeZoneSearchQueries, done) => {
									 
										var result = {};
										result.rests = [];
										result.errors = [];
										
								  	if (typeof window == 'undefined' || window == null) {
								  		console.log('CONSOLE: ERROR: window is not defined');
								  		done(null, result);	
								  	}
										
										var searchParams = {};
										var searchQueriesWithEmpty = [''];
										
									  try {
											window.address = addressTimeZoneSearchQueries.address;
										 	
										 	console.log('CONSOLE: ' + window.address + '| Evaluate Start');
										 	
								 			searchParams.offset = 0;
								 			searchParams.lat = addressTimeZoneSearchQueries.addressLatLng.lat;
								 			searchParams.lng = addressTimeZoneSearchQueries.addressLatLng.lng;
								 			searchParams.limit = 48;
								 			searchParams.suggest_mode = 'true';
								 			searchParams.search_items = 'true';
								 			searchParams.extra = 'stores.business_id';
								 			searchParams.autocomplete = 'true';
								 			
										} catch (error) {
							    		console.log('CONSOLE: ERROR: ' + error);
							    		done(null, result);
							    	}
						 				
					 					var searchPromisesArr = [];
										searchPromisesArr.push(function() {
												return new Promise(function(searchPromisesRes, searchPromisesRej) {
													let pars = JSON.parse(JSON.stringify(searchParams));
													console.log('CONSOLE: ' + window.address + '| SearchRequest Start');
													$.ajax({
													  url: "https://api.doordash.com/v2/store_search/",
													  method: "GET",
													  dataType: "json",
													  cache: false,
													  crossDomain: true,
													  data: pars,
													  contentType: "application/json",
													  beforeSend: function(xhr){
													  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
													  },
												    xhrFields: {
												    	withCredentials: true
												    },
												    success: function(searchRequestResponse) {
												    	console.log('CONSOLE: ' + window.address + '| SearchRequest Done');
												    	
												    	var stores = [];
												    	
												    	try {
													    	if (searchRequestResponse.stores && searchRequestResponse.stores.length > 0) {
													    		for (let sti = 0; sti < searchRequestResponse.stores.length; sti++) {
													    			let store = searchRequestResponse.stores[sti];
													    			
													    			console.log('CONSOLE: ' + window.address + '| RestRequest Start: ' + store.name);
													    			
													    			let storeDetails = $.ajax({
																							    	        type: "GET",
																							    	        url: "https://api.doordash.com/v2/restaurant/" + store.id + "/",
																													  dataType: "json",
																													  cache: false,
																													  crossDomain: true,
																													  contentType: "application/json",
																													  beforeSend: function(xhr){
																													  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
																													  },
																												    xhrFields: {
																												    	withCredentials: true
																												    },																							    	        
																							    	        async: false
																							    	    }).responseText;
													    			storeDetails = JSON.parse(storeDetails);
													    			console.log('CONSOLE: ' + window.address + '| RestRequest Done: ' + storeDetails.name);
													    			storeDetails.restAddress = storeDetails.address;
													    			result.rests.push(storeDetails);
													    			if (sti >= 9) {
													    				break;
													    			}
													    		}
													    	}
												    	} catch (error) {
												    		console.log('CONSOLE: ERROR: ' + error);
												    	}
												    	
												    	searchPromisesRes();
												    	return;
												    },
												    error: function(msg, textStatus, errorThrown) {
												    	console.log('CONSOLE: SearchRequestResponse: error');
															var resultToSave = {};
															resultToSave.request = 'storeSearchRequest';
															resultToSave.msg = msg;
															resultToSave.textStatus = textStatus;
															resultToSave.errorThrown = errorThrown;
															result.errors.push(resultToSave);
															searchPromisesRes();
															return;
												    },
														timeout: 8000
													});
												});
										});
										
										searchPromisesArr.push(function() {
											return new Promise(function(res, rej) {
												console.log('CONSOLE: ' + window.address + '| Evaluate Done');
												done(null, result);
												res();
												return;
											})
										});
										
										searchPromisesArr.reduce(function(prev, cur) { return prev.then(cur) }, Promise.resolve());
										return;		
									}, {address: address, addressLatLng: latLng, timeZone: jobDescription.TimeZone, searchQueries: searchQueries})
								  .catch(function(error) {
								    if (DEBUG_MODE) {
								      const screenshotFileName = loggerLib.screenshotFileName('address_search_error');
					  		  	  nightmare.screenshot(screenshotFileName);
					  		  	  loggerLib.log(error, null, [screenshotFileName]);
								    } else {
								      loggerLib.log(error);
								    }
								    homePageReload = true;
							  		return {};
							  	});
			
			
			
			
	  	if (typeof result.errors != 'undefined' && result.errors.length > 0) {
	  		for (let eri = 0; eri < result.errors.length; eri++) {
	  			loggerLib.log(result.errors[eri]);	
	  		}
	  		delete result.errors;
	  		homePageReload = true;
	  	}
	  	
	  	if (typeof result.rests != 'undefined' && result.rests.length > 0) {
	  		var directions = [0, 45, 90, 135, 180, 225, 270, 315];
				for (var ri = 0; ri < result.rests.length; ri++) {
					result.rests[ri].site = WEBSITE_NAME;
					result.rests[ri].address = address;
					result.rests[ri].market = jobDescription.CBSA;
					var rest = result.rests[ri];
					
					if (!rest.restAddress) {
						continue;
					}
					
			  	if (restAlreadyProcessed(address, rest.id, resultsGlobal)) {
			  		loggerLib.log('REST Already Processed');	
			  		continue;
			  	}
					
					loggerLib.log('Checking ' + rest.name + ' ' + (ri+1) + '/' + result.rests.length);
					var errorOccured = false;
			  	for (var dirI = 0; dirI < directions.length; dirI++) {
			  		var direction = directions[dirI];
			  		
			  		if (errorOccured) {
			  			break;
			  		}
			  		
			  		var distanceMeters = 6000;
			  		
			  		var lastFound = null;
			  		var lastFoundLatLng = {};
			  		var lastNotFound = null;
			  		var maxSteps = 50;
			  		
			  		var step = 1;
			  		var distanceFound = false;
			  		while (!distanceFound) {
			  			var oldDistanceMeters = distanceMeters;
			  			var directionText = getDirectionText(direction);
			  			loggerLib.log('Step[' + step + '] Distance: ' + distanceMeters + ' Direction: ' + directionText);
			  			
			  			
			  			var coordsToCheck = geo.moveTo({lat: rest.restAddress.lat, lon: rest.restAddress.lng}, {heading: direction, distance: distanceMeters});
			  			
				  		var restFound = yield nightmare
							 										.evaluate((restIdLatLng, done) => {
							 											var result = false;
							 									  	if (typeof window == 'undefined' || window == null) {
							 									  		console.log('CONSOLE: ERROR: window is not defined');
							 									  		done(null, result);	
							 									  	}
													    			let deliverable = $.ajax({
																						    	        type: "GET",
																						    	        url: "https://api.doordash.com/v1/stores/" + restIdLatLng.restId + "/deliverable/?lat=" + restIdLatLng.latLng.lat + "&lng=" + restIdLatLng.latLng.lng,
																												  dataType: "json",
																												  cache: false,
																												  crossDomain: true,
																												  contentType: "application/json",
																												  beforeSend: function(xhr){
																												  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
																												  },
																											    xhrFields: {
																											    	withCredentials: true
																											    },																							    	        
																						    	        async: false
																						    	    }).responseText;
													    			
													    			deliverable = JSON.parse(deliverable);
													    			result = deliverable.deliver_to_address; 
													    			done(null, result);	
							 										}, {restId: rest.id, latLng: {lat: coordsToCheck.lat, lng: coordsToCheck.lon}})
							 									  .catch(function(error) {
						 									      loggerLib.log(error);
							 									    homePageReload = true;
							 								  		return null;
							 								  	});
				  			
				  		if (restFound === null) {
				  			distanceFound = true;
				  			errorOccured = true;
				  			break;
				  		}
				  			
			  			if (restFound) {
			  				loggerLib.log('Step[' + step + '] Rest found');
			  				lastFoundLatLng = coordsToCheck;
			  				lastFound = distanceMeters;
			  				if (lastNotFound) {
			  					distanceMeters = Math.round((distanceMeters + lastNotFound) / 2);
			  				} else {
			  					distanceMeters = distanceMeters + Math.round(distanceMeters / 2);
			  				}
			  				if (distanceMeters == oldDistanceMeters) {
			  					distanceMeters = distanceMeters + 1;
			  				}
			  			} else {
			  				loggerLib.log('Step[' + step + '] Rest NOT found');
			  				lastNotFound = distanceMeters;
			  				if (lastFound) {
			  					distanceMeters = Math.round((distanceMeters + lastFound) / 2);
			  				} else {
				  				distanceMeters = distanceMeters - Math.round(distanceMeters / 2);		  					
			  				}
			  				if (distanceMeters == oldDistanceMeters) {
			  					distanceMeters = distanceMeters - 1;
			  				}
			  			}
			  			
			  			if (lastFound && lastNotFound) {
			  				var accuracy = Math.abs(lastFound - lastNotFound);
			  				loggerLib.log('Step[' + step + '] Accuracy: ' + accuracy);
			  				if (accuracy <= 100) {
			  					var dirText = getDirectionText(direction); 
					  			rest[dirText + '_distance'] = lastFound;
					  			rest[dirText + '_max_lat'] = lastFoundLatLng.lat;
					  			rest[dirText + '_max_lng'] = lastFoundLatLng.lon;
			  					distanceFound = true;
			  					loggerLib.log('Step[' + step + '] FINISHED !!! Direction: ' + directionText + ' Distance: ' + lastFound);
			  				}
			  			}
			  			step++;
			  			
			  			if (step >= maxSteps) {
			  				errorOccured = true;
			  			  distanceFound = true;
			  				break;
			  			}
			  			
			  		}
			  	}
			  	
			  	if (!errorOccured) {
			  		rest.dateTimeUTC0 = (new Date()).toISOString();
		  			rest.dateTimeLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format();
		  			rest.dateLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('YYYY-MM-DD');
		  			rest.hourLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('HH');
		  			rest.dayLocal =  momentTz(new Date()).tz(jobDescription.TimeZone).format('dddd');
		  			rest.fullAddress = rest.restAddress.printable_address;
		  			rest.api_radius = rest.delivery_radius; 
		  			rest.restaurant_id = rest.id;
		  			rest.lat = rest.restAddress.lat;
		  			rest.lng = rest.restAddress.lng;
		  			resultsGlobal.push(rest);		  	
				  	
		  			const threadTime = Math.round((process.hrtime(start)[0]/60));
				  	loggerLib.log('--- Results Saved: ' + resultsGlobal.length + ' --- Execution Time (minutes): ' + threadTime);
						if (fs.existsSync(fName)) {
							fs.unlinkSync(fName);
						}
						fs.writeFileSync(fName, json2csv({data: resultsGlobal, fields: fields}), 'ascii');
			  	}
			  	
				}
				
				loggerLib.log('ADDRESS END|' + address + '|Results: ' + result.rests.length);				
		  } else {
		  	loggerLib.log('ADDRESS END|' + address + '|NO Results');
		  }
	  	
			
			if (nightmare) {
				yield nightmare.evaluate(() => {
			    window.localStorage.clear();
			    return;
			  }).catch(function(error) {
		      loggerLib.log(error);
		      return;
		  	});
			  yield nightmare.cookies.clearAll();
			}
			
		} // JOB END
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));		
		loggerLib.log('JOB END|' + (jdi+1) + '|' + jobTime + '|' + usedMemory );
	}
	
  if (nightmare) {
  	nightmare.proc.kill();
  	nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }	
	
	return resultsGlobal;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }	
	
  loggerLib.log('Process exit');
	process.exit();
});