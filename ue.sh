#!/bin/bash

  for b in `seq 1 4`
  do
    DEBUG=* xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony --max-old-space-size=1024 ubereats-thread-v2-api.js $b >> ./logs/ubereats-thread-$b-api.log 2>&1 &
    sleep 3
  done