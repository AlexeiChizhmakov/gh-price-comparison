// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony doordash-thread-api-proxy.js 1 debug
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread-api-proxy.js 1 debug
// DEBUG=nightmare:*,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony doordash-thread-api-proxy.js 1 debug >> ./doordash-thread-api-proxy.log 2>&1

const configLib = require("./lib/config");
const resultsLib = require("./lib/results2");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const proxyLib = require('./lib/proxy');
const addressCoordsLib = require('./lib/address_coords');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js')

process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = 'DoorDash';
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
    if (val == 'debug') {
      DEBUG_MODE = true;
    }
  }
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

var nightmare;

// Main RUN Function
const run = function * () {
	
	const resultsGlobal = [];
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
	if (jobDescriptions.length == 0) {
	  return;
	}
	
	const nightmareConfig = {
	    waitTimeout: 15000,
	    executionTimeout: 300000,
		  show: false,
		  switches: {
					'ignore-certificate-errors': true
			},					  
		  webPreferences: {
		  	partition: "dd-partition-" + Math.random(),
		    webaudio: false,
		    images: false,
		    plugins: false,
		    webgl: false,    
		    webSecurity: false
		  }
	};
	
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
      parent.respondTo('proxy', function(host, done) {
          win.webContents.session.setProxy({
              proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
          }, function() {
              done();
          });
      });
      done();
  },
  function(host, done) {
      this.child.call('proxy', host, done);
  });
	
	nightmare = new Nightmare(nightmareConfig);
	
  nightmare.on('console', (log, msg) => {
  	if (typeof msg === 'string' || msg instanceof String) {
  		if (msg.indexOf('CONSOLE:') >= 0) {
        loggerLib.log(msg);
      }
  	}
  });
	
	const curDateStr = (new Date()).toISOString().replace('T', ' ').split('.')[0].split(' ')[0]; 
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		let addresses = configLib.getAddresses(jobDescription);
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length);
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let homePageReload = true;
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let addressResult = [];
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length + ' JOB: ' + (jdi+1) + '/' + jobDescriptions.length + ' ' + address);			
			
			if (homePageReload) {
				let proxy = '';
				let proxyFound = false;
				while (!proxyFound) {
					proxy = proxyLib.getRandomUnusedProxy();
					loggerLib.log('Checking Proxy: ' + proxy);
					proxyFound = yield proxyLib.checkProxy(proxy).then(data => {
						loggerLib.log('Proxy OK');
						proxyLib.setProxyInUse(proxy);
						return true;
				  }).catch((error) => {
				  	loggerLib.log('Proxy ERROR');
				  	proxyLib.setProxyInUse(proxy);
				  	return false;
				  });
				}
				loggerLib.log('Using Proxy: ' + proxy);
				let userAgent = configLib.getRandomUserAgentString();
				var homePageLoad = yield nightmare
							.proxy(proxy)
							.useragent(userAgent).viewport(720, 1280)
							.filter({
						    urls: [
					           	 'https://typography.doordash.com/*',
					           	 'https://connect.facebook.net/*',
					           	 'https://cdn.doordash.com/*',
					           	 'https://apis.google.com/*',
					           	 'https://bam.nr-data.net/*',
					           	 'https://js-agent.newrelic.com/*',
					           	 'https://web.btncdn.com/*',
					           	 'https://cdn.segment.com/*'
						          ],
						  }, function(details, cb) {
						    return cb({ cancel: true });
						  })
							.goto('https://www.doordash.com')
							.inject('js', 'inject-js/jquery.min.js')
							.inject('js', 'inject-js/moment-with-locales.min.js')
							.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
							.evaluate(function() {
								 return document.body.innerHTML.indexOf('id="root"') >= 0 || document.body.innerHTML.indexOf('id="ConsumerApp"') >= 0;
             	 })				
							.catch(function(error) {
						    if (DEBUG_MODE) {
						      const screenshotFileName = loggerLib.screenshotFileName('home_page_load_error');
			  		  	  nightmare.screenshot(screenshotFileName);
			  		  	  loggerLib.log(error, null, [screenshotFileName]);
						    } else {
						      loggerLib.log(error);
						    }
						    return false;
					  	});
				loggerLib.log('Home Page Load: ' + homePageLoad);
				if (!homePageLoad) {
					continue;
				}
				homePageReload = false;
			}			
			
			// Get Search Queries From Job Description
			const searchQueries = configLib.getSearchQueries(jobDescription);
			
		  var latLng =  yield addressCoordsLib.getCoordinatesByAddress(address).then(data => {
		    return data;
		  }).catch((error) => {
		  	loggerLib.log(error);
		    return [];
		  });			
			
			var result = yield nightmare
								 .evaluate((addressTimeZoneSearchQueries, done) => {
									 
										var result = {};
										result.rests = [];
										result.errors = [];
										
								  	if (typeof window == 'undefined' || window == null) {
								  		console.log('CONSOLE: ERROR: window is not defined');
								  		done(null, result);	
								  	}
										
										var searchParams = {};
										var searchQueriesWithEmpty = [''];
										
									  try {
											window.address = addressTimeZoneSearchQueries.address;
										 	
										 	console.log('CONSOLE: ' + window.address + '| Evaluate Start');
										 	
								 			var categories = addressTimeZoneSearchQueries.searchQueries;
							 				searchQueriesWithEmpty = [''];
							 				searchQueriesWithEmpty.push(...categories);
							 				
							 				console.log('CONSOLE: ' + window.address + '| Search Queries: ' + categories.join(','));
							 				
								 			searchParams.offset = 0;
								 			searchParams.lat = addressTimeZoneSearchQueries.addressLatLng.lat;
								 			searchParams.lng = addressTimeZoneSearchQueries.addressLatLng.lng;
								 			searchParams.limit = 48;
								 			searchParams.suggest_mode = 'true';
								 			searchParams.search_items = 'true';
								 			searchParams.extra = 'stores.business_id';
								 			searchParams.autocomplete = 'true';
								 			
										} catch (error) {
							    		console.log('CONSOLE: ERROR: ' + error);
							    		done(null, result);
							    	}
						 				
					 					var searchPromisesArr = [];
										var catsTotal = searchQueriesWithEmpty.length;
										for (var i = 0; i < catsTotal; i++) {
											searchPromisesArr.push(function() {
													return new Promise(function(searchPromisesRes, searchPromisesRej) {
														let searchQuery = searchQueriesWithEmpty.shift(); 
														let pars = JSON.parse(JSON.stringify(searchParams));
														if (searchQuery.length > 0) {
															pars.query = searchQuery;
														} 
														console.log('CONSOLE: ' + window.address + '| SearchRequest (' + searchQuery + '): Start');
														$.ajax({
														  url: "https://api.doordash.com/v2/store_search/",
														  method: "GET",
														  dataType: "json",
														  cache: false,
														  crossDomain: true,
														  data: pars,
														  searchQuery: searchQuery,
														  contentType: "application/json",
														  beforeSend: function(xhr){
														  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
														  },
													    xhrFields: {
													    	withCredentials: true
													    },
													    success: function(searchRequestResponse) {
													    	console.log('CONSOLE: ' + window.address + '| SearchRequest (' + this.searchQuery + '): Done');
													    	
													    	var stores = [];
													    	
													    	try {
														    	if (searchRequestResponse.stores && searchRequestResponse.stores.length > 0) {
														    		for (let sti = 0; sti < searchRequestResponse.stores.length; sti++) {
														    			let store = searchRequestResponse.stores[sti];
														    			if (store.status.asap_available) {
														    				stores.push(store);
														    			}
														    			if (sti >= 19) {
														    				break;
														    			}
														    		}
														    	}
													    	} catch (error) {
													    		console.log('CONSOLE: ERROR: ' + error);
													    	}
													    	
													    	let searchQuery = this.searchQuery;
													    	
													    	console.log('CONSOLE: ' + window.address + '| SearchRequest (' + this.searchQuery + ') | Stores: ' + stores.length);
													    	
													    	restPromisesArr = [];
													    	
													    	let restTotal = stores.length;
													    	for (var ri = 0; ri < restTotal; ri++) {
													    		restPromisesArr.push(function() {
																		return new Promise(function(restPromisesRes, restPromisesRej) {
																			console.log('CONSOLE: ' + window.address + '| RestSearch Start');
																			try {
																				let rest = stores.shift();
																				let restId = rest.id;
																				
																				$.ajax({
																				  url: "https://api.doordash.com/v2/restaurant/" + restId + "/",
																				  method: "GET",
																				  dataType: "json",
																				  cache: false,
																				  crossDomain: true,
																				  searchQuery: searchQuery,
																				  contentType: "application/json",
																				  beforeSend: function(xhr){
																				  	xhr.setRequestHeader('Cache-Control', 'max-age=0')
																				  },
																			    xhrFields: {
																			    	withCredentials: true
																			    },
																			    success: function(restRequestResponse) {
																			    	console.log('CONSOLE: ' + window.address + '| RestSearch (' + restId + ') [' + this.searchQuery + '] Done');
																			    	try {
																			    		if (restRequestResponse.asap_time) {
																					    	var resultToSave = {};
																					    	
																					    	resultToSave.name = restRequestResponse.business.name;
																					      resultToSave.searchQuery = this.searchQuery;
																		 					  resultToSave.href = 'https://www.doordash.com/store/' + restRequestResponse.slug + '-' + restRequestResponse.id + '/';
																		 				  	resultToSave.position = restTotal - stores.length;
																		 				  	
																			  	  		resultToSave.price = restRequestResponse.price_range;	
																			  	  		
																			  	  		let cuisine = restRequestResponse.description.split(',');
																			  	  		let cuisineArr = []; 
																			  	  		for (let ci = 0; ci < cuisine.length; ci++) {
																			  	  			cuisineArr.push(cuisine[ci].trim());
																			  	  		}
																				  	  	resultToSave.cuisine = cuisineArr.join('|');
																				  	  	
																				  	  	var eta = restRequestResponse.status;
																				  	  	if (eta.indexOf('mins') >= 0) {
																				  	  		eta = eta.replace('mins', '').trim();;
																				  	  		eta = eta.split('-');
																				  	  		if (eta.length == 2) {
																						  	  	resultToSave.eta = eta[0].trim() + '-' + eta[1].trim();
																						  	  	resultToSave.etaMin = eta[0].trim();
																						  	  	resultToSave.etaMax = eta[1].trim();																			  	  			
																				  	  		}
																				  	  	}
																				  	  	
																			  	  		resultToSave.deliverySearchPageFee = restRequestResponse.delivery_fee / 100;
																			  	  		resultToSave.deliveryMenuPageFee = restRequestResponse.delivery_fee / 100;
																			  	  		resultToSave.deliveryCheckoutPageFee = restRequestResponse.delivery_fee / 100;
																			  	  		resultToSave.deliveryFeeWithoutDiscounts = restRequestResponse.delivery_fee_details.original_fee.unit_amount / 100;
																			  	  		resultToSave.serviceFee = restRequestResponse.service_rate;
																			  	  		
																			  	  		let freeDeliveryMinOrder = null;
																			  	  		let merchantPromotions = restRequestResponse.merchant_promotions;
																			  	  		for (let mpi = 0; mpi < merchantPromotions.length; mpi++) {
																			  	  			if (merchantPromotions[mpi].delivery_fee === 0) {
																			  	  				freeDeliveryMinOrder = merchantPromotions[mpi].minimum_order_cart_subtotal / 100;
																			  	  			}
																			  	  		}
																			  	  		if (freeDeliveryMinOrder) {
																			  	  			resultToSave.freeDeliveryMinOrder = freeDeliveryMinOrder;
																			  	  		}
																			  	  		
																				  	  	resultToSave.dateTimeUTC0 = (new Date()).toISOString(); 						  	  	
																				  	  	resultToSave.dateTimeLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD hh:mm:ss');
																				  	  	resultToSave.dateLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD');
																				  	  	resultToSave.hourLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('HH');
																				  	  	resultToSave.dayLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('dddd');
																			  	  		
																					    	result.rests.push(resultToSave);
																			    		}
																			    	} catch (error) {
																							console.log('CONSOLE: ERROR: ' + error);
																						}
																			    	
																			    	restPromisesRes();
																			    },
																			    error: function(msg, textStatus, errorThrown) {
																			    	console.log('CONSOLE: RestRequestResponse: error');
																						var resultToSave = {};
																						resultToSave.request = 'restRequest';
																						resultToSave.msg = msg;
																						resultToSave.textStatus = textStatus;
																						resultToSave.errorThrown = errorThrown;
																						result.errors.push(resultToSave);
																						restPromisesRes();
																			    },
																					timeout: 8000
																				});
																    	} catch (error) {
																				console.log('CONSOLE: ERROR: ' + error);
																				restPromisesRes();
																			}
																			
																		});
																	});
													    	}
													    	
													    	restPromisesArr.push(function() {
																	return new Promise(function(res, rej) {
																		searchPromisesRes();
																		res();
																		return;
																	})
																});
																
													    	restPromisesArr.reduce(function(prev, cur) { return prev.then(cur) }, Promise.resolve());
													    	return;
													    },
													    error: function(msg, textStatus, errorThrown) {
													    	console.log('CONSOLE: SearchRequestResponse: error');
																var resultToSave = {};
																resultToSave.request = 'storeSearchRequest';
																resultToSave.msg = msg;
																resultToSave.textStatus = textStatus;
																resultToSave.errorThrown = errorThrown;
																result.errors.push(resultToSave);
																searchPromisesRes();
																return;
													    },
															timeout: 8000
														});
													});
											});
										}
										
										searchPromisesArr.push(function() {
											return new Promise(function(res, rej) {
												console.log('CONSOLE: ' + window.address + '| Evaluate Done');
												done(null, result);
												res();
												return;
											})
										});
										
										searchPromisesArr.reduce(function(prev, cur) { return prev.then(cur) }, Promise.resolve());
										return;		
									}, {address: address, addressLatLng: latLng, timeZone: jobDescription.TimeZone, searchQueries: searchQueries})
								  .catch(function(error) {
								    if (DEBUG_MODE) {
								      const screenshotFileName = loggerLib.screenshotFileName('address_search_error');
					  		  	  nightmare.screenshot(screenshotFileName);
					  		  	  loggerLib.log(error, null, [screenshotFileName]);
								    } else {
								      loggerLib.log(error);
								    }
								    homePageReload = true;
							  		return {};
							  	});
			
			
	  	if (typeof result.errors != 'undefined' && result.errors.length > 0) {
	  		for (let eri = 0; eri < result.errors.length; eri++) {
	  			loggerLib.log(result.errors[eri]);	
	  		}
	  		delete result.errors;
	  		homePageReload = true;
	  	}
	  	
	  	if (typeof result.rests != 'undefined' && result.rests.length > 0) {

				for (var ri = 0; ri < result.rests.length; ri++) {
					result.rests[ri].site = WEBSITE_NAME;
					result.rests[ri].address = address;
					result.rests[ri].market = jobDescription.CBSA;
				}
				
				resultsGlobal.push(...result.rests);
				
				loggerLib.log('ADDRESS END|' + address + '|Results: ' + result.rests.length + '|Results Total: ' + resultsGlobal.length);				
		  } else {
		  	loggerLib.log('ADDRESS END|' + address + '|NO Results');
		  }	  	
			
			if (nightmare) {
				yield nightmare.evaluate(() => {
			    window.localStorage.clear();
			    return;
			  }).catch(function(error) {
		      loggerLib.log(error);
		      return;
		  	});
			  yield nightmare.cookies.clearAll();
			}
			
		} // JOB END
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));		
		
		if (resultsGlobal.length > 0) {
		  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
		}
		
		loggerLib.log('JOB END|' + (jdi+1) + '|' + jobTime + '|' + usedMemory + '|Results saved: ' + resultsGlobal.length);
	}
	
	if (!DEBUG_MODE) {
		const threadTime = Math.round((process.hrtime(start)[0]/60));
		const cloudWatchLib = require("./lib/cloudwatch");
		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
		
		let totalPrice = 0;
		let avgPrice = 0;
		let totalMaxEta = 0;
		let avgMaxEta = 0;
		for (let i = 0; i < resultsGlobal.length; i++) {
			let res = resultsGlobal[i];
			if (typeof res.deliveryFeeWithoutDiscounts != 'undefined' && res.deliveryFeeWithoutDiscounts != null) {
				totalPrice = totalPrice + parseFloat(res.deliveryFeeWithoutDiscounts);
			}
			if (typeof res.etaMax != 'undefined' && res.etaMax != null) {
				totalMaxEta = totalMaxEta + res.etaMax;
			}
		}
		if (resultsGlobal.length > 0) {
			totalPrice = Math.round(totalPrice * 100) / 100;
			avgPrice = Math.round((totalPrice / resultsGlobal.length) * 100) / 100;
			totalMaxEta = Math.round(totalMaxEta * 100) / 100;
			avgMaxEta = Math.round((totalMaxEta / resultsGlobal.length) * 100) / 100;
			yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Price', avgPrice);
			yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Max ETA', avgMaxEta);		
			loggerLib.log('CW DATA Sent|AVG Price: ' + avgPrice + '|AVG Max ETA: ' + avgMaxEta);			
		}
	}		
	
  if (nightmare) {
  	nightmare.proc.kill();
  	nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }	
	
	return resultsGlobal;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
	if (results) {
		loggerLib.log('FINAL Results saved: ' + results.length);
		if (results.length > 0) {
		  resultsLib.saveToCSV(WEBSITE_NAME, results, THREAD_ID);
		}
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }	
	
  loggerLib.log('Process exit');
	process.exit();
});