
module.exports = {
		
	/**
	 * Get Coordinates By Address
	 */	
	getCoordinatesByAddress: function (address) {
		const csv = require('csvtojson');
		return new Promise((res, rej) => {
			let result = null;
			csv({
				delimiter: ",",
			})
			.fromFile('./cache/address_coordinates.csv')
			.on('json', (json) => {
				if (json['Address'].toLowerCase().trim() == address.toLowerCase().trim()) {
					result = {};
					result.lat = json.Lat;
					result.lng = json.Lng;
				}
			})
			.on('error', (error)=>{
				rej(error);
			})
			.on('end', () => {
				res(result);	
			});
		});
	}
	
}