const constants = require('../config/constants');

module.exports = {
		
	/**
	 * Save Parse Results to CSV file
	 */	
	saveToCSV: function (site, rows, threadId) {
		const Iconv  = require('iconv').Iconv;
		const iconv = new Iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE');
		const fs = require('fs');
		
		rows.forEach(function (row) {
			if (row.name) {
				row.name = iconv.convert(row.name.toString()).toString().replace(/[^\x20-\x7E]+/g, '?'); 
			}
			if (row.cuisine) {
				row.cuisine = iconv.convert(row.cuisine.toString()).toString().replace(/[^\x20-\x7E]+/g, '?');
			}
			if (row.href) {
				row.href = iconv.convert(row.href.toString()).toString().replace(/[^\x20-\x7E]+/g, '?');
			}				
			if (row.eta) {
				row.eta = iconv.convert(row.eta.toString()).toString().replace(/[^\x20-\x7E]+/g, '?');
			}				
			if (row.etaMin) {
				row.etaMin = parseInt(row.etaMin);
			}
			if (row.etaMax) {
				row.etaMax = parseInt(row.etaMax);
			}
			if (row.minOrder) {
				row.minOrder = row.minOrder.toString().replace('$', '').replace('+', '');
				row.minOrder = parseFloat(row.minOrder).toFixed(2).toString().replace(',', '.');
				if (row.minOrder == 'NaN') {
					row.minOrder = '';
				}
			}
			if (row.deliverySearchPageFee) {
				row.deliverySearchPageFee = row.deliverySearchPageFee.toString().replace('$', '').replace('+', '');
				row.deliverySearchPageFee = parseFloat(row.deliverySearchPageFee).toFixed(2).toString().replace(',', '.');
				if (row.deliverySearchPageFee == 'NaN') {
					row.deliverySearchPageFee = '';
				}
			}
			if (row.deliveryMenuPageFee) {
				row.deliveryMenuPageFee = row.deliveryMenuPageFee.toString().replace('$', '');
				row.deliveryMenuPageFee = parseFloat(row.deliveryMenuPageFee).toFixed(2).toString().replace(',', '.');
				if (row.deliveryMenuPageFee == 'NaN') {
					row.deliveryMenuPageFee = '';
				}
			}
			if (row.deliveryCheckoutPageFee) {
				row.deliveryCheckoutPageFee = row.deliveryCheckoutPageFee.toString().replace('$', '');
				row.deliveryCheckoutPageFee = parseFloat(row.deliveryCheckoutPageFee).toFixed(2).toString().replace(',', '.');
				if (row.deliveryCheckoutPageFee == 'NaN') {
					row.deliveryCheckoutPageFee = '';
				}
			}
			if (row.freeDeliveryMinOrder) {
				row.freeDeliveryMinOrder = row.freeDeliveryMinOrder.toString().replace('$', '');
				row.freeDeliveryMinOrder = parseFloat(row.freeDeliveryMinOrder).toFixed(2).toString().replace(',', '.');
				if (row.freeDeliveryMinOrder == 'NaN') {
					row.freeDeliveryMinOrder = '';
				}
			}			
			if (row.smallOrderFee) {
				row.smallOrderFee = row.smallOrderFee.toString().replace('$', '');
				row.smallOrderFee = parseFloat(row.smallOrderFee).toFixed(2).toString().replace(',', '.');
				if (row.smallOrderFee == 'NaN') {
					row.smallOrderFee = '';
				}
			}
			if (row.smallOrderMin) {
				row.smallOrderMin = row.smallOrderMin.toString().replace('$', '');
				row.smallOrderMin = parseFloat(row.smallOrderMin).toFixed(2).toString().replace(',', '.');
				if (row.smallOrderMin == 'NaN') {
					row.smallOrderMin = '';
				}
			}
			if (row.serviceFee) {
				row.serviceFee = row.serviceFee.toString().replace('$', '');
				row.serviceFee = parseFloat(row.serviceFee).toFixed(2).toString().replace(',', '.');
				if (row.serviceFee == 'NaN') {
					row.serviceFee = '';
				}
			}
			if (row.minDeliveryFee) {
				row.minDeliveryFee = row.minDeliveryFee.toString().replace('$', '');
				row.minDeliveryFee = parseFloat(row.minDeliveryFee).toFixed(2).toString().replace(',', '.');
				if (row.minDeliveryFee == 'NaN') {
					row.minDeliveryFee = '';
				}
			}
			if (row.deliveryFeeWithoutDiscounts) {
				row.deliveryFeeWithoutDiscounts = row.deliveryFeeWithoutDiscounts.toString().replace('$', '');
				row.deliveryFeeWithoutDiscounts = parseFloat(row.deliveryFeeWithoutDiscounts).toFixed(2).toString().replace(',', '.');
				if (row.deliveryFeeWithoutDiscounts == 'NaN') {
					row.deliveryFeeWithoutDiscounts = '';
				}
			}				
			if (row.hourLocal) {
				row.hourLocal = parseInt(row.hourLocal);
			}
			if (row.dateTimeUTC0) {
				row.dateTimeUTC0 = row.dateTimeUTC0.replace('T', ' ').split('.')[0];
			}
			row.site = site;	
		});
		 
		const json2csv = require('json2csv');
		
		const fields = [
		                'site', 'market', 'address', 'searchQuery', 'name', 'href', 'position',
		        		    'price', 'cuisine', 'eta', 'etaMin', 'etaMax', 'minOrder',
		        		    'deliverySearchPageFee', 'deliveryMenuPageFee', 'deliveryCheckoutPageFee',
		        		    'minDeliveryFee', 'deliveryFeeWithoutDiscounts',
		        		    'freeDeliveryMinOrder', 'smallOrderFee', 'smallOrderMin', 'serviceFee',
		        		    'dateTimeUTC0', 'dateTimeLocal', 'dateLocal', 'hourLocal', 'dayLocal'
		            ];		

		//[WEBSITE]-[THREAD_ID]-[TYPE]-[DATE]-[HOUR].csv
		const fileName = site + '-' + threadId + '-' + 'Results' + '-' + ((new Date()).toISOString().split('T')[0]) + '-' + ((new Date()).toISOString().split('T')[1].split(':')[0]) + '.csv';

		if (!fs.existsSync(constants.CURRENT_RESULTS_FOLDER)){
			fs.mkdirSync(constants.CURRENT_RESULTS_FOLDER);
		}
		
		fs.writeFileSync(constants.CURRENT_RESULTS_FOLDER + fileName, json2csv({data: rows, fields: fields}), 'ascii');
	},
	
}