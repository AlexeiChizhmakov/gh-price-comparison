const constants = require('../config/constants');

module.exports = {
		
	/**
	 * Log Metric
	 */	
	logMetric: function (websiteName, metricName, metricValue, dimensionName, dimensionValue) {
		const AWS = require('aws-sdk');
		
		if (typeof dimensionName == 'undefined' && typeof dimensionValue == 'undefined') {
			dimensionName = 'Website';
			dimensionValue = websiteName;
		}
		
		AWS.config.update({
				maxRetries: 10,
				accessKeyId: constants.AWS_CW_ACCESS_KEY_ID,
				secretAccessKey: constants.AWS_CW_SECRET_ACCESS_KEY
			}
		);
		
		const metric = {
				Namespace: 'GH Parser',
				MetricData: [
				  {
				    Dimensions: [
				      {
				        Name: dimensionName,
				        Value: dimensionValue
				      },
				    ],
				    MetricName: metricName,
				    Timestamp: new Date().toISOString(),				    
				    Unit: 'Count',
				    Value: metricValue
				  },
				]
		};
		
		const cloudwatch = new AWS.CloudWatch({region: constants.AWS_CW_REGION});
		
		return new Promise((res, rej) => {
			cloudwatch.putMetricData(metric, (err, data) => {
				if (err) {					
					res(err);
				} else {
					res(data);
				}
			});
		});
	},

}