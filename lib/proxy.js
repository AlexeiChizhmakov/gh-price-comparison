const fs = require('fs');

module.exports = {
		
	/**
	 * All Proxies from proxy-list.txt
	 */	
	getAllProxies: function () {
		const proxies = fs.readFileSync('./proxy/proxy-list.txt').toString().split('\n');
		return proxies;
	},
	
	
	getFileName: function (proxy) {
		return './proxy/' + proxy.replace(/\./g, '-').replace(':', '_') + '.proxy';
	},
	
	
	setProxyInUse: function (proxy) {
		fs.writeFileSync(this.getFileName(proxy), '');
	},
	
	
	isProxyInUse: function (proxy) {
		try {
		  fs.accessSync(this.getFileName(proxy), fs.constants.R_OK | fs.constants.W_OK);
		  return true;
		} catch (err) {
			return false;
		}
	},
	
	checkProxy: function(proxy) {
		var ProxyVerifier = require('proxy-verifier');
		
		var proxyObj = {
			ipAddress: proxy.split(':')[0],
			port: parseInt(proxy.split(':')[1]),
			protocol: 'http'
		};
		
		var options = {
		    testUrl: 'http://ping.instantproxies.com',
		    testFn: function(data, status, headers) {
		    	if (status != '200') {
		    		throw new Error('Result status not 200');
		    	} 
		    }
		};
		
		options.requestOptions = {
				timeout: 1000
		};

		return new Promise((res, rej) => {
			ProxyVerifier.test(proxyObj, options,
				function(error, result) {
					if (result) {
						if (result.ok) {
							res('OK');
						} else {
							rej('ERROR');	
						}
					} else {
						rej('ERROR');
					}
				});
		});
		
	},
	
	getRandomUnusedProxy: function() {
		let proxies = this.getAllProxies();
		proxies = this.shuffleArray(proxies);
		
		for (let i = 0; i < proxies.length; i++) {
			if (!this.isProxyInUse(proxies[i])) {
				return proxies[i];
			}
		}

		return proxies[0];
	},
	
	shuffleArray: function shuffle(array) {
	  var currentIndex = array.length, temporaryValue, randomIndex;
	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {
	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;
	    // And swap it with the current element.
	    temporaryValue = array[currentIndex];
	    array[currentIndex] = array[randomIndex];
	    array[randomIndex] = temporaryValue;
	  }
	  return array;
	}

}