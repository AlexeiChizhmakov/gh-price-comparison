// node --harmony grubhub-thread-api.js 1 >> ./logs/grubhub-thread-api.log 2>&1 &
// node --harmony grubhub-thread-api.js 1 >> grubhub-thread-1-api.log 2>&1 &
// node --harmony grubhub-thread-api.js 2 >> grubhub-thread-2-api.log 2>&1 &
// node --harmony grubhub-thread-api.js 3 >> grubhub-thread-3-api.log 2>&1 &
// node --harmony grubhub-thread-api.js 4 >> grubhub-thread-4-api.log 2>&1 &

const vo = require('vo');
const configLib = require("./lib/config");
const loggerLib = require("./lib/logger");
const resultsLib = require("./lib/results2");
const addressCoordsLib = require('./lib/address_coords');
const constants = require('./config/constants');
var request = require('request');

const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js');

//TIMER Start
let start = process.hrtime();

//Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.GRUBHUB_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }  
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

const resultsGlobal = [];


// Main RUN Function
const run = function * () {
	
  // PROD
  const clientId = 'itirra_aWhhdGVpdGlycmFwcmljZXB';
  const baseUrl = 'https://api-gtm.grubhub.com';  
  
  // Get Auth Token
  const authResponse = yield function() {
    return new Promise((res, rej) => {  
      var options = {
          method: 'POST',
          url: baseUrl + '/oauth2/direct/auth',
          json: {
            'grant_type': 'token',
            'client_id': clientId,
            'scope': 'anonymous',
          }
      };
      request(options, function (error, response, body) {
        if (error) {          
          rej('ERROR getting Auth Token: ' + error);
        }
        if (!response || response.statusCode != 200) {          
          rej('ERROR getting Auth Token. Status Code: ' + response.statusCode);         
        }       
        res(body);
      });
    }); 
  }().then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
    return null;
  });

	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);
		const searchQueriesWithEmpty = [''];
		searchQueriesWithEmpty.push(...searchQueries);
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length + ' SEARCH QUERIES: ' + searchQueries.length);
		
		let timezone = '';
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let addressResult = [];
			
			let address = addresses[ai].trim();
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
		  var latLng =  yield addressCoordsLib.getCoordinatesByAddress(address).then(data => {
		    return data;
		  }).catch((error) => {
		  	loggerLib.log(error);
		    return [];
		  });
		  
		  if (typeof latLng != 'undefined' && latLng) {
					
		  	for (var sqi = 0; sqi < searchQueriesWithEmpty.length; sqi++) {
				
		  		let searchQuery = searchQueriesWithEmpty[sqi].trim();
			  
		      if (authResponse) {
		        const authToken = authResponse.access_token;
		        
		        let restResponse = yield function() {
		          return new Promise((res, rej) => {
		            var options = {
		                method: 'GET',
		                url: baseUrl + '/restaurants/search/search_listing',
		                qs: {
		                  'locationMode': 'DELIVERY',
		                  'pageSize': '20',
		                  'location': 'POINT(' + latLng.lng +' ' + latLng.lat + ')',
		                  'facet': 'open_now:true'
		                },            
		                'auth': {
		                  'bearer': authToken
		                }           
		            };
		            
		            if (searchQuery.length > 0) {
		            	options.qs.queryText = searchQuery;
		            }
		            
		            request(options, function (error, response, body) {
		              if (error) {          
		              	loggerLib.log(error);
		              	loggerLib.log(response);
		              	loggerLib.log(body);
		                rej('ERROR getting Results');
		              }
		              if (!response || typeof response == 'undefined' || response.statusCode != 200) {          
		              	loggerLib.log(error);
		              	loggerLib.log(response);
		              	loggerLib.log(body);        
		                rej('ERROR getting Results');          
		              }       
		              res(body);
		            });
		          }); 
		        }().then(data => {
		          return data;
		        }).catch((error) => {
		        	loggerLib.log(error);
		          return null;
		        });
		        
		        try {
		        	restResponse = JSON.parse(restResponse);
		        } catch(error) {
		        	loggerLib.log(error);
		        	restResponse = {};
		        }
		        
		        if (restResponse != null && typeof restResponse.results != 'undefined') {
		        	for (var ri = 0; ri < restResponse.results.length; ri++) {
		        		let restData = restResponse.results[ri];
	    
				        let result = {};
				        
				        result.site = WEBSITE_NAME;
				        result.market = jobDescription.CBSA;				        
				  	  	result.address = address;
				  	  	result.searchQuery = searchQuery;
				  	  	result.name = restData.name;
				  	  	let nameSlug = restData.name.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
				  	  	let addressSlug = restData.address.street_address.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
				  	  	let citySlug = restData.address.address_locality.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
				  	  	result.href = 'https://www.grubhub.com/restaurant/' + nameSlug + '-' + addressSlug + '-' + citySlug + '/' + restData.restaurant_id;
				  	  	result.position = restData.rank;
				  	  	result.price = restData.price_rating;
				  	  	result.cuisine = restData.cuisines.join('|');		  	  	
				  	  	let maxDeliveryTime = restData.delivery_time_estimate + 10;
				  	  	result.eta = restData.delivery_time_estimate + '-' + maxDeliveryTime;
				  	  	result.etaMin = restData.delivery_time_estimate;
				  	  	result.etaMax = maxDeliveryTime;
				  	  	
				  	  	if (typeof restData.delivery_minimum != 'undefined' && restData.delivery_minimum != null) {
				  	  		result.minOrder = restData.delivery_minimum.price / 100;
				  	  	}
				  	  	
				  	  	if (typeof restData.delivery_fee != 'undefined' && restData.delivery_fee != null) {
					  	  	result.deliverySearchPageFee = restData.delivery_fee.price / 100;
					  	  	result.deliveryMenuPageFee = result.deliverySearchPageFee;
					  	  	result.deliveryCheckoutPageFee = result.deliveryMenuPageFee;
				  	  	}
				  	  	
				  	  	if (typeof restData.min_delivery_fee != 'undefined' && restData.min_delivery_fee != null) {
				  	  		result.minDeliveryFee = restData.min_delivery_fee.price / 100;
				  	  	}
				  	  	
				  	  	if (typeof restData.delivery_fee_without_discounts != 'undefined' && restData.delivery_fee_without_discounts != null) {
				  	  		result.deliveryFeeWithoutDiscounts = restData.delivery_fee_without_discounts.price / 100;
				  	  	}
				  	  	
								result.dateTimeUTC0 = (new Date()).toISOString();
								result.dateTimeLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('YYYY-MM-DD hh:mm:ss');
								result.dateLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('YYYY-MM-DD');
								result.hourLocal = momentTz(new Date()).tz(jobDescription.TimeZone).format('HH');
								result.dayLocal =  momentTz(new Date()).tz(jobDescription.TimeZone).format('dddd');
								
								addressResult.push(result);
		        	}
		        	
		        	loggerLib.log('SEARCH QUERY (' + searchQuery + ') END. Results: ' + restResponse.results.length);
		        	
		        	if (restResponse.results.length == 0 && searchQuery.length == 0) {
		        		break;
		        	}		        	
		        }
		        
		      }
				}
		  } else {
		  	loggerLib.log('LAT LNG IS UNDEFINED !!!|' + address);
		  }
			
			resultsGlobal.push(...addressResult);
			loggerLib.log('ADDRESS END. Results: ' + addressResult.length);
		}
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));
		
		if (resultsGlobal.length > 0) {
		  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
		}		
		
		loggerLib.log('JOB END|' + (jdi+1) + '|' + jobTime + '|' + usedMemory + '|Results saved: ' + resultsGlobal.length);
	}
	
	if (!DEBUG_MODE) {
		const threadTime = Math.round((process.hrtime(start)[0]/60));
		const cloudWatchLib = require("./lib/cloudwatch");
		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
				
		let totalPrice = 0;
		let avgPrice = 0;
		let totalMaxEta = 0;
		let avgMaxEta = 0;
		for (let i = 0; i < resultsGlobal.length; i++) {
			let res = resultsGlobal[i];
			if (typeof res.deliverySearchPageFee != 'undefined' && res.deliverySearchPageFee != null) {
				totalPrice = totalPrice + parseFloat(res.deliverySearchPageFee);
			}
			if (typeof res.etaMax != 'undefined' && res.etaMax != null) {
				totalMaxEta = totalMaxEta + res.etaMax;
			}
		}
		if (resultsGlobal.length > 0) {
			totalPrice = Math.round(totalPrice * 100) / 100;
			avgPrice = Math.round((totalPrice / resultsGlobal.length) * 100) / 100;
			totalMaxEta = Math.round(totalMaxEta * 100) / 100;
			avgMaxEta = Math.round((totalMaxEta / resultsGlobal.length) * 100) / 100;
			yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Price', avgPrice);
			yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Max ETA', avgMaxEta);
			loggerLib.log('CW DATA Sent|AVG Price: ' + avgPrice + '|AVG Max ETA: ' + avgMaxEta);
		}
	}
	
	return;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
	
	if (resultsGlobal.length > 0) {
	  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
	}
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  process.exit();
});