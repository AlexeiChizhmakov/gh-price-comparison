// DEBUG=nightmare:*,electron:* node --harmony ubereats-thread-api.js 1
// DEBUG=nightmare:actions,electron:* node --harmony ubereats-thread-api.js 1 &>> ./logs/ubereats-thread1.log&

// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony ubereats-thread-api.js 1 debug
// xvfb-run -a --server-args="-screen 0 720x1280x24" node --harmony ubereats-thread-api.js 1 debug
// xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony ubereats-thread-api.js 1 debug &>> ./logs/ubereats-thread1.log&

// DEBUG=nightmare:actions,electron:* xvfb-run -a --server-args="-screen 0 1280x2000x24" node --harmony ubereats-thread.js 1 debug
// node --harmony ubereats-thread.js 1 &>> ./logs/ubereats-thread1.log&
const configLib = require("./lib/config");
const resultsLib = require("./lib/results2");
const loggerLib = require("./lib/logger");
const constants = require('./config/constants');
const proxyLib = require('./lib/proxy');
const addressCoordsLib = require('./lib/address_coords');
const slugid = require('slugid');
const util = require('util');
const moment = require('./inject-js/moment-with-locales.min');
const momentTz = require('./inject-js/moment-timezone-with-data-2012-2022.min.js')

process.on('uncaughtException', function (err) {
  console.log(err);
  console.log("Node NOT Exiting...");
});

// TIMER Start
let start = process.hrtime();

var Nightmare = require("nightmare");
require('nightmare-load-filter')(Nightmare);
var vo = require('vo');

// Debug Mode
let DEBUG_MODE = false;

// Website Name
const WEBSITE_NAME = constants.UBEREATS_WEBSITE_NAME;
loggerLib.webSite = WEBSITE_NAME;

// Thread ID
let THREAD_ID = 0;
process.argv.forEach(function (val, index, array) {
  if (index == 2) {
  	THREAD_ID = val;
  }
  if (index == 3) {
  	if (val == 'debug') {
  		DEBUG_MODE = true;
  	}
  }
});
loggerLib.threadId = THREAD_ID;

loggerLib.log('--- START');

var nightmare;

// Main RUN Function
const run = function * () {
	
	const resultsGlobal = [];
	
	// Get Job Descriptions for this website/thread from config
	const jobDescriptions = yield configLib.getJobDescription(WEBSITE_NAME, THREAD_ID).then(data => {
    return data;
  }).catch((error) => {
  	loggerLib.log(error);
  	return [];
  });
	let jobDescriptionsNew = [];
	
	loggerLib.log('THREAD ' + THREAD_ID);
	
  if (jobDescriptions.length == 0) {
    return;
  } 
  
  nightmare = new Nightmare({
    waitTimeout: 10000,
    executionTimeout: 200000,
    show: false,
	  switches: {
			'ignore-certificate-errors': true
	  },		    
    webPreferences: {
    	partition: "ue-partition-" + Math.random(),
      webaudio: false,
      images: false,
      plugins: false,
      webgl: false,   
      webSecurity: false
    }
  });
  
  nightmare.on('console', (log, msg) => {
	 	if (msg.indexOf('CONSOLE:') >= 0) {
	 		loggerLib.log(msg);
	 	}
  });
  nightmare.on('close', (data) => {
  	loggerLib.log('Child process closing: ' + data);
  });
  nightmare.on('exit', (data) => {
  	loggerLib.log('Child process exiting: ' + data);
  });
  nightmare.on('error', (data) => {
  	loggerLib.log('Child process error: ' + data);
  });
  nightmare.on('disconnect', (data) => {
  	loggerLib.log('Child process disconnected: ' + data);
  });  
  
	Nightmare.action('proxy', function(name, options, parent, win, renderer, done) {
    parent.respondTo('proxy', function(host, done) {
        win.webContents.session.setProxy({
            proxyRules: 'http=' + host + ';https='+ host +';ftp=' + host,
        }, function() {
            done();
        });
    });
    done();
	},
	function(host, done) {
	    this.child.call('proxy', host, done);
	});
	
	
  // HELP PAGE PARSE START
	let userAgent = configLib.getRandomUserAgentString();
	var finalHelpPageFees = {};
	finalHelpPageFees.serviceFee = '';
	finalHelpPageFees.smallOrderMin = '';
  var helpPageFees = yield nightmare.useragent(userAgent).viewport(720, 1280)
										  .goto('https://help.uber.com/ubereats/article/how-do-prices-work-on-uber-eats?nodeId=7411dc8d-8b92-419c-b981-b8227a91596e')
											.wait('footer.page-footer')
											.evaluate(function() {
												 return window.__JSON_GLOBALS_["reactProps"];
				             	 })			
											.catch(function(error) {
										    if (DEBUG_MODE) {
										      const screenshotFileName = loggerLib.screenshotFileName('help_page_load_error');
										  	  nightmare.screenshot(screenshotFileName);
										  	  loggerLib.log(error, null, [screenshotFileName]);
										    } else {
										      loggerLib.log(error);
										    }
										    return false;
											});
  if (helpPageFees && helpPageFees.node) {
  	helpPageFees = helpPageFees.node.components;
  	for (var hlpI = 0; hlpI < helpPageFees.length; hlpI++) {
  		var helpPageFeesContent = helpPageFees[hlpI].content.text;
  		var serviceFeeRegexp = /Service fees equal(.*?)of/ig;
  		var serviceFeePercent = serviceFeeRegexp.exec(helpPageFeesContent);
  		if (serviceFeePercent && serviceFeePercent[1]) {
  			serviceFeePercent[1] = serviceFeePercent[1].trim();
  			if (serviceFeePercent[1].indexOf('%') > 0) {
  				finalHelpPageFees.serviceFee = parseInt(serviceFeePercent[1].replace('%', '').trim());
  			}
  		}
  		var smallOrderMinFeeRegexp = /Small order fees only apply to orders less than(.*?)\./ig;
  		var smallOrderMinFee = smallOrderMinFeeRegexp.exec(helpPageFeesContent);
  		if (smallOrderMinFee && smallOrderMinFee[1]) {
  			smallOrderMinFee[1] = smallOrderMinFee[1].trim();
  			if (smallOrderMinFee[1].indexOf('$') >= 0) {
  				finalHelpPageFees.smallOrderMin = parseInt(smallOrderMinFee[1].replace('$', '').trim());
  			}
  		}
  	}
  }
  // HELP PAGE PARSE END	
  
  const curDateStr = (new Date()).toISOString().replace('T', ' ').split('.')[0].split(' ')[0];
  
	let homePageReload = true;
  
	// Loop Job Descriptions
	for (var jdi = 0; jdi < jobDescriptions.length; jdi++) {
		
		// Job TIMER Start
		let jobStart = process.hrtime();
		
		let jobDescription = jobDescriptions[jdi];
		
		// Get Addresses From Job Description
		const addresses = configLib.getAddresses(jobDescription);
		
		// Get Search Queries From Job Description
		const searchQueries = configLib.getSearchQueries(jobDescription);		
		
		loggerLib.log('JOB START: ' + (jdi+1) + '/' + jobDescriptions.length + ' ADDRESSES: ' + addresses.length);
		
		for (var ai = 0; ai < addresses.length; ai++) {
			
			let address = addresses[ai];
			
			loggerLib.log('ADDRESS START: ' + (ai+1) + '/' + addresses.length  + ' ' + address);
			
			if (homePageReload) {
			  let userAgent = configLib.getRandomUserAgentString();
			  var homePageLoad = yield nightmare.useragent(userAgent).viewport(720, 1280)
								  .filter({
								    urls: [
								         	  'https://d3i4yxtzktqr9n.cloudfront.net/*',
								         	  'https://www.ubereats.com/static/*'
								          ],
								   }, function(details, cb) {
								     return cb({ cancel: true });
								   })     		  
								  .goto('https://www.ubereats.com/en-US/')
									.inject('js', 'inject-js/jquery.min.js')
									.inject('js', 'inject-js/moment-with-locales.min.js')
									.inject('js', 'inject-js/moment-timezone-with-data-2012-2022.min.js')
									.wait('#address-selection-input')
								  .then(() => {
								  	return true;
							  	})
							  	.catch(function(error) {
								    if (DEBUG_MODE) {
								      const screenshotFileName = loggerLib.screenshotFileName('home_page_load_error');
					  		  	  nightmare.screenshot(screenshotFileName);
					  		  	  loggerLib.log(error, null, [screenshotFileName]);
								    } else {
								      loggerLib.log(error);
								    }
								    return false;
							  	});
			  
				loggerLib.log('Home Page Load: ' + homePageLoad);
				if (!homePageLoad) {
					continue;
				}
				homePageReload = false;
			}
			
		  var latLng =  yield addressCoordsLib.getCoordinatesByAddress(address).then(data => {
		    return data;
		  }).catch((error) => {
		  	loggerLib.log(error);
		    return [];
		  });			
			
		  var result = yield nightmare
			  .evaluate((addressTimeZoneSearchQueries, done) => {
			  	
			  	var result = {};
				  result.rests = [];
				  result.errors = [];
			  	
			  	if (typeof window == 'undefined' || window == null) {
			  		console.log('CONSOLE: ERROR: window is not defined');
			  		done(null, result);	
			  	}
			  	
			    window.address = addressTimeZoneSearchQueries.address;
		  	  console.log('CONSOLE: ' + window.address + '| Evaluate Start');
				 
			 		var params = {};
					params.targetLocation = {};
					params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
					params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
					params.targetLocation.reference = '';
					params.targetLocation.type = "google_places";
					params.targetLocation.address = {};
					params.targetLocation.address.title = '';
					params.targetLocation.address.address1 = '';
					params.targetLocation.address.city = '';
					params.bafEducationCount = 20;
					params.feed = "combo";
					params.feedTypes = ['STORE', 'SEE_ALL_STORES'];
					params.feedVersion = 2;
			 		
					console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Start');
		 			var bootstrapEaterRequest = $.ajax({
						url: "https://www.ubereats.com/rtapi/eats/v1/bootstrap-eater",
						method: "POST",
						dataType: "json",
						data: JSON.stringify(params),
						cache: false,
						crossDomain: true,
						contentType: "application/json",
						beforeSend: function(xhr) {
							xhr.setRequestHeader('Accept', '*/*');
							xhr.setRequestHeader('Cache-Control', 'max-age=0');
							xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
							xhr.setRequestHeader('x-csrf-token', window.csrfToken);
						},
						xhrFields: {
							withCredentials: true
						},
						timeout: 8000
					});
		 			
		 			bootstrapEaterRequest.done(function(bootstrapEaterRequestResponse) {
		 				
		 				console.log('CONSOLE: ' + window.address + '| BootstrapEaterRequest Done');
		 				
		 				if (!bootstrapEaterRequestResponse.marketplace.isInServiceArea) {
							var resultToSave = {};
							resultToSave.request = 'bootstrapEaterRequest';
							resultToSave.msg = 'No Service in area';
							result.errors.push(resultToSave);
							done(null, result);
							return;
		 				}
		 				
		 			
		 				try {
			 				let index = 1;
			 				for (let fii = 0; fii < bootstrapEaterRequestResponse.marketplace.feed.feedItems.length; fii++) {
			 					
			 				  let feedItem = bootstrapEaterRequestResponse.marketplace.feed.feedItems[fii];
			 				 
			 					if (feedItem.type == 'STORE') {
			 					  let feedItemRest = feedItem.payload.storePayload;
			 					  var feedItemRestData = JSON.parse(feedItemRest.trackingCode);
			 					  if (feedItemRestData.storePayload.isOrderable) {
			 					  	var resultToSave = {};
				 					  resultToSave.searchQuery = '';
				 					  resultToSave.name = feedItemRest.stateMapDisplayInfo.available.title.text;
				 					  
				 					  resultToSave.uuid = feedItem.uuid;
				 					  resultToSave.city = bootstrapEaterRequestResponse.marketplace.cityName;
				 					  resultToSave.href = '';
				 				  	resultToSave.position = index;
				 				  	
				 				  	if (typeof feedItemRest.stateMapDisplayInfo.available.tagline.text != 'undefined') {
							  	  	let priceCuisine = feedItemRest.stateMapDisplayInfo.available.tagline.text.trim().split('•');
							  	  	for (let pci = 0; pci < priceCuisine.length; pci++) {
							  	  		priceCuisine[pci] = priceCuisine[pci].trim(); 
							  	  	}
							  	  	let price = priceCuisine.shift();
							  	  	if (price.indexOf('$') >= 0) {
							  	  		resultToSave.price = price.length;	
							  	  	}
							  	  	resultToSave.cuisine = priceCuisine.join('|');
				 				  	}
						  	  	
						  	  	resultToSave.eta = feedItemRestData.storePayload.etdInfo.dropoffETARange.min + '-' + feedItemRestData.storePayload.etdInfo.dropoffETARange.max;
						  	  	resultToSave.etaMin = feedItemRestData.storePayload.etdInfo.dropoffETARange.min;
						  	  	resultToSave.etaMax = feedItemRestData.storePayload.etdInfo.dropoffETARange.max;
						  	  	
						  	  	if (typeof feedItemRestData.storePayload.fareInfo != 'undefined') {
						  	  		resultToSave.deliverySearchPageFee = feedItemRestData.storePayload.fareInfo.serviceFee;
						  	  		resultToSave.deliveryMenuPageFee = feedItemRestData.storePayload.fareInfo.serviceFee;
						  	  		resultToSave.deliveryCheckoutPageFee = feedItemRestData.storePayload.fareInfo.serviceFee;
						  	  	}
						  	  	
						  	  	resultToSave.dateTimeUTC0 = (new Date()).toISOString(); 						  	  	
						  	  	resultToSave.dateTimeLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD hh:mm:ss');
						  	  	resultToSave.dateLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD');
						  	  	resultToSave.hourLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('HH');
						  	  	resultToSave.dayLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('dddd');									  	  	
						  	  	
						  	  	result.rests.push(resultToSave);
				 					
					 					if (index >= 20) {
					 						break;
					 					}
					 					index++;
			 					  }
			 					}
			 				}
		 				} catch(error) {
		 					console.log('CONSOLE: ERROR: ' + error);
		 				}
		 				
		 				if (result.rests.length == 0) {
							var resultToSave = {};
							resultToSave.request = 'bootstrapEaterRequest';
							resultToSave.msg = 'No restaurants found';
							result.errors.push(resultToSave);				 					
		 					done(null, result);
		 					return;
		 				}
		 				
		 				let categories = addressTimeZoneSearchQueries.searchQueries;
		 				
		 				console.log('CONSOLE: ' + window.address + '| Search Queries: ' + categories.join(','));
		 				
		 				var params = {};
						params.targetLocation = {};
						params.targetLocation.latitude = parseFloat(addressTimeZoneSearchQueries.latLng.lat);
						params.targetLocation.longitude = parseFloat(addressTimeZoneSearchQueries.latLng.lng);
						params.targetLocation.reference = '';
						params.targetLocation.type = "google_places";
						params.targetLocation.address = {};
						params.targetLocation.address.title = '';
						params.targetLocation.address.address1 = '';
						params.targetLocation.address.city = bootstrapEaterRequestResponse.marketplace.cityName;
						params.sortAndFilters = [];
						
						var promisesArr = [];
						var catsTotal = categories.length;
						for (var i = 0; i < catsTotal; i++) {
								promisesArr.push(function() {
									return new Promise(function(res, rej) {
										var pars = JSON.parse(JSON.stringify(params));
										pars.userQuery = categories.shift();
										console.log('CONSOLE: ' + window.address + '| SearchRequest (' + pars.userQuery + '): Start');
										var searchRequest = $.ajax({
											url: "https://www.ubereats.com/rtapi/eats/v1/search",
											method: "POST",
											dataType: "json",
											data: JSON.stringify(pars),
											cache: false,
											crossDomain: true,
											contentType: "application/json",
											beforeSend: function(xhr){
												xhr.setRequestHeader('Accept', '*/*');
												xhr.setRequestHeader('Cache-Control', 'max-age=0');
												xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
												xhr.setRequestHeader('x-csrf-token', window.csrfToken);
											},
											xhrFields: {
												withCredentials: true
											},
											success: function(searchRequestResponse) {
												try {
													let userQuery = JSON.parse(this.data).userQuery;
													
													console.log('CONSOLE: ' + window.address + '| SearchRequest (' + userQuery + '): Done');
													
													let city = JSON.parse(this.data).targetLocation.address.city;
	
													let index = 1;
									 				for (let fii = 0; fii < searchRequestResponse.results.length; fii++) {
									 					
									 				  let feedItem = searchRequestResponse.results[fii];
									 				 
									 					if (feedItem.type == 'store') {
									 					  let feedItemRest = feedItem.store;
									 					  
									 					  if (feedItemRest.isOrderable) {
									 					  	var resultToSave = {};
										 					  resultToSave.searchQuery = userQuery;
										 					  resultToSave.name = feedItemRest.title;
										 					  
										 					 
										 					  resultToSave.uuid = feedItemRest.uuid;
										 					  resultToSave.city = city;
										 					  resultToSave.href = '';
										 				  	resultToSave.position = index;
										 				  	
										 				  	if (typeof feedItemRest.priceBucket != 'undefined') {
										 				  		resultToSave.price = feedItemRest.priceBucket.length;
										 				  	}
												  	  	
										 				  	if (typeof feedItemRest.categories != 'undefined') {
													  	  	let cuisine = [];
													  	  	for (let ci = 0; ci < feedItemRest.categories.length; ci++) {
													  	  		cuisine.push(feedItemRest.categories[ci].name);
													  	  	}
													  	  	resultToSave.cuisine = cuisine.join('|');
										 				  	}
												  	  	
												  	  	resultToSave.eta = feedItemRest.etaRange.min + '-' + feedItemRest.etaRange.max;
												  	  	resultToSave.etaMin = feedItemRest.etaRange.min;
												  	  	resultToSave.etaMax = feedItemRest.etaRange.max;
												  	  	
												  	  	if (typeof feedItemRest.fareInfo != 'undefined') {
												  	  		resultToSave.deliverySearchPageFee = feedItemRest.fareInfo.serviceFee;
												  	  		resultToSave.deliveryMenuPageFee = feedItemRest.fareInfo.serviceFee;
												  	  		resultToSave.deliveryCheckoutPageFee = feedItemRest.fareInfo.serviceFee;
												  	  	}
												  	  	
												  	  	resultToSave.dateTimeUTC0 = (new Date()).toISOString();
												  	  	resultToSave.dateTimeLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD hh:mm:ss');
												  	  	resultToSave.dateLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('YYYY-MM-DD');
												  	  	resultToSave.hourLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('HH');
												  	  	resultToSave.dayLocal = moment(new Date()).tz(addressTimeZoneSearchQueries.timeZone).format('dddd');									  	  	
												  	  	
												  	  	result.rests.push(resultToSave);
										 					
											 					if (index >= 20) {
											 						break;
											 					}
											 					index++;
									 					  }
									 					}
									 				}
									 				res();
									 				return;
												} catch (error) {
													console.log('CONSOLE: ERROR: ' + error);
													res();
													return;
												}
											},
											error: function(msg, textStatus, errorThrown) {
												let userQuery = JSON.parse(this.data).userQuery;
												console.log('CONSOLE: ' + window.address + '| SearchRequest (' + userQuery + '): ERROR');
												var resultToSave = {};
												resultToSave.request = 'searchRequest';
												resultToSave.msg = msg;
												resultToSave.textStatus = textStatus;
												resultToSave.errorThrown = errorThrown;
												result.errors.push(resultToSave);															
												res();
												return;
											},
											timeout: 8000
										});
									})});
							}
							
							promisesArr.push(function() {
								return new Promise(function(res, rej) {
									console.log('CONSOLE: ' + window.address + '| Evaluate Done');
									done(null, result);
									res();
									return;
								})
							});
							
							promisesArr.reduce(function(prev, cur) { return prev.then(cur) }, Promise.resolve());
		 				
			 		}); // bootstrapEaterRequest
		 			
		 			
		 			bootstrapEaterRequest.fail(function(msg, textStatus, errorThrown) {
						var resultToSave = {};
						resultToSave.request = 'bootstrapEaterRequest';
						resultToSave.msg = msg;
						resultToSave.textStatus = textStatus;
						resultToSave.errorThrown = errorThrown;
						result.errors.push(resultToSave);
						done(null, result);	
						return;
					});	
				 			
			 }, {address: address, timeZone: jobDescription.TimeZone, searchQueries: searchQueries, latLng: latLng})
		   .catch(function(error) {		  	 
		  	 if (DEBUG_MODE) {
		  		 const screenshotFileName = loggerLib.screenshotFileName('address_search_error'); 
		  		 nightmare.screenshot(screenshotFileName);
		  		 loggerLib.log(error, null, [screenshotFileName]);
		  	 } else {
		  		 loggerLib.log(error); 
		  	 }
		  	 homePageReload = true;
		  	 return {};
		   });
		  
		  
	  	if (typeof result.errors != 'undefined' && result.errors.length > 0) {
	  		for (let eri = 0; eri < result.errors.length; eri++) {
	  			loggerLib.log(result.errors[eri]);	
	  		}
	  		delete result.errors;
	  	}
	  	
			if (typeof result.rests != 'undefined' && result.rests.length > 0) {

				for (var ri = 0; ri < result.rests.length; ri++) {
					result.rests[ri].site = WEBSITE_NAME;
					result.rests[ri].address = address;
					result.rests[ri].market = jobDescription.CBSA;
					result.rests[ri].serviceFee = finalHelpPageFees.serviceFee;
					result.rests[ri].smallOrderMin = finalHelpPageFees.smallOrderMin;					
		  	  let nameSlug = result.rests[ri].name.toLowerCase().replace(/\s/g, '-').replace(/[^a-z0-9-]+/gi, '');
					let uuidSlug = slugid.encode(result.rests[ri].uuid);
					delete result.rests[ri].uuid;
					result.rests[ri].href = 'https://www.ubereats.com/en-US/' +  result.rests[ri].city + '/food-delivery/' + nameSlug + '/' + uuidSlug
					delete result.rests[ri].city;
				}
				
				resultsGlobal.push(...result.rests);
				
				loggerLib.log('ADDRESS END|' + address + '|Results: ' + result.rests.length + '|Results Total: ' + resultsGlobal.length);				
		  } else {
		  	loggerLib.log('ADDRESS END|' + address + '|NO Results');
		  }
			
		} // Address END
		
		let usedMemory = Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100;
		let jobTime = Math.round((process.hrtime(jobStart)[0]));
		
		if (resultsGlobal.length > 0) {
		  resultsLib.saveToCSV(WEBSITE_NAME, resultsGlobal, THREAD_ID);
		}
		
		loggerLib.log('JOB END|' + (jdi+1) + '|' + jobTime + '|' + usedMemory + '|Results saved: ' + resultsGlobal.length);
	} // JOB END
	
	if (!DEBUG_MODE) {
		const threadTime = Math.round((process.hrtime(start)[0]/60));
		const cloudWatchLib = require("./lib/cloudwatch");
		yield cloudWatchLib.logMetric(WEBSITE_NAME, 'Execution Time (minutes)', threadTime, WEBSITE_NAME + '-Threads', THREAD_ID);
		
		let totalPrice = 0;
		let avgPrice = 0;
		let totalMaxEta = 0;
		let avgMaxEta = 0;
		for (let i = 0; i < resultsGlobal.length; i++) {
			let res = resultsGlobal[i];
			if (typeof res.deliverySearchPageFee != 'undefined' && res.deliverySearchPageFee != null) {
				totalPrice = totalPrice + parseFloat(res.deliverySearchPageFee);
			}
			if (typeof res.etaMax != 'undefined' && res.etaMax != null) {
				totalMaxEta = totalMaxEta + res.etaMax;
			}
		}
		if (resultsGlobal.length > 0) {
			totalPrice = Math.round(totalPrice * 100) / 100;
			avgPrice = Math.round((totalPrice / resultsGlobal.length) * 100) / 100;
			totalMaxEta = Math.round(totalMaxEta * 100) / 100;
			avgMaxEta = Math.round((totalMaxEta / resultsGlobal.length) * 100) / 100;
			let cwPriceResult = yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Price', avgPrice);
			loggerLib.log("CW Price Result: " + util.inspect(cwPriceResult, { compact: true }).replace(/\n/g, ''));
			let cwEtaResult = yield cloudWatchLib.logMetric(WEBSITE_NAME, 'AVG Max ETA', avgMaxEta);
			loggerLib.log("CW ETA Result: " + util.inspect(cwEtaResult, { compact: true }).replace(/\n/g, ''));
			loggerLib.log('CW DATA Sent|AVG Price: ' + avgPrice + '|AVG Max ETA: ' + avgMaxEta);
		}
	}		
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
	
	return resultsGlobal;
}

vo(run)(function(error, results) {
	if (error) {
 		loggerLib.log(error);
	}
 	 
	if (results) {
		loggerLib.log('FINAL Results saved: ' + results.length);
		if (results.length > 0) {
		  resultsLib.saveToCSV(WEBSITE_NAME, results, THREAD_ID);
		}
	}	
	
 	const executionTime = Math.round((process.hrtime(start)[0]/60));
	loggerLib.log('--- DONE. Execution Time: ' + executionTime + ' minute(s)', null, [executionTime]);
	
  if (nightmare) {
    nightmare.proc.kill();
    nightmare.end();
    nightmare = null;
    loggerLib.log('Nightmare/Electron killed');
  }
  
  loggerLib.log('Process exit');
  process.exit();
});