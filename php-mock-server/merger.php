<?

$allTimings = explode(PHP_EOL, file_get_contents('price-scraping-timings.csv'));
unset($allTimings[count($allTimings) - 1]);

$requestTimings = explode("\n", file_get_contents('logs.csv'));
unset($requestTimings[count($requestTimings) - 1]);
$requestTimingsKv = array();
foreach ($requestTimings as $requestTiming) {
    $requestTiming = explode(';', $requestTiming);
    if (isset($requestTimingsKv[$requestTiming[0]])) {
        $requestTimingsKv[$requestTiming[0]] = $requestTimingsKv[$requestTiming[0]] + 1;
    } else {
        $requestTimingsKv[$requestTiming[0]] = 0;
    }
}


$resultTimings = array();

foreach ($allTimings as $tmg) {
    if (isset($requestTimingsKv[$tmg])) {
        $resultTimings[$tmg] = $requestTimingsKv[$tmg];
    } else {
        $resultTimings[$tmg] = 0;
    }
}

echo '<table>';
foreach ($resultTimings as $k => $v) {
    echo '<tr>';
    echo '<td>' . $k . '</td>';
    echo '<td>' . $v . '</td>';
    echo '</tr>';
}
echo '</table>';
// print_r($resultTimings);


die('DONE');

?>