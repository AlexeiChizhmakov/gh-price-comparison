<?

$allTimings = explode(PHP_EOL, file_get_contents('price-scraping-timings.csv'));
unset($allTimings[count($allTimings) - 1]);

$requestTimings = explode("\n", file_get_contents('Logs-2018-05-29-23.log'));
unset($requestTimings[count($requestTimings) - 1]);
$requestTimingsKv = array();
foreach ($requestTimings as $requestTiming) {
    $requestTiming = explode('|', $requestTiming);
    $time = strtok($requestTiming[1], '.');

    if (isset($requestTiming[5])) {
        if (strpos($requestTiming[5], ') START') !== FALSE) {
            if (isset($requestTimingsKv[$time])) {
                $requestTimingsKv[$time] = $requestTimingsKv[$time] + 1;
            } else {
                $requestTimingsKv[$time] = 0;
            }
        }
    }

}


$resultTimings = array();

foreach ($allTimings as $tmg) {
    if (isset($requestTimingsKv[$tmg])) {
        $resultTimings[$tmg] = $requestTimingsKv[$tmg];
    } else {
        $resultTimings[$tmg] = 0;
    }
}

echo '<table>';
foreach ($resultTimings as $k => $v) {
    echo '<tr>';
    echo '<td>' . $k . '</td>';
    echo '<td>' . $v . '</td>';
    echo '</tr>';
}
echo '</table>';
// print_r($resultTimings);


die('DONE');

?>